;(function ($) {
    "use strict";

    /*============= preloader js css =============*/
    // var cites = [];
    // cites[0] = "We design Docly for the readers, optimizing not for page views or engagement";
    // cites[1] = "Docly turns out that context is a key part of learning.";
    // cites[2] = "You can create any type of product documentation with Docly";
    // cites[3] = "Advanced visual search system powered by Ajax";
    // var cite = cites[Math.floor(Math.random() * cites.length)];
    //$('#preloader p').text(cite);
    let id = '#preloader';
    let timeout = 1000;
    $(id).addClass('loading');
    $(id + " > svg").addClass('active');

    $(window).on('load', () => setTimeout(() => $(id).fadeOut(500, () => $(id).removeClass('loading')), timeout));
})(jQuery)