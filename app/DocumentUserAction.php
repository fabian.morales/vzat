<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentUserAction extends Model
{
    protected $table = 'document_user_action';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function document() {
        return $this->belongsTo(Document::class);
    }
}
