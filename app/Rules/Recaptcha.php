<?php

namespace App\Rules;

use Illuminate\Support\Facades\Http;
use Illuminate\Contracts\Validation\Rule;

class Recaptcha implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $httpRes = Http::asForm()->post('https://www.google.com/recaptcha/api/siteverify', [
            'secret' => env('CAPTCHA_SECRET'),
            'response' => $value,
        ]);
        
        return $httpRes['success'] && $httpRes['action'] == 'submit' && $httpRes['score'] > 0.5;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __("No se superó la prueba del captcha");
    }
}
