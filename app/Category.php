<?php

namespace App;

use App\Helpers\LocaleHelper;
use App\Traits\MultiLanguageFieldTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use MultiLanguageFieldTrait;
    protected $table = 'category';
    protected $fillable = [
        'name_en', 'name_es', 'color', 'description_en', 'description_es'
    ];
    protected $appends = ['name', 'description'];
}
