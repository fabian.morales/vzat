<?php

namespace App;

use App\Helpers\LocaleHelper;
use App\Traits\MultiLanguageFieldTrait;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use MultiLanguageFieldTrait;
    protected $table = 'country';
    protected $fillable = [
        'name_es', 'name_en',
    ];
    protected $appends = ['name'];
}
