<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentChange extends Model
{
    protected $table = 'document_change';
    protected $fillable = [
        'ip_address', 'action', 'user_id', 'document_id', 'before', 'after',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function document() {
        return $this->belongsTo(Document::class);
    }
}
