<?php

namespace App\Console\Commands;

use App\Document;
use Illuminate\Console\Command;

class GenerateImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:regen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('max_execution_time', 3600);

        $documents = Document::whereNotNull('cover_image')->where('cover_image', '<>', '')->get();
        foreach ($documents as $document) {
            $document->generateImages();
        }
    }
}
