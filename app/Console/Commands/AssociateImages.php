<?php

namespace App\Console\Commands;

use App\Document;
use Illuminate\Console\Command;

class AssociateImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:assoc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('max_execution_time', 3600);

        $documents = Document::whereNotNull('cover_image')->where('cover_image', '<>', '')->get();        
        foreach ($documents as $document) {
            $image = storage_path("app/temp/covers/{$document->cover_image}");
            if (is_file($image)) {
                $path = ['storage', 'images', 'document', $document->id];
                $dir = public_path();
                foreach ($path as $p) {
                    $dir .= '/' . $p;
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                }

                copy($image, $dir . "/" . $document->cover_image);
            }
        }
    }
}
