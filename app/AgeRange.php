<?php

namespace App;

use App\Traits\MultiLanguageFieldTrait;
use Illuminate\Database\Eloquent\Model;

class AgeRange extends Model
{
    use MultiLanguageFieldTrait;
    protected $table = 'age_range';
    protected $fillable = [
        'name_en', 'name_es',
    ];
}
