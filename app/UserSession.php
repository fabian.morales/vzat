<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSession extends Model
{
    protected $table = 'user_session';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function scopeActive($query) {
        return $query->where('active', 1);
    }

    public function scopeCurrent($query) {
        return $query->where('sessid', session('app.sessid'));
    }
}
