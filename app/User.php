<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'email_reason', 'password', 'entity', 'position', 'country_id', 'role_id', 'accept_terms', 'last_ip_addr', 'receive_emails', 'gender_id', 'age_range_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'validated',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function country() {
        return $this->belongsTo(Country::class);
    }

    public function gender() {
        return $this->belongsTo(Gender::class);
    }

    public function ageRange() {
        return $this->belongsTo(AgeRange::class);
    }

    public function sessions() {
        return $this->hasMany(UserSession::class);
    }

    public function verificationLinks() {
        return $this->hasMany(UserVerification::class);
    }

    public function loginAttempts() {
        return $this->hasMany(LoginAttempt::class);
    }

    public function documentActions() {
        return $this->hasMany(DocumentUserAction::class);
    }

    public function documentChanges() {
        return $this->hasMany(DocumentChange::class);
    }
}
