<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $table = 'search';
    protected $fillable = [
        'text_search', 'hash', 'user_id', 'ip_address', 'language_id', 'category_id', 'sector_id', 'state_id'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function language() {
        return $this->belongsTo(Language::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function sector() {
        return $this->belongsTo(Sector::class);
    }

    public function state() {
        return $this->belongsTo(State::class);
    }

    public function words() {
        return $this->belongsToMany(SearchWord::class);
    }
}
