<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class ApprovalUserNotify extends Mailable
{
    use Queueable, SerializesModels;
    private $user;
    private $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->locale(App::getLocale())
            ->subject(__("Tu cuenta ha sido aprobada"))
            ->view('emails.user_approval.approval', ['user' => $this->user, 'token' => $this->token, 'title' => __("Tu cuenta ha sido aprobada")]);
    }
}
