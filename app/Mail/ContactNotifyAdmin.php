<?php

namespace App\Mail;

use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class ContactNotifyAdmin extends Mailable
{
    use Queueable, SerializesModels;
    private $contact;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->locale(App::getLocale())
            ->subject(__("Nueva solicitud de contacto"))
            ->view('emails.contact.admin_notify', ['contact' => $this->contact, 'title' => __("Nueva solicitud de contacto")]);
    }
}
