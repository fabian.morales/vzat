<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class DisapprovalUserNotify extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->locale(App::getLocale())
            ->subject(__("Tu cuenta no ha sido aprobada"))
            ->view('emails.user_approval.disapproval', ['user' => $this->user, 'title' => __("Tu cuenta no ha sido aprobada")]);
    }
}
