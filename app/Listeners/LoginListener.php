<?php

namespace App\Listeners;

use App\LoginAttempt;
use App\UserSession;
use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;

class LoginListener
{
    private $request;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $user = $event->user;
        $user->last_login = Carbon::now();
        $user->last_ip_addr = $this->request->ip();
        $user->save();

        $attempt = new LoginAttempt();
        $attempt->ip_address = $this->request->ip();
        $attempt->result = 'success';
        $attempt->user_id = $user->id;
        $attempt->email = $this->request->get('email');
        $attempt->save();

        $sessid = \Session::getId();
        $session = new UserSession();
        $session->ip_address = $this->request->ip();
        $session->sessid = $sessid;
        $session->last_activity = Carbon::now();
        $session->user_id = $user->id;
        $session->active = 1;
        $session->save();

        $this->request->session()->put('app.sessid', $sessid);
    }
}
