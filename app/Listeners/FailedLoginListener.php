<?php

namespace App\Listeners;

use App\LoginAttempt;
use Illuminate\Auth\Events\Failed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;

class FailedLoginListener
{
    private $request;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    /**
     * Handle the event.
     *
     * @param  Failed  $event
     * @return void
     */
    public function handle(Failed $event)
    {
        $attempt = new LoginAttempt();
        $attempt->ip_address = $this->request->ip();
        $attempt->result = 'failed';
        $attempt->email = $this->request->get('email');
        $attempt->save();
    }
}
