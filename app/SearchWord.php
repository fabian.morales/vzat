<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchWord extends Model
{
    protected $table = 'search_word';
    protected $fillable = [
        'value', 
    ];

    public function searches() {
        return $this->belongsToMany(Search::class);
    }
}
