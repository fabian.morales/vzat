<?php

namespace App;

use App\Helpers\LocaleHelper;
use App\Traits\MultiLanguageFieldTrait;
use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    use MultiLanguageFieldTrait;
    protected $table = 'sector';
    protected $fillable = [
        'name_en', 'name_es',
    ];
    protected $appends = ['name'];

    public function documents() {
        return $this->belongsToMany(Document::class);
    }
}
