<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;

class SummaryReportSheetExport implements FromArray, WithTitle
{
    private $title;
    private $data;

    public function __construct($data, $title) {
        $this->data = $data;
        $this->title = $title;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array {
        return $this->data;
    }

    public function title(): string {
        return $this->title;
    }
}
