<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SummaryReportExport implements WithMultipleSheets
{
    private $collection;

    public function __construct($collection) {
        $this->collection = $collection;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function sheets(): array {
        $sheets = [];

        foreach ($this->collection as $row) {
            $data = array_map(function($item) {
                return array_values((array)$item);
            }, $row['data']);

            $data = array_merge([$row['head']], $data);
            
            $sheet = new SummaryReportSheetExport($data, $row['title']);
            $sheets[] = $sheet;
        }

        return $sheets;
    }
}
