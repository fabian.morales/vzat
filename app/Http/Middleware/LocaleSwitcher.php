<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use App\Helpers\LocaleHelper;
use Config;

class LocaleSwitcher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('locale')) {
            $locale = Config::get('app.locale');
            $hispanic = LocaleHelper::getHispanicCountries();
            $country = LocaleHelper::getCountry();

            if (key_exists($country->country_code, $hispanic)) {
                $locale = 'es';
            }

            Session::put('locale', $locale);
        }

        App::setLocale(Session::get('locale'));
        return $next($request);
    }
}
