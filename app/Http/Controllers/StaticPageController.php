<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class StaticPageController extends Controller
{
    public function localeSwitch(Request $request, $lang) {
        $lang = $lang == 'en' ? 'en' : 'es';
        App::setLocale($lang);
        \Session::put('locale', $lang);
        return $request->ajax() ? ["ok" => App::getLocale()] : redirect()->back();
    }

    public function showAboutPage() {
        SEOMeta::setTitle(__("Quiénes somos"));
        return view("frontoffice.pages.about");
    }

    public function showProductsPage() {
        SEOMeta::setTitle(__("Nuestros productos"));
        return view("frontoffice.pages.products");
    }

    public function showPrivacyPolicyPage() {
        SEOMeta::setTitle(__("Política de privacidad"));
        return view("frontoffice.pages.terms");
    }

    public function showDashboard() {
        SEOMeta::setTitle(__("Tablero"));
        return view("frontoffice.pages.dashboard");
    }
}
