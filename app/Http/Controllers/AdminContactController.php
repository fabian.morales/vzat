<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class AdminContactController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'contacts';
    }

    public function returnDataTable() {
        $this->checkPermission('view');
        return \DataTables::of(Contact::orderBy('created_at'))->make(true);
    }

    public function list() {
        $this->checkPermission('view');

        $contacts = Contact::all();
        return view('backoffice.contacts.list', compact('contacts'));
    }

    public function view($id) {
        $this->checkPermission('view');

        $contact = Contact::with('country')->where('id', $id)->first();
        if (empty($contact)) {
            return back()
                ->with(['message_error' => __("Solicitud de contacto no encontrada")]);
        }

        return view('backoffice.contacts.view', compact('contact'));
    }
}
