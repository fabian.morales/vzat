<?php

namespace App\Http\Controllers;

use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LanguageController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'languages';
    }

    public function list() {
        $this->checkPermission('view');

        $languages = Language::all();
        return view('backoffice.languages.list', ['languages' => $languages]);
    }

    private function showForm(Language $language, $edit = false) {
        if (empty($language)) {
            $language = new Language();
        }

        return view('backoffice.languages.form', ['language' => $language, 'edit' => $edit]);
    }

    public function create() {
        $this->checkPermission('create');

        return $this->showForm(new Language());
    }

    public function edit($id) {
        $this->checkPermission('edit');

        $language = Language::find($id);
        if (empty($language)) {
            return back()
                ->with(['message_error' => __("Idioma no encontrado")]);
        }

        return $this->showForm($language, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $language = new Language();
        $language->fill($request->all());
        if ($language->save()) {
            return redirect()
                ->route('admin::languages::list')
                ->with(['message_success' => __("Idioma guardado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar el idioma")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $language = Language::find($request->get('id'));
        if (empty($language)) {
            return back()
                ->with(['message_error' => __("Idioma no encontrado")]);
        }

        $language->fill($request->all());
        if ($language->save()) {
            return redirect()
                ->route('admin::languages::list')
                ->with(['message_success' => __("Idioma actualizado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar el idioma")]);
        }
    }
}
