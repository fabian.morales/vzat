<?php

namespace App\Http\Controllers;

use App\Category;
use App\Document;
use App\DocumentChange;
use App\DocumentKeyword;
use App\Helpers\ImageHelper;
use App\Language;
use App\Sector;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DocumentController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'documents';
    }

    public function list() {
        $this->checkPermission('view');

        $documents = []; //Document::all();
        return view('backoffice.documents.list', compact('documents'));
    }

    public function dataTable() {
        return \DataTables::of(Document::with(['category', 'language']))->make(true);
    }

    // public function generateImages(Document $document) {
    //     ImageHelper::makeImage($document->getOriginalImagePath(), $document->getImagePath('inner'), 770, 450);
    //     ImageHelper::makeImage($document->getOriginalImagePath(), $document->getImagePath('featured'), 670, 450);
    //     ImageHelper::makeImage($document->getOriginalImagePath(), $document->getImagePath('thumbnail'), 370, 220);
    //     ImageHelper::makeImage($document->getOriginalImagePath(), $document->getImagePath('tiny'), 70, 70);
    // }

    private function showForm(Document $document, $sectors = null, $states = null, $edit = false) {
        if (empty($document)) {
            $document = new Document();
        }

        $categories = Category::all();
        $languages = Language::all();

        if (empty($sectors)) {
            $sectors = Sector::all();
        }

        if (empty($states)) {
            $states = State::all();
        }

        return view('backoffice.documents.form', compact('document', 'categories', 'languages', 'sectors', 'states', 'edit'));
    }

    public function create() {
        $this->checkPermission('create');

        return $this->showForm(new Document());
    }

    public function edit($id) {
        $this->checkPermission('edit');

        $document = Document::with('keywords')->where('id', $id)->first();
        if (empty($document)) {
            return back()
                ->with(['message_error' => __("Documento no encontrado")]);
        }

        $states = State::with(['documents' => function($q) use ($id) {
            $q->where('document_id', $id);
        }])->get();

        $sectors = Sector::with(['documents' => function($q) use ($id) {
            $q->where('document_id', $id);
        }])->get();

        return $this->showForm($document, $sectors, $states, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'document' => 'required',
            'category_id' => 'required',
            'publication_date' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $file = $request->file('document');

        if (!$file->isValid() || $file->extension() != "pdf") {
            return back()
                ->with(['message_error' => __("Archivo no valido")]);
        }

        $document = new Document();
        $document->fill($request->all());
        $document->description = $document->description ?? $document->title;
        $document->user_id = \Auth::user()->id;
        $document->views = 0;
        $document->downloads = 0;

        $filename = uniqid() . '.' . $file->extension();
        $document->file = $filename;
        $document->file_name = $file->getClientOriginalName();
        $document->size = $file->getSize();
        
        if ($document->save()) {
            $document->sectors()->sync($request->get('sectors'));
            $document->states()->sync($request->get('states'));

            $keywords = $request->get('keywords');
            if (!empty($keywords)) {
                $keywords = explode(",", $keywords);
                $document->keywords()->saveMany(array_map(function($value) use ($document) {
                    return new DocumentKeyword(["value" => $value, "document_id" => $document->id]);
                }, $keywords));
            }
            
            Storage::putFileAs('documents', $file, $filename);

            $document = Document::with(['sectors', 'states', 'keywords'])->where('id', $document->id)->first();
            DocumentChange::create([
                'ip_address' => $request->ip(), 
                'action' => 'create', 
                'user_id' => \Auth::user()->id, 
                'document_id' => $document->id, 
                'before' => '', 
                'after' => $document->toJson()
            ]);

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = uniqid() . '.' . $file->extension();
        
                $path = ['storage', 'images', 'document', $document->id];
                $dir = public_path();
                foreach ($path as $p) {
                    $dir .= '/' . $p;
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                }

                $file->move($dir, $filename);
                $document->cover_image = $filename;
                $document->save();

                $document->generateImages();
            }

            return redirect()
                ->route('admin::documents::list')
                ->with(['message_success' => __("Documento guardado exitosamente")]);
        }
        else {
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar el documento")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'category_id' => 'required',
            'publication_date' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $document = Document::with(['sectors', 'states', 'keywords'])->where('id', $request->get('id'))->first();
        if (empty($document)) {
            return back()
                ->with(['message_error' => __("Documento no encontrado")]);
        }

        $before = $document->toJson();

        $document->fill($request->all());
        $document->description = $document->description ?? $document->title;
        $document->user_id = \Auth::user()->id;

        if ($document->save()) {
            $document->sectors()->sync($request->get('sectors'));
            $document->states()->sync($request->get('states'));

            $keywords = $request->get('keywords');
            if (!empty($keywords)) {
                $keywords = explode(",", $keywords);
                $document->keywords()->delete();
                $document->keywords()->saveMany(array_map(function($value) use ($document) {
                    return new DocumentKeyword(["value" => $value, "document_id" => $document->id]);
                }, $keywords));
            }
            
            if ($request->has('document') && $request->file('document')->isValid()) {
                $file = $request->file('document');
                $filename = uniqid() . '.' . $file->extension();
                $document->file_name = $file->getClientOriginalName();
                $document->file = $filename;
                $document->size = $file->getSize();
                Storage::putFileAs('documents', $file, $filename);
                $document->save();
            }

            $after =  $document->toJson();
            if ($before != $after) {
                DocumentChange::create([
                    'ip_address' => $request->ip(), 
                    'action' => 'update', 
                    'user_id' => \Auth::user()->id, 
                    'document_id' => $document->id, 
                    'before' => $before, 
                    'after' => $after
                ]);
            }

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = uniqid() . '.' . $file->extension();
        
                $path = ['storage', 'images', 'document', $document->id];
                $dir = public_path();
                foreach ($path as $p) {
                    $dir .= '/' . $p;
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                }

                $file->move($dir, $filename);
                $document->cover_image = $filename;
                $document->save();

                $document->generateImages();
            }

            return redirect()
                ->route('admin::documents::list')
                ->with(['message_success' => __("Documento actualizado exitosamente")]);
        }
        else {
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar el documento")]);
        }
    }

    public function delete($id) {
        $this->checkPermission('delete');
        DB::beginTransaction();

        $document = Document::find($id);
        if (empty($document)) {
            return back()
                ->with(['message_error' => __("Documento no encontrado")]);
        }

        $document->sectors()->detach();
        $document->states()->detach();
        $document->keywords()->delete();

        if ($document->delete()) {
            DB::commit();
            return redirect()
                ->route('admin::documents::list')
                ->with(['message_success' => __("Documento borrado exitosamente")]);
        }
        else{
            DB::rollBack();
            return back()
                ->with(['message_error' => __("No se pudo borrar el documento")]);
        }
    }
}
