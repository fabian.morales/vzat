<?php

namespace App\Http\Controllers;

use App\UserVerification;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserVerificationController extends Controller
{
    public function verify($token) {
        try {
            $verification = UserVerification::with('user')->where('token', $token)->first();
            if (empty($verification) || $verification->valid == 0) {
                throw new \Exception(__("El código de verificación no es válido"));
            }

            /**
             * TO-DO: limitación de código por fechas
             */

            $user = $verification->user;

            if ($user->approved != 1) {
                throw new \Exception(__("La cuenta debe ser aprobada por el equipo antes de ser usada."));
            }

            if ($user->validated == 1) {
                throw new \Exception("Este usuario ya está verificado.");
            }

            $user->validated = 1;
            $user->email_verified_at = Carbon::now();
            if (!$user->save()) {
                throw new \Exception(__("No se pudo verificar la cuenta, por favor inténtelo de nuevo."));
            }

            $verification->valid = 0;
            $verification->save();

            return redirect()
                ->route('login')
                ->with(['message_success' => __('La cuenta ha sido verificada exitosamente.')]);

        }
        catch (\Exception $th) {
            return redirect()
                ->route('login')
                ->with(['message_error' => $th->getMessage()]);
        }
    }
}
