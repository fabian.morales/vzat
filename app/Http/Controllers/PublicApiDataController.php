<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helpers\LocaleHelper;
use App\Language;
use App\Sector;
use App\State;
use Illuminate\Http\Request;

class PublicApiDataController extends Controller
{
    public function getCategories() {
        $locale = LocaleHelper::getLocale();
        $categories = Category::orderBy("name_" . $locale)->get();
        return $categories->toJson();
    }

    public function getStates() {
        $states = State::orderBy("name")->get();
        return $states->toJson();
    }

    public function getLanguages() {
        $languages = Language::orderBy("name")->get();
        return $languages->toJson();
    }

    public function getSectors() {
        $locale = LocaleHelper::getLocale();
        $sectors = Sector::orderBy("name_" . $locale)->get();
        return $sectors->toJson();
    }
}
