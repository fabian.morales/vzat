<?php

namespace App\Http\Controllers\Auth;

use App\AgeRange;
use App\Country;
use App\Helpers\LocaleHelper;
use App\Http\Controllers\Controller;
use App\Mail\RegistrationUserNotifyAdmin;
use App\Mail\RegistrationUserNotifyUser;
use App\Providers\RouteServiceProvider;
use App\User;
use App\DoimainBlackList;
use App\Gender;
use App\GenericDomain;
use App\Rules\Recaptcha;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::LOGIN;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'entity' => ['required', 'string', 'max:255'],
            'position' => ['required', 'string', 'max:255'],
            'country_id' => ['required', 'integer'],
            'accept_terms' => ['required', 'string'],
        ];

        if (App::environment(['staging', 'production'])) {
            $rules['g-recaptcha-response'] = ['required', new Recaptcha];
        }
        
        return Validator::make($data, $rules);
    }

    public function showRegistrationForm() {
        $countries = Country::orderBy('name_' . LocaleHelper::getLocale())->get();
        $genders = Gender::all();
        $ageRanges = AgeRange::all();
        return view('auth.register', compact('countries', 'genders', 'ageRanges'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $role = Role::where('name', 'Usuario')->first();
        $data['accept_terms'] = $data['accept_terms'] ?? "No";
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'email_reason' => $data['email_reason'],
            'password' => Hash::make($data['password']),
            'entity' => $data['entity'],
            'position' => $data['position'],
            'country_id' => $data['country_id'],
            'gender_id' => $data['gender_id'],
            'age_range_id' => $data['age_range_id'],
            'accept_terms' => $data['accept_terms'],
            'receive_emails' => $data['receive_emails'] ?? 'No',
            'role_id' => $role->id,
            'validated' => 0,
            'approved' => 0,
            'last_ip_addr' => '',
        ]);

        $user->assignRole($role);

        return $user;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        list($inbox, $domain) = explode('@', $request->get('email'));
        if (DoimainBlackList::where('name', $domain)->count() > 0) {
            return back()
                ->with(['message_error' => __('Correo no válido')])
                ->withInput();
        }

        if (GenericDomain::where('domain', $domain)->count() > 0 && empty($request->get('email_reason', ''))){
            return back()
                ->with(['message_error' => __('¿Por qué le interesa conocer nuestra información?')])
                ->withInput();
        }

        event(new Registered($user = $this->create($request->all())));

        $request->session()->flash('message_success', __("Sus datos han sido enviados exitosamente. Una vez sea aprobada la cuenta recibirá un correo electrónico."));

        Mail::to($user->email)->send(new RegistrationUserNotifyUser($user));
        Mail::to(env('MAIL_ADMIN_TO'))->send(new RegistrationUserNotifyAdmin($user));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
}
