<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        AuthenticatesUsers::logout as traitLogout;
        AuthenticatesUsers::login as traitLogin;
        AuthenticatesUsers::showLoginForm as traitShowLoginForm;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        $intendedURL = Session::get('url.intended');

        if($intendedURL && $intendedURL != route('home')) {
            Session::flash('message_error', __("Debe iniciar sesión para continuar"));
        }

        return $this->traitShowLoginForm();
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if (empty($user)) {
            return back()
                ->with(['message_error' => __('El usuario o la contraseña que está usando son incorrectos. Por favor inténtelo de nuevo.')])
                ->withErrors(['message_error' => __('El usuario o la contraseña que está usando son incorrectos. Por favor inténtelo de nuevo.')])
                ->withInput();
        }

        if ($user->approved != 1) {
            return back()
                ->with(['message_error' => __('Su cuenta aún no ha sido aprobada. Un vez esta sea aprobada recibirá un correo electrónico con la confirmación.')])
                ->withErrors(['message_error' => __('Su cuenta aún no ha sido aprobada. Un vez esta sea aprobada recibirá un correo electrónico con la confirmación.')])
                ->withInput();
        }

        if ($user->validated != 1) {
            return back()
                ->with(['message_error' => __('Es necesario verificar su correo electrónico para continuar.')])
                ->withErrors(['message_error' => __('Es necesario verificar su correo electrónico para continuar.')])
                ->withInput();
        }

        return $this->traitLogin($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $rules = [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ];

        /*if (App::environment(['staging', 'production'])) {
            $rules['g-recaptcha-response'] = 'required|captcha';
        }*/

        $request->validate($rules);
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $redirect = $user->hasRole('Administrador') ? $redirect = route('dashboard') : route('home');

        if (session()->has('url.intended') && session()->get('url.intended') != route('home')) {
            $redirect = session()->get('url.intended');
        }

        return redirect($redirect);
    }

    public function logout(Request $request) {
        if (\Auth::check()) {
            $session =\Auth::user()->sessions()->current()->first();

            if (!empty($session)) {
                $session->last_activity = Carbon::now();
                $session->active = 0;
                $session->save();
            }
        }

        return $this->traitLogout($request);
    }
}
