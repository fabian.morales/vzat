<?php

namespace App\Http\Controllers;

use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StateController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'states';
    }

    public function list() {
        $this->checkPermission('view');

        $states = State::all();
        return view('backoffice.states.list', compact('states'));
    }

    private function showForm(State $state, $edit = false) {
        if (empty($state)) {
            $state = new State();
        }

        return view('backoffice.states.form', compact('state', 'edit'));
    }

    public function create() {
        $this->checkPermission('create');

        return $this->showForm(new State());
    }

    public function edit($id) {
        $this->checkPermission('edit');

        $state = State::find($id);
        if (empty($state)) {
            return back()
                ->with(['message_error' => __("Estado no encontrado")]);
        }

        return $this->showForm($state, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $state = new State();
        $state->fill($request->all());
        if ($state->save()) {
            return redirect()
                ->route('admin::states::list')
                ->with(['message_success' => __("Estado guardado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar el estado")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $state = State::find($request->get('id'));
        if (empty($state)) {
            return back()
                ->with(['message_error' => __("Estado no encontrado")]);
        }

        $state->fill($request->all());
        if ($state->save()) {
            return redirect()
                ->route('admin::states::list')
                ->with(['message_success' => __("Estado actualizado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar el estado")]);
        }
    }
}
