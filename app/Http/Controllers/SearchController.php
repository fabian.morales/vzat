<?php

namespace App\Http\Controllers;

use App\Document;
use App\DocumentUserAction;
use App\Search;
use App\SearchWord;
use Doctrine\Inflector\Rules\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request->has('text_search') || $request->has('category_id')) {
            $ctl = new HomeController();
            return $ctl->index($request);
        }

        return redirect()->route('home');
    }

    private function processText($text) {
        $excludedWords = ["el", "la", "los", "las", "a", "o", "y", "e", "u", "al", "de", "del", "para", "con", "en", "su", "sus"];
        $text = strtolower($text);
        $text = preg_replace("/[^a-z0-9áéíóúüñ\-]/i", " ", $text);
        $words = explode(" ", $text);
        $words = array_unique(array_map(function($value) use($excludedWords) {
            return !in_array(trim($value), $excludedWords) ? trim($value) : "";
        }, $words));

        $searchWords = array_map(function($value) use($excludedWords) {
            return "*" . $value . "*";
        }, $words);

        $text = str_replace("**", "", implode(" ", $searchWords));

        return [$words, $text];
    }

    public function getAllDocuments(Request $request) {
        return Document::with(["language", "category", "sectors", "states"])
            ->orderBy('publication_date', 'desc')
            ->get()
            ->toJson();
    }

    public function getRecentDocuments(Request $request) {
        $query = Document::with(["language", "category", "sectors", "states"])->limit(3);
        
        if ($request->has("languages")) {
            $query = $query->whereHas("language", function($q) use ($request) {
                $q->whereIn("id", $request->get("languages"));
            });
        }

        if ($request->has("categories")) {
            $query = $query->whereHas("category", function($q) use ($request) {
                $q->whereIn("id", $request->get("categories"));
            });
        }

        if ($request->has("sectors")) {
            $query = $query->whereHas("sectors", function($q) use ($request) {
                $q->whereIn("id", $request->get("sectors"));
            });
        }

        if ($request->has("states")) {
            $query = $query->whereHas("states", function($q) use ($request) {
                $q->whereIn("id", $request->get("states"));
            });
        }

        if ($request->has("order")) {
            $order = $request->get("order") == "date" ? "publication_date" : "title";
            $way = $request->has("way") && $request->get("way") == "desc" ? "desc" : "asc";
            $query = $query->orderBy($order, $way);
        }
        else{
            $query = $query->orderBy('publication_date', 'desc');
        }

        return $query->get()->toJson();
    }

    public function searchDocuments(Request $request) {
        if ((!$request->has("text_search") || !$textSearch = $request->get("text_search")) && !($request->has("category_id") || $request->has("state_id"))) {
            return $this->getAllDocuments($request);
        }

        $search = [
            'user_id' => \Auth::user()->id,
            'ip_address' => $request->ip(),
        ];

        $query = Document::with(["language", "category", "sectors", "states"])->orderBy('publication_date', 'desc')->orderBy('created_at', 'desc');

        if ($request->has("language_id")) {
            $query = $query->whereHas("language", function($q) use ($request) {
                $q->where("id", $request->get("language_id"));
            });
            $search['language_id'] = $request->get("language_id");
        }

        if ($request->has("category_id")) {
            $query = $query->whereHas("category", function($q) use ($request) {
                $q->where("id", $request->get("category_id"));
            });
            $search['category_id'] = $request->get("category_id");
        }

        if ($request->has("sector_id")) {
            $query = $query->whereHas("sectors", function($q) use ($request) {
                $q->where("id", $request->get("sector_id"));
            });
            $search['sector_id'] = $request->get("sector_id");
        }

        if ($request->has("state_id")) {
            $query = $query->whereHas("states", function($q) use ($request) {
                $q->where("id", $request->get("state_id"));
            });
            $search['state_id'] = $request->get("state_id");
        }

        if (!empty($textSearch)) {
            $search['text_search'] = $request->get("text_search");
            list($words, $textSearch) = $this->processText($textSearch);
            $search['words'] = $words;
        
            $query->where(function ($q0) use($textSearch) {
                $q0->whereRaw( "MATCH (title, description, summary) AGAINST (? IN BOOLEAN MODE)", [$textSearch])
                ->orWhereHas("keywords", function($q) use ($textSearch) {
                    $q->whereRaw( "MATCH (value) AGAINST (? IN BOOLEAN MODE)", [$textSearch]);
                })
                ->orWhereHas("states", function($q) use ($textSearch) {
                    $q->whereRaw( "MATCH (name) AGAINST (? IN BOOLEAN MODE)", [$textSearch]);
                })
                ->orWhereHas("sectors", function($q) use ($textSearch) {
                    $q->whereRaw( "MATCH (name_es, name_en) AGAINST (? IN BOOLEAN MODE)", [$textSearch]);
                });
            });
        }

        $this->storeSearch($search);

        return $query->get()->toJson();
    }

    private function storeSearch($search) {
        $hash = base64_encode(json_encode($search));
        $lastTime = (new \DateTime())->modify('-60 seconds')->format('Y-m-d H:i:s');
        $cnt = Search::where('hash', $hash)->where('created_at', '>=', $lastTime)->count();
        if ($cnt == 0) {
            $wordsId = [];
            $search['hash'] = $hash;
            if (!empty($search['words'])) {
                $words = $search['words'];
                unset($search['words']);

                $created = SearchWord::whereIn('value', $words)->get();
                if ($createdIds = $created->pluck('id')->all()) {
                    $wordsId = $createdIds;
                }

                $createdWords = $created->pluck('value')->all();
                $diff = array_diff($words, $createdWords);
                foreach ($diff as $word) {
                    $newWord = SearchWord::create(['value' => $word]);
                    $wordsId[] = $newWord->id;
                }
            }

            if ($newSearch = Search::create($search)) {
                $newSearch->words()->sync($wordsId);
            }
        }
    }

    public function updateSearch(Request $request) {
        $search = [
            'user_id' => \Auth::user()->id,
            'ip_address' => $request->ip(),
        ];

        if ($request->has("languages")) {
            $languages = $request->get("languages");
            $search['language_id'] = reset($languages);
        }

        if ($request->has("categories")) {
            $categories = $request->get("categories");
            $search['category_id'] = reset($categories);
        }

        if ($request->has("sectors")) {
            $sectors = $request->get("sectors");
            $search['sector_id'] = reset($sectors);
        }

        if ($request->has("states")) {
            $states = $request->get("states");
            $search['state_id'] = reset($states);
        }

        $search['text_search'] = $request->get("text_search");
        list($words, $textSearch) = $this->processText($search['text_search']);
        $search['words'] = $words;        

        $this->storeSearch($search);

        return response()->json(["ok" => 1]);
    }
}
