<?php

namespace App\Http\Controllers;

use App\Document;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;

class HomeController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        SEOMeta::setTitle(__("Búsqueda de documentos"));
        
        /*$categories = Category::orderBy("name_es")->get();
        $languages = Language::orderBy("name")->get();
        $sectors = Sector::orderBy("name_es")->get();
        $states = State::orderBy("name")->get();*/

        $textSearch = $request->get('text_search', '');
        $categoryId = $request->get('category_id', '');
        $stateId = $request->get('state_id', '');

        $isPost = $request->isMethod('post');

        return view('frontoffice.pages.home', compact('textSearch', 'categoryId', 'stateId', 'isPost'));
        //return view('frontoffice.pages.home', compact('categories', 'languages', 'sectors', 'states', 'textSearch', 'categoryId', 'isPost'));
    }

    public function indexAdmin()
    {
        $documents = Document::with(["language", "category"])->orderBy("created_at")->limit(5)->get();
        return view('backoffice.pages.home', compact('documents'));
    }
}
