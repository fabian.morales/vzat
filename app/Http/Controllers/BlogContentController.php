<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use App\BlogContent;
use App\Helpers\ImageHelper;
use App\Helpers\LocaleHelper;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogContentController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'blog_posts';
    }

    public function list() {
        $this->checkPermission('view');

        $posts = []; //BlogContent::where('type', 'blog')->get();
        return view('backoffice.blog_posts.list', compact('posts'));
    }

    public function dataTable() {
        return \DataTables::of(BlogContent::where('type', 'blog'))->make(true);
    }

    public function generateImages(BlogContent $post) {
        ImageHelper::makeImage($post->getOriginalImagePath(), $post->getImagePath('inner'), 770, 450);
        ImageHelper::makeImage($post->getOriginalImagePath(), $post->getImagePath('featured'), 670, 450);
        ImageHelper::makeImage($post->getOriginalImagePath(), $post->getImagePath('thumbnail'), 370, 220);
        ImageHelper::makeImage($post->getOriginalImagePath(), $post->getImagePath('tiny'), 70, 70);
    }

    private function showForm(BlogContent $post, $categories = null, $edit = false) {
        if (empty($post)) {
            $post = new BlogContent();
        }

        if (empty($categories)) {
            $categories = BlogCategory::all();
        }

        return view('backoffice.blog_posts.form', compact('post', 'categories', 'edit'));
    }

    public function create() {
        $this->checkPermission('create');

        return $this->showForm(new BlogContent());
    }

    public function edit($id) {
        $this->checkPermission('edit');

        $post = BlogContent::find($id);
        if (empty($post)) {
            return back()
                ->with(['message_error' => __("Post no encontrado")]);
        }

        $categories = BlogCategory::with(['posts' => function($q) use ($id) {
            $q->where('blog_content_id', $id);
        }])->get();

        return $this->showForm($post, $categories, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');

        $validator = Validator::make($request->all(), [
            'title_es' => 'required',
            'body_es' => 'required',
            'title_en' => 'required',
            'body_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $post = new BlogContent();
        $post->fill($request->all());
        $post->status = (int)$request->get('status', 0);
        $post->type = 'blog';
        $post->weight = 0;
        $post->user_id = \Auth::user()->id;
        //$post->title_en = $post->title_es;
        //$post->body_en = $post->body_es;
        $post->slug = LocaleHelper::hyphenize($post->title_es);

        if ($post->save()) {
            $post->categories()->sync($request->get('categories'));
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = uniqid() . '.' . $file->extension();
        
                $path = ['storage', 'images', 'blog', $post->id];
                $dir = public_path();
                foreach ($path as $p) {
                    $dir .= '/' . $p;
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                }

                $file->move($dir, $filename);
                $post->image = $filename;
                $post->save();

                $this->generateImages($post);
            }

            if ($request->has('save_and_stay')) {
                return redirect()
                    ->route('admin::blog_posts::edit', ['id' => $post->id])
                    ->with(['message_success' => __("Post guardado exitosamente")]);
            }
            else{
                return redirect()
                    ->route('admin::blog_posts::list')
                    ->with(['message_success' => __("Post guardado exitosamente")]);
            }
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar el post")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');

        $validator = Validator::make($request->all(), [
            'title_es' => 'required',
            'body_es' => 'required',
            'title_en' => 'required',
            'body_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $post = BlogContent::find($request->get('id'));
        if (empty($post)) {
            return back()
                ->with(['message_error' => __("Post no encontrado")]);
        }

        $oldImage = $post->image;

        $post->fill($request->all());
        $post->type = 'blog';
        $post->weight = 0;
        $post->user_id = \Auth::user()->id;
        //$post->title_en = $post->title_es;
        //$post->body_en = $post->body_es;
        $post->slug = LocaleHelper::hyphenize($post->title_es);
        $post->status = (int)$request->get('status', 0);

        if ($post->save()) {
            $post->categories()->sync($request->get('categories'));
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = uniqid() . '.' . $file->extension();
        
                $path = ['storage', 'images', 'blog', $post->id];
                $dir = public_path();
                foreach ($path as $p) {
                    $dir .= '/' . $p;
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                }

                $file->move($dir, $filename);
                $post->image = $filename;
                $post->save();

                foreach (glob($dir . "/*" . $oldImage) as $oldFile) {
                    @unlink($oldFile);
                }
                
                $this->generateImages($post);
            }

            if ($request->has('save_and_stay')) {
                return redirect()
                    ->route('admin::blog_posts::edit', ['id' => $post->id])
                    ->with(['message_success' => __("Post actualizado exitosamente")]);
            }
            else{
                return redirect()
                    ->route('admin::blog_posts::list')
                    ->with(['message_success' => __("Post actualizado exitosamente")]);
            }
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar el post")]);
        }
    }

    public function delete($id) {
        $this->checkPermission('delete');

        DB::beginTransaction();

        $post = BlogContent::find($id);
        if (empty($post)) {
            return back()
                ->with(['message_error' => __("Post no encontrado")]);
        }

        $post->categories()->detach();

        if ($post->delete()) {
            DB::commit();
            return redirect()
                ->route('admin::blog_posts::list')
                ->with(['message_success' => __("Post borrado exitosamente")]);
        }
        else{
            DB::rollBack();
            return back()
                ->with(['message_error' => __("No se pudo borrar el post")]);
        }
    }
}
