<?php

namespace App\Http\Controllers;

use App\Category;
use App\Document;
use App\Exports\SummaryReportExport;
use App\Helpers\LocaleHelper;
use App\Sector;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReportController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'reports';
    }

    public function getStatsDocuments(Request $request) {
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';
        $categoryField = LocaleHelper::getLocale() == "es" ? "c.name_es" : "c.name_en";

        $sql = "SELECT a.id, a.title, d.name language, {$categoryField} category,
            sum(case when b.action = 'view' then 1 ELSE 0 END) views, 
            sum(case when b.action = 'download' then 1 ELSE 0 END) downloads
            FROM document a 
            LEFT JOIN document_user_action b ON (a.id = b.document_id)
            INNER JOIN category c ON (a.category_id = c.id)
            INNER JOIN language d ON (a.language_id = d.id)
            where b.created_at between ? and ?
            GROUP by a.id, a.title, d.name, {$categoryField}
            ORDER BY 5, 6, a.title";
        $rows = DB::select($sql, [$startDate, $endDate]);

        if ($request->get('datatable', 0)) {
            return \DataTables::of(collect($rows))->make(true);
        }
        else{
            return json_encode($rows);
        }
    }

    public function getStatsBlog(Request $request) {
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';

        $sql = "SELECT a.id, a.title_es title,
            sum(case when b.action = 'view' then 1 ELSE 0 END) views
            FROM blog_content a 
            LEFT JOIN blog_user_action b ON (a.id = b.blog_content_id)
            where b.created_at between ? and ?
            GROUP by a.id, a.title_es
            ORDER BY 3, a.title_es";
        $rows = DB::select($sql, [$startDate, $endDate]);

        if ($request->get('datatable', 0)) {
            return \DataTables::of(collect($rows))->make(true);
        }
        else{
            return json_encode($rows);
        }
    }

    public function getTrendDocuments(Request $request) {
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';
        $date = $request->get('period', 'daily') == 'daily' ? 'date(b.created_at)' : 'DATE_FORMAT(b.created_at, "%b %Y")';
        $documentId = (int)$request->get('document_id', 0);

        $categoryField = LocaleHelper::getLocale() == "es" ? "c.name_es" : "c.name_en";

        $sql = "SELECT " . $date . " date, a.id, a.title, d.name language, {$categoryField} category,
            sum(case when b.action = 'view' then 1 ELSE 0 END) views, 
            sum(case when b.action = 'download' then 1 ELSE 0 END) downloads
            FROM document a 
            INNER JOIN document_user_action b ON (a.id = b.document_id)
            INNER JOIN category c ON (a.category_id = c.id)
            INNER JOIN language d ON (a.language_id = d.id)
            where a.id = ? and b.created_at between ? and ?
            GROUP by " . $date . ", a.id, a.title, d.name, {$categoryField}
            ORDER BY " . $date . ", 6, 7, a.title";
        $rows = DB::select($sql, [$documentId, $startDate, $endDate]);

        if ($request->get('datatable', 0)) {
            return \DataTables::of(collect($rows))->make(true);
        }
        else{
            return json_encode($rows);
        }
    }

    public function getStatsCategorySearch(Request $request) {
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';

        $sql = "SELECT b.id, b.name_es category, COUNT(a.category_id) cnt
            from search a
            INNER join category b ON (a.category_id = b.id)
            where a.created_at between ? and ?
            GROUP BY b.id, b.name_es
            ORDER BY 1";
        $rows = DB::select($sql, [$startDate, $endDate]);
        
        if ($request->get('datatable', 0)) {
            return \DataTables::of(collect($rows))->make(true);
        }
        else{
            return json_encode($rows);
        }
    }

    public function getStatsWordSearch(Request $request) {
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';

        $sql = "SELECT c.id, c.value word, COUNT(c.value) cnt
            from search a
            INNER JOIN search_search_word b ON (a.id = b.search_id)
            INNER JOIN search_word c ON (b.search_word_id = c.id)
            where a.created_at between ? and ?
            GROUP BY c.id, c.value
            ORDER BY 1";
        $rows = DB::select($sql, [$startDate, $endDate]);

        if ($request->get('datatable', 0)) {
            return \DataTables::of(collect($rows))->make(true);
        }
        else{
            return json_encode($rows);
        }
    }

    public function getUserActivity(Request $request) {
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';
        $userId = (int)$request->get('user_id', 0);

        $sql = "SELECT b.id, b.name, a.ip_address, case when a.action = 'download' then '" . __("Descarga Documento") .  "' ELSE '" . __("Visualiza Documento") . "' END action, a.created_at, c.title info
            FROM document_user_action a
            LEFT JOIN users b ON (b.id = a.user_id)
            LEFT JOIN document c ON (a.document_id = c.id)
            where b.id = ? and a.created_at between ? and ?
            UNION all
                SELECT b.id, b.name, a.ip_address, '" . __("Inicio de sesión") . "', a.created_at, ''
                FROM login_attempt a
                inner JOIN users b ON (a.user_id = b.id)
                where b.id = ? and a.created_at between ? and ?
            UNION ALL
            SELECT b.id, b.name, a.ip_address, case when a.action = 'download' then '" . __("Descarga Post") . "' ELSE '" . __("Visualiza Post") . "' END action, a.created_at, c.title_es info
            FROM blog_user_action a
            LEFT JOIN users b ON (b.id = a.user_id)
            LEFT JOIN blog_content c ON (a.blog_content_id = c.id)
            where b.id = ? and a.created_at between ? and ?
            ORDER BY created_at desc";

        $rows = DB::select($sql, [$userId, $startDate, $endDate, $userId, $startDate, $endDate, $userId, $startDate, $endDate]);

        if ($request->get('datatable', 0)) {
            return \DataTables::of(collect($rows))->make(true);
        }
        else{
            return json_encode($rows);
        }
    }

    public function getUserLogins(Request $request) {
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';
        
        $countryField = LocaleHelper::getLocale() == "es" ? "c.name_es" : "c.name_en";

        $sql = "SELECT a.id, b.id user_id, b.name, a.email, a.created_at, b.entity, b.position, a.result, {$countryField} country
            FROM login_attempt a
            left JOIN users b ON (a.user_id = b.id)
            left join country c on (b.country_id = c.id)
            where a.created_at between ? and ? and b.deleted_at is null
            ORDER BY created_at desc";

        $rows = DB::select($sql, [$startDate, $endDate]);

        if ($request->get('datatable', 0)) {
            return \DataTables::of(collect($rows))->make(true);
        }
        else{
            return json_encode($rows);
        }
    }

    public function getGeneralSearches(Request $request) {
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';

        $sql = "select a.id, a.created_at, a.text_search, a.state_id, b.name state_name
            from search a
            left join state b on (a.state_id = b.id)
            where (a.text_search is not null or a.state_id is not null)
            and a.created_at between ? and ?";
        $rows = DB::select($sql, [$startDate, $endDate]);
        
        if ($request->get('datatable', 0)) {
            return \DataTables::of(collect($rows))->make(true);
        }
        else{
            return json_encode($rows);
        }
    }

    public function getGeneralDownloads(Request $request) {
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';
        
        $categoryField = LocaleHelper::getLocale() == "es" ? "b.name_es" : "b.name_en";
        $sectorField = LocaleHelper::getLocale() == "es" ? "c.name_es" : "c.name_en";

        $sql = "select a.id, a.created_at, a.document_id, d2.title document_name, {$categoryField} category_name, 
            group_concat({$sectorField}) sector_name, group_concat(d.name) state_name
            from document_user_action a
            left join document d2 on (a.document_id = d2.id)
            left join category b on (d2.category_id = b.id)
            left join document_sector ds on (d2.id = ds.document_id)
            left join sector c on (ds.sector_id = c.id)
            left join document_state ds2 on (d2.id = ds2.document_id)
            left join state d on (ds2.state_id = d.id)
            where a.action = 'download' and d2.id is not null and a.created_at between ? and ? and d2.deleted_at is null
            group by a.id, a.document_id, d2.title, b.name_es, a.created_at
            order by a.created_at desc";

        $rows = DB::select($sql, [$startDate, $endDate]);

        if ($request->get('datatable', 0)) {
            return \DataTables::of(collect($rows))->make(true);
        }
        else{
            return json_encode($rows);
        }
    }

    public function showGeneralReports() {
        $reports = [
            'documents_stats' => __('Estadísticas de documentos'),
            'search_word_stats' => __('Estadísticas de palabras buscadas'),
            'search_category_stats' => __('Estadísticas de categorías buscadas'),
            'blog_stats' => __('Estadísticas del blog'),
            'user_logins' => __('Inicios de sesión'),
            'general_searches' => __('Búsquedas'),
            'general_downloads' => __('Reportes descargados'),
        ];

        $columnsReport = [
            'documents_stats' => ['id' => 'ID', 'title' => __('Nombre'), 'language' => __('Idioma'), 'category' => __('Categoría'), 'views' => __('Vistas'), 'downloads' => __('Descargas')],
            'search_word_stats' => ['id' => 'ID', 'word' => __('Palabra'), 'cnt' => __('Cantidad')],
            'search_category_stats' => ['id' => 'ID', 'category' => 'Categoria', 'cnt' => __('Cantidad')],
            'blog_stats' => ['id' => 'ID', 'title' => __('Nombre'), 'views' => __('Vistas')],
            'user_logins' => ['id' => 'ID', 'user_id' => __("Id Usuario"), "name" => __("Nombre"), "email" => __("Correo"), "created_at" => __("Fecha"), "entity" => __("Organización"), "position" => __("Cargo"), "country" => __("País"), "result" => __("Resultado")],
            'general_searches' => ['id' => 'ID', 'created_at' => __("Fecha"), 'text_search' => __("Texto para búsqueda"), 'state_id' => __("Id estado"), "state_name" => __("Nombre")],
            'general_downloads' => ['id' => 'ID', 'created_at' => __("Fecha"), 'document_id' => __("Id documento"), "document_name" => __("Nombre documento"), "category_name" => __("Nombre tipo"), "sector_name" => __("Nombre sector"), "state_name" => __("Nombre estado")],
        ];

        $datasets = [
            'documents_stats' => [
                ['field' => 'title', 'value' => 'views', 'label' => __('Vistas')],
                ['field' => 'title', 'value' => 'downloads', 'label' => __('Descargas')]
            ],
            'search_word_stats' => [
                ['field' => 'word', 'value' => 'cnt', 'label' => __('Cantidad')]
            ],
            'search_category_stats' => [
                ['field' => 'category', 'value' => 'cnt', 'label' => __('Cantidad')]
            ],
            'blog_stats' => [
                ['field' => 'title', 'value' => 'views', 'label' => __('Vistas')],
            ],
            'user_logins' => [],
            'general_searches' => [],
            'general_downloads' => [],
        ];

        return view('backoffice.reports.general', compact('reports', 'columnsReport', 'datasets'));
    }

    public function showDocTrendReport() {
        $documents = Document::all();
        return view('backoffice.reports.document_trends', compact('documents'));
    }

    public function showUserReport() {
        $users = User::all();
        return view('backoffice.reports.user_activity', compact('users'));
    }

    public function downloadReport(Request $request) {
        $functions = [
            'documents_stats' => 'getStatsDocuments',
            'search_word_stats' => 'getStatsWordSearch',
            'search_category_stats' => 'getStatsCategorySearch',
            'blog_stats' => 'getStatsBlog',
            'document_trends' => 'getTrendDocuments',
            'user_activity' => 'getUserActivity',
            'user_logins' => 'getUserLogins',
            'general_searches' => 'getGeneralSearches',
            'general_downloads' => 'getGeneralDownloads',
        ];

        $headers = [
            'documents_stats' => ['ID', __('Nombre'), __('Idioma'), __('Categoría'), __('Vistas'), __('Descargas')],
            'search_word_stats' => ['ID', __('Palabra'), __('Cantidad')],
            'search_category_stats' => ['ID', __('Categoría'), __('Cantidad')],
            'blog_stats' => ['ID', __('Nombre'), __('Vistas')],
            'document_trends' => [__('Fecha'), 'ID', __('Nombre'), __('Idioma'), __('Categoría'), __('Vistas'), __('Descargas')],
            'user_activity' => ['ID',  __('Usuario'), __('Dirección IP'), __('Acción'), __('Fecha'), __('Información')],
            'user_logins' => ['id' => 'ID', 'user_id' => __("Id Usuario"), "name" => __("Nombre"), "email" => __("Correo"), "created_at" => __("Fecha"), "entity" => __("Organización"), "position" => __("Cargo"), "result" => __("Resultado"), "country" => __("País")],
            'general_searches' => ['id' => 'ID', 'created_at' => __("Fecha"), 'text_search' => __("Texto para búsqueda"), 'state_id' => __("Id estado"), "state_name" => __("Nombre")],
            'general_downloads' => ['id' => 'ID', 'created_at' => __("Fecha"), 'document_id' => __("Id documento"), "document_name" => __("Nombre documento"), "category_name" => __("Nombre tipo"), "sector_name" => __("Nombre sector"), "state_name" => __("Nombre estado")],
        ];

        $report = $request->get('report');
        $functionName = $functions[$report];
        $data = json_decode($this->$functionName($request));

        $filename = $report . "_" . date("Ymd_Hi") . ".csv";
        $file = fopen(Storage::path('temp/' . $filename), 'w+');
        fputcsv($file, array_map("utf8_decode", $headers[$report]), ";");
        foreach ($data as $row) {
            fputcsv($file, array_map("utf8_decode", (array)$row), ";");
        }
        fclose($file);

        return Storage::download('temp/' . $filename, $filename);
    }

    public function showSummaryForm() {
        $states = State::orderBy("name")->get();
        $sectors = Sector::orderBy("name_es")->get();
        $categories = Category::orderBy("name_es")->get();

        $type = "";
        $startDate = "";
        $endDate = "";

        $categoryId = "";
        $sectorId = "";
        $stateId = "";

        return view('backoffice.reports.summary', compact('states', 'sectors', 'categories', 'type', 'sectorId', 'categoryId', 'stateId', 'startDate', 'endDate'));
    }

    public function getSummaryReports(Request $request) {
        $type = $request->get('type');
        $startDate = $request->get('start_date') ?? '2000-01-01';
        $endDate = ($request->get('end_date') ?? '2100-12-31') . ' 23:59:59';

        $categoryId = $request->get('category_id');
        $sectorId = $request->get('sector_id');
        $stateId = $request->get('state_id');

        $where = "";
        if (!empty($categoryId)) {
            $where .= " and c.id = " . $categoryId;
        }

        if (!empty($sectorId)) {
            $where .= " and s.id = " . $sectorId;
        }

        if (!empty($stateId)) {
            $where .= " and d.id = " . $stateId;
        }

        $categoryField = LocaleHelper::getLocale() == "es" ? "c.name_es" : "c.name_en";
        $sectorField = LocaleHelper::getLocale() == "es" ? "s.name_es" : "s.name_en";
        $countryField = LocaleHelper::getLocale() == "es" ? "co.name_es" : "co.name_en";

        $tables = [];
        $graphs = [];

        switch($type) {
            case 'user_login':
                $sql = "select count(*) cnt from login_attempt a
                    where a.created_at between ? and ?";
                $attempts = DB::select($sql, [$startDate, $endDate]);

                $sql = "select count(*) cnt from login_attempt a
                    where a.created_at between ? and ? and a.result = 'success'";
                $success = DB::select($sql, [$startDate, $endDate]);

                $sql = "select count(distinct a.user_id) cnt from login_attempt a
                    where a.created_at between ? and ? and a.result = 'success'";
                $users = DB::select($sql, [$startDate, $endDate]);

                $sql = "select coalesce({$countryField}, 'N/A') name, count(*) cnt 
                    from login_attempt a 
                    left join users b on (a.user_id = b.id)
                    left join country co on (b.country_id = co.id)
                    where a.result = 'success' and a.created_at between ? and ?
                    group by {$countryField} 
                    order by 2";
                $countries = DB::select($sql, [$startDate, $endDate]);

                $total = (int)$success[0]->cnt;

                $tables['attempt'] = [
                    'head' => [__("Total de intentos")],
                    'title' => __('Intentos de inicio de sesión'),
                    'data' => [],
                ];
                foreach ($attempts as $attempt) {
                    $tables['attempt']['data'][] = $attempt;
                }

                $tables['success'] = [
                    'head' => [__("Total de intentos exitosos")],
                    'title' => __('Intentos de inicio de sesión exitosos'),
                    'data' => [],
                ];
                foreach ($success as $item) {
                    $tables['success']['data'][] = $item;
                }

                $tables['user'] = [
                    'head' => [__("Total de usuarios")],
                    'title' => __('Usuarios únicos que iniciaron sesión'),
                    'data' => [],
                ];
                foreach ($users as $user) {
                    $tables['user']['data'][] = $user;
                }

                $tables['country'] = [
                    'head' => [__("Nombre"), "#", "%"],
                    'title' => __('Países'),
                    'data' => [],
                ];
                $graphs['country'] = ['title' => __('Países')];
                foreach ($countries as $country) {
                    $country->percent = $country->cnt / $total * 100;
                    $tables['country']['data'][] = $country;
                    $graphs['country']['data'][] = $country->cnt;
                    $graphs['country']['labels'][] = $country->name;
                }

                break;
            case 'search':
                $sql = "select c.id, {$categoryField} name, count(*) cnt 
                    from search a
                    left join category c on (a.category_id = c.id)
                    left join sector s on (a.sector_id = s.id)
                    left join state d on (a.state_id = d.id)
                    where c.id is not null and a.created_at between ? and ? {$where}
                    group by c.id, {$categoryField}";
                $categories = DB::select($sql, [$startDate, $endDate]);

                $sql = "select s.id, {$sectorField} name, count(*) cnt 
                    from search a
                    left join category c on (a.category_id = c.id)
                    left join sector s on (a.sector_id = s.id)
                    left join state d on (a.state_id = d.id)
                    where s.id is not null and a.created_at between ? and ? {$where}
                    group by s.id, {$sectorField}";
                $sectors = DB::select($sql, [$startDate, $endDate]);

                $sql = "select d.id, d.name, count(*) cnt 
                    from search a
                    left join state d on (a.state_id = d.id)
                    left join category c on (a.category_id = c.id)
                    left join sector s on (a.sector_id = s.id)
                    where d.id is not null and a.created_at between ? and ? {$where}
                    group by d.id, d.name";
                $states = DB::select($sql, [$startDate, $endDate]);

                $sql = "select text_search, count(*) cnt 
                    from search a 
                    left join state d on (a.state_id = d.id)
                    left join category c on (a.category_id = c.id)
                    left join sector s on (a.sector_id = s.id)
                    where text_search is not null and a.created_at between ? and ? {$where}
                    group by text_search 
                    order by 2 desc 
                    limit 0, 10";
                $terms = DB::select($sql, [$startDate, $endDate]);

                $sql = "select count(*) cnt 
                    from search a 
                    left join state d on (a.state_id = d.id)
                    left join category c on (a.category_id = c.id)
                    left join sector s on (a.sector_id = s.id)
                    where a.created_at between ? and ? {$where}";
                $general = DB::select($sql, [$startDate, $endDate]);
                $total = (int)$general[0]->cnt;

                $tables['category'] = [
                    'head' => ['ID', __("Nombre"), "#", "%"],
                    'title' => __('Tipos'),
                    'data' => [],
                ];
                $graphs['category'] = ['title' => __('Tipos')];
                foreach ($categories as $category) {
                    $category->percent = $category->cnt / $total * 100;
                    $tables['category']['data'][] = $category;
                    $graphs['category']['data'][] = $category->cnt;
                    $graphs['category']['labels'][] = $category->name;
                }

                $tables['sector'] = [
                    'head' => ['ID', __("Nombre"), "#", "%"],
                    'title' => __('Sectores'),
                    'data' => [],
                ];
                $graphs['sector'] = ['title' => __('Sectores')];
                foreach ($sectors as $sector) {
                    $sector->percent = $sector->cnt / $total * 100;
                    $tables['sector']['data'][] = $sector;
                    $graphs['sector']['data'][] = $sector->cnt;
                    $graphs['sector']['labels'][] = $sector->name;
                }

                $tables['state'] = [
                    'head' => ['ID', __("Nombre"), "#", "%"],
                    'title' => __('Estados'),
                    'data' => [],
                ];
                $graphs['state'] = ['title' => __('Estados')];
                foreach ($states as $state) {
                    $state->percent = $state->cnt / $total * 100;
                    $tables['state']['data'][] = $state;
                    $graphs['state']['data'][] = $state->cnt;
                    $graphs['state']['labels'][] = $state->name;
                }

                $tables['term'] = [
                    'head' => [__("Nombre"), "#"],
                    'title' => __('Términos más buscados'),
                    'data' => [],
                ];
                foreach ($terms as $term) {
                    $tables['term']['data'][] = $term;
                }

                $tables['general'] = [
                    'head' => [__("Total búsquedas")],
                    'title' => __('Total de búsquedas'),
                    'data' => [],
                ];
                foreach ($general as $item) {
                    $tables['general']['data'][] = $item;
                }

                break;
            case 'document_download':
                $sql = "select c.id, {$categoryField} name, count(distinct a.id) cnt 
                    from document_user_action a
                    left join document d2 on (a.document_id = d2.id)
                    left join category c on (d2.category_id = c.id)
                    left join document_sector ds on (d2.id = ds.document_id)
                    left join sector s on (ds.sector_id = s.id)
                    left join document_state ds2 on (s.id = ds2.document_id)
                    left join state d on (ds2.state_id = d.id)
                    where c.id is not null and a.created_at between ? and ? and action = 'download' {$where}
                    group by c.id, {$categoryField}";
                $categories = DB::select($sql, [$startDate, $endDate]);

                $sql = "select s.id, {$sectorField} name, count(distinct a.id) cnt
                    from document_user_action a
                    left join document d2 on (a.document_id = d2.id)
                    left join category c on (d2.category_id = c.id)
                    left join document_sector ds on (d2.id = ds.document_id)
                    left join sector s on (ds.sector_id = s.id)
                    left join document_state ds2 on (s.id = ds2.document_id)
                    left join state d on (ds2.state_id = d.id)
                    where s.id is not null and a.created_at between ? and ? and action = 'download' {$where}
                    group by s.id, {$sectorField}";
                $sectors = DB::select($sql, [$startDate, $endDate]);

                $sql = "select d.id, d.name, count(distinct a.id) cnt 
                    from document_user_action a
                    left join document d2 on (a.document_id = d2.id)
                    left join category c on (d2.category_id = c.id)
                    left join document_sector ds on (d2.id = ds.document_id)
                    left join sector s on (ds.sector_id = s.id)
                    left join document_state ds2 on (s.id = ds2.document_id)
                    left join state d on (ds2.state_id = d.id)
                    where d.id is not null and a.created_at between ? and ? and action = 'download' {$where}
                    group by d.id, d.name";
                $states = DB::select($sql, [$startDate, $endDate]);

                $sql = "select count(distinct a.id) cnt 
                    from document_user_action a
                    left join document d2 on (a.document_id = d2.id)
                    left join category c on (d2.category_id = c.id)
                    left join document_sector ds on (d2.id = ds.document_id)
                    left join sector s on (ds.sector_id = s.id)
                    left join document_state ds2 on (s.id = ds2.document_id)
                    left join state d on (ds2.state_id = d.id)
                    where a.created_at between ? and ? and action = 'download' {$where}";
                $general = DB::select($sql, [$startDate, $endDate]);

                $sql = "select count(distinct d2.id) cnt 
                    from document_user_action a
                    left join document d2 on (a.document_id = d2.id)
                    left join category c on (d2.category_id = c.id)
                    left join document_sector ds on (d2.id = ds.document_id)
                    left join sector s on (ds.sector_id = s.id)
                    left join document_state ds2 on (s.id = ds2.document_id)
                    left join state d on (ds2.state_id = d.id)
                    where a.created_at between ? and ? and action = 'download' {$where}";
                $documents = DB::select($sql, [$startDate, $endDate]);
                
                $total = (int)$general[0]->cnt;

                $tables['category'] = [
                    'head' => ['ID', __("Nombre"), "#", "%"],
                    'title' => __('Tipos'),
                    'data' => [],
                ];
                $graphs['category'] = ['title' => __('Tipos')];
                foreach ($categories as $category) {
                    $category->percent = $category->cnt / $total * 100;
                    $tables['category']['data'][] = $category;
                    $graphs['category']['data'][] = $category->cnt;
                    $graphs['category']['labels'][] = $category->name;
                }

                $tables['sector'] = [
                    'head' => ['ID', __("Nombre"), "#", "%"],
                    'title' => __('Sectores'),
                    'data' => [],
                ];
                $graphs['sector'] = ['title' => __('Sectores')];
                foreach ($sectors as $sector) {
                    $sector->percent = $sector->cnt / $total * 100;
                    $tables['sector']['data'][] = $sector;
                    $graphs['sector']['data'][] = $sector->cnt;
                    $graphs['sector']['labels'][] = $sector->name;
                }

                $tables['state'] = [
                    'head' => ['ID', __("Nombre"), "#", "%"],
                    'title' => __('Estados'),
                    'data' => [],
                ];
                $graphs['state'] = ['title' => __('Estados')];
                foreach ($states as $state) {
                    $state->percent = $state->cnt / $total * 100;
                    $tables['state']['data'][] = $state;
                    $graphs['state']['data'][] = $state->cnt;
                    $graphs['state']['labels'][] = $state->name;
                }

                $tables['general'] = [
                    'head' => [__("Total descargas")],
                    'title' => __('Total de descargas'),
                    'data' => [],
                ];
                foreach ($general as $item) {
                    $tables['general']['data'][] = $item;
                }

                $tables['document'] = [
                    'head' => [__("Total documentos")],
                    'title' => __('Total de documentos únicos descargados'),
                    'data' => [],
                ];
                foreach ($documents as $document) {
                    $tables['document']['data'][] = $item;
                }
                break;
        }

        $graphs = collect(array_values($graphs));

        $states = State::orderBy("name")->get();
        $sectors = Sector::orderBy("name_es")->get();
        $categories = Category::orderBy("name_es")->get();

        switch (true) {
            case $request->has('download_csv'):
                $filename = $type . "_" . date("Ymd_Hi") . ".csv";
                $file = fopen(Storage::path('temp/' . $filename), 'w+');
    
                foreach ($tables as $table) {
                    fputcsv($file, [utf8_decode($table["title"])], ";");
                    fputcsv($file, array_map("utf8_decode", $table['head']), ";");
                    foreach ($table["data"] as $row) {
                        fputcsv($file, array_map("utf8_decode", (array)$row), ";");
                    }
        
                    fputcsv($file, [""], ";");
                }
    
                fclose($file);
    
                return Storage::download('temp/' . $filename, $filename);
                break;

            case $request->has('download_xlsx'):
                $filename = $type . "_" . date("Ymd_Hi") . ".xlsx";
                return \Excel::download(new SummaryReportExport($tables), $filename);
                break;

            default:
                return view('backoffice.reports.summary', compact('states', 'sectors', 'categories', 'tables', 'graphs', 'type', 'sectorId', 'categoryId', 'stateId', 'startDate', 'endDate'));
        }
    }
}
