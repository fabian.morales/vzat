<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Country;
use App\Mail\ContactNotifyAdmin;
use App\Mail\ContactNotifyUser;
use App\Rules\Recaptcha;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function showContactPage() {
        SEOMeta::setTitle(__("Contacto"));
        $countries = Country::orderBY("name_es")->get();
        return view("frontoffice.pages.contact", compact('countries'));
    }

    public function saveContact(Request $request) {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'entity' => 'required',
            'country_id' => 'required',
            'message' => 'required',
        ];            

        if (App::environment(['staging', 'production'])) {
            $rules['g-recaptcha-response'] = ['required', new Recaptcha];
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $contact = new Contact();
        $contact->fill($request->all());
        if ($contact->save()) {
            Mail::to($contact->email)->send(new ContactNotifyUser($contact));
            Mail::to(env('MAIL_ADMIN_TO'))->send(new ContactNotifyAdmin($contact));
            
            return back()
                ->with(['message_success' => __("Su mensaje ha sido enviado exitosamente, nos pondremos en contacto lo más pronto posible.")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("Hubo en error al momento de enviar los datos, por favor inténtelo de nuevo.")]);
        }
    }
}
