<?php

namespace App\Http\Controllers;

use App\Category;
use App\Document;
use App\DocumentUserAction;
use App\State;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ViewDocumentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function download(Request $request, $file) {
        $document = Document::where("file", $file)->first();

        if (empty($document)) {
            return back()
                ->with(['message_error' => __('No hay ningún documento con esas características.')]);
        }

        $userAction = $request->has('view') ? 'view' : 'download';

        if ($userAction == 'view') {
            $document->views += 1;
        }
        else {
            $document->downloads += 1;
        }
        
        $document->save();

        $action = new DocumentUserAction();
        $action->ip_address = $request->ip();
        $action->action = $userAction;
        $action->user_id = \Auth::user()->id;
        $action->document_id = $document->id;
        $action->save();

        return Storage::download('documents/' . $document->file, $document->file_name);
    }

    public function showPdfViewer() {
        return view('frontoffice.documents.pdf_viewer');
    }

    public function view(Request $request, $id) {
        $document = Document::with(['category', 'language'])->where("id", $id)->first();

        if (empty($document)) {
            return redirect()
                ->route('home')
                ->with(['message_error' => __('No hay ningún documento con esas características.')]);
        }

        SEOMeta::setTitle($document->title);

        //$categories = Category::orderBy("name_es")->get();
        $states = State::orderBy("name")->get();

        return view('frontoffice.documents.view', compact('states', 'document'));
    }

    public function getRelatedDocuments($categoryId) {
        $related = Document::with(['category', 'language'])->where('category_id', $categoryId)->inRandomOrder()->take(3)->get();
        return $related->toJson();
    }
}
