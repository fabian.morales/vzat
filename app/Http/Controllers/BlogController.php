<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use App\BlogContent;
use App\BlogUserAction;
use App\Category;
use App\Search;
use App\Traits\ContentTrait;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    use ContentTrait;

    public function showHome() {
        $posts = BlogContent::with(['categories', 'author'])->isPost()->active()->orderBy('created_at', 'desc')->limit(6)->get();
        $lastPost = BlogContent::with(['categories', 'author'])->isPost()->active()->orderBy('created_at', 'desc')->limit(1)->first();
        $categories = BlogCategory::with(['posts' => function($q) {
                $q->active()->orderBy('created_at', 'desc');
            }, 'posts.author', 'posts.categories'])
            ->whereHas('posts')
            ->orderBy('name_es')->get();
        $docCategories = Category::orderBy('name_es')->get();

        SEOMeta::setTitle(__("Blog & Noticias"));
        SEOMeta::setDescription("");

        OpenGraph::setDescription("");
        OpenGraph::setTitle(__("Blog & Noticias"));

        return view('frontoffice.blog.home', compact('posts', 'lastPost', 'categories', 'docCategories'));
    }

    public function showAllPosts() {
        $posts = BlogContent::isPost()->active()->with(['categories', 'author'])->orderBy('created_at', 'desc')->get();
        $categories = []; //CategoriaBlog::orderBy('name_es')->get();
        $docCategories = Category::orderBy('name_es')->get();
        return view('frontoffice.blog.all', compact('posts', 'categories', 'docCategories'));
    }

    public function showCategoryPosts($id, $slug) {
        $category = BlogCategory::where('id', $id)->where('slug', $slug)->with([
            'posts' => function($q) {
                $q->active();
            }, 
            'posts.categories', 'posts.author'
        ])->first();
        if (empty($category)) {
            return back()
                ->with(['message_error' => __("Categoría no encontrada")]);
        }

        $categories = []; //CategoriaBlog::orderBy('name_es')->get();
        $docCategories = Category::orderBy('name_es')->get();

        $category->views += 1;
        $category->save();

        return view('frontoffice.blog.category', compact('category', 'categories', 'docCategories'));
    }

    public function showPost(Request $request, $id, $slug) {
        $post = BlogContent::where('id', $id)->where('slug', $slug)->active()->isPost()->with(['categories', 'author'])->first();
        
        if (empty($post)) {
            return back()
                ->with(['message_error' => __("Post no encontrado")]);
        }

        $categories = BlogCategory::whereHas('posts', 
            function($q) {
                $q->active();
            })
            ->orderBy('name_es')
            ->withCount([
                'posts'  => function($q) {
                    $q->active();
                }
            ])
            ->get();

        $docCategories = Category::orderBy('name_es')->get();
        $recents = BlogContent::where('id', '<>', $id)->active()->orderBy('created_at', 'desc')->limit(5)->get();
        $related = [];
        if (!empty($post->categories)) {
            $postCategories = $post->categories->pluck('id')->all();
            $related = BlogContent::where('id', '<>', $id)
                ->active()
                ->whereHas('categories', function($q) use ($postCategories) {
                    $q->whereIn('blog_category.id', $postCategories);
                })
                ->with(['categories', 'author'])
                ->limit(3)
                ->orderBy("created_at", "desc")
                ->get();
        }

        $desc = $post->getDescription();
        SEOMeta::setTitle($post->title());
        SEOMeta::setDescription($desc);
        //SEOMeta::setCanonical('https://codecasts.com.br/lesson');

        OpenGraph::setDescription($desc);
        OpenGraph::setTitle($post->title());
        OpenGraph::setUrl(url()->current());
        OpenGraph::addProperty('type', 'articles');

        TwitterCard::setTitle($post->title());

        $html = $this->getHtmlSection($post, $id);

        $post->views += 1;
        $post->save();

        if (\Auth::check()) {
            $action = new BlogUserAction();
            $action->ip_address = $request->ip();
            $action->action = 'view';
            $action->user_id = \Auth::user()->id;
            $action->blog_content_id = $post->id;
            $action->save();
        }

        return view('frontoffice.blog.post', compact('post', 'html', 'categories', 'docCategories', 'related', 'recents'));
    }
}
