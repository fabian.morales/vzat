<?php

namespace App\Http\Controllers;

use App\Mail\ApprovalUserNotify;
use App\Mail\DisapprovalUserNotify;
use App\Mail\VerificationLinkSend;
use App\User;
use App\UserVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class UserApprovalController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'users_approval';
    }

    private function createVerificationLink(User $user) {
        $user->verificationLinks()->update(['valid' => 0]);
        $token = uniqid() . uniqid();
        $verification = new UserVerification();
        $verification->user_id = $user->id;
        $verification->token = $token;
        $verification->valid = 1;
        $verification->save();

        return $verification;
    }

    public function showIndex() {
        $this->checkPermission('view');
        return view('backoffice.users.approval_list');
        //$users = User::where('approved', 1)->orderBy('created_at')->get();
    }

    public function returnDataTable() {
        $this->checkPermission('view');
        return \DataTables::of(User::where('approved', 0)->orderBy('created_at'))->make(true);
    }

    public function saveApproval(Request $request) {
        DB::beginTransaction();
        $this->checkPermission('edit');

        try {
            $action = $request->get('action');
            if(!in_array($action, ['approve', 'disapprove'])) {
                throw new \Exception(__("Acción no válida"));
            }
    
            $ids = $request->get('users');
            $actionValue = $action == 'disapprove' ? 2 : 1;
            $users = User::whereIn('id', $ids)->where('approved', 0)->get();
            
            foreach ($users as $user) {
                if ($user->approvated != 0 || $user->validated == 1) {
                    continue;
                }

                $user->approved = $actionValue;
                $user->reject_reason = $request->get('reject_reason');
                if (!$user->save()) {
                    throw new \Exception(__('No se pudo aplicar la acción'));
                }

                if ($actionValue == 1) {
                    $verification = $this->createVerificationLink($user);
                    Mail::to($user->email)->send(new ApprovalUserNotify($user, $verification->token));
                }
                else{
                    Mail::to($user->email)->send(new DisapprovalUserNotify($user));
                }
            }

            DB::commit();
            return response()->json([
                'ok' => '1',
                'message' => __('Acción aplicada correctamente'),
            ]);
        } 
        catch (\Exception $th) {
            DB::rollBack();
            return response()->json([
                'ok' => '0',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function sendVerificationLink($userId) {
        $this->checkPermission('edit');
        
        try {
            $user = User::find($userId);
            if (empty($user)) {
                throw new \Exception(__("Usuario no encontrado"));
            }

            if ($user->approved != 1) {
                throw new \Exception(__("Este usuario no ha sido aprobado"));
            }

            if ($user->validated == 1) {
                throw new \Exception(__("Este usuario ya está verificado."));
            }

            $verification = $this->createVerificationLink($user);
            Mail::to($user->email)->send(new VerificationLinkSend($user, $verification->token));

            return response()->json([
                'ok' => '1',
                'message' => __('Correo de verificación enviado'),
            ]);
        } 
        catch (\Exception $th) {
            return response()->json([
                'ok' => '0',
                'message' => $th->getMessage(),
            ]);
        }
    }
}
