<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CountryController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'countries';
    }

    public function list() {
        $this->checkPermission('view');

        $countries = Country::all();
        return view('backoffice.countries.list', compact('countries'));
    }

    private function showForm(Country $country, $edit = false) {
        if (empty($country)) {
            $country = new Country();
        }

        return view('backoffice.countries.form', compact('country', 'edit'));
    }

    public function create() {
        $this->checkPermission('create');

        return $this->showForm(new Country());
    }

    public function edit($id) {
        $this->checkPermission('edit');

        $country = Country::find($id);
        if (empty($country)) {
            return back()
                ->with(['message_error' => __("País no encontrado")]);
        }

        return $this->showForm($country, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');

        $validator = Validator::make($request->all(), [
            'name_es' => 'required',
            'name_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $country = new Country();
        $country->fill($request->all());
        //$country->name_en = $country->name_es;

        if ($country->save()) {
            return redirect()
                ->route('admin::countries::list')
                ->with(['message_success' => __("País guardado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar el país")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');

        $validator = Validator::make($request->all(), [
            'name_es' => 'required',
            'name_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $country = Country::find($request->get('id'));
        if (empty($country)) {
            return back()
                ->with(['message_error' => __("País no encontrado")]);
        }

        $country->fill($request->all());
        //$country->name_en = $country->name_es;
        
        if ($country->save()) {
            return redirect()
                ->route('admin::countries::list')
                ->with(['message_success' => __("País actualizado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar el país")]);
        }
    }
}
