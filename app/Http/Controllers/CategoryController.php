<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CategoryController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'categories';
    }

    public function list() {
        $this->checkPermission('view');

        $categories = Category::all();
        return view('backoffice.categories.list', compact('categories'));
    }

    private function showForm(Category $category, $edit = false) {
        if (empty($category)) {
            $category = new Category();
        }

        return view('backoffice.categories.form', compact('category', 'edit'));
    }

    public function create() {
        $this->checkPermission('create');

        return $this->showForm(new Category());
    }

    public function edit($id) {
        $this->checkPermission('edit');

        $category = Category::find($id);
        if (empty($category)) {
            return back()
                ->with(['message_error' => __("Categoría no encontrada")]);
        }

        return $this->showForm($category, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');

        $validator = Validator::make($request->all(), [
            'name_es' => 'required',
            'name_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $category = new Category();
        $category->fill($request->all());
        //$category->name_en = $category->name_es;

        if ($category->save()) {
            return redirect()
                ->route('admin::categories::list')
                ->with(['message_success' => __("Categoría guardada exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar la categoría")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');

        $validator = Validator::make($request->all(), [
            'name_es' => 'required',
            'name_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $category = Category::find($request->get('id'));
        if (empty($category)) {
            return back()
                ->with(['message_error' => __("Categoría no encontrada")]);
        }

        $category->fill($request->all());
        //$category->name_en = $category->name_es;
        
        if ($category->save()) {
            return redirect()
                ->route('admin::categories::list')
                ->with(['message_success' => __("Categoría actualizada exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar la categoría")]);
        }
    }

    public function delete($id) {
        $this->checkPermission('delete');

        DB::beginTransaction();

        try {
            $category = Category::find($id);
            if (empty($category)) {
                return back()
                    ->with(['message_error' => __("Categoría no encontrada")]);
            }
    
            if ($category->delete()) {
                DB::commit();
                return redirect()
                    ->route('admin::categories::list')
                    ->with(['message_success' => __("Categoría borrada exitosamente")]);
            }
            else{
                DB::rollBack();
                return back()
                    ->with(['message_error' => __("No se pudo borrar la categoría")]);
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()
                ->with(['message_error' => __("La categoría no puede ser borrada")]);
        }
    }
}
