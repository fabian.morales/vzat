<?php

namespace App\Http\Controllers;

use App\AgeRange;
use App\Country;
use App\Gender;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'users';
    }
    
    public function list() {
        $this->checkPermission('view');
        $users = User::all();
        return view('backoffice.users.list', ['users' => $users]);
    }

    public function dataTable(Request $request) {
        $query = User::query();
        $approvalStatus = $request->get('searchByApproved', '');
        if ($approvalStatus != '') {
            $query = $query->where('approved', $request->get('searchByApproved'));
        }

        return \DataTables::of($query)->make(true);
    }

    private function showForm(User $user, $edit = false) {
        if (empty($user)) {
            $user = new User();
        }

        $roles = Role::orderBy('name')->get();
        $countries = Country::orderBy('name_es')->get();
        $genders = Gender::all();
        $ageRanges = AgeRange::all();

        return view('backoffice.users.form', compact('user', 'edit', 'roles', 'countries', 'genders', 'ageRanges'));
    }

    public function create() {
        $this->checkPermission('create');
        return $this->showForm(new User());
    }

    public function edit($id) {
        $this->checkPermission('edit');
        $user = User::find($id);
        if (empty($user)) {
            return back()
                ->with(['message_error' => __("Usuario no encontrado")]);
        }

        return $this->showForm($user, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'country_id' => 'required',
            'role_id' => 'required',
            'entity' => 'required',
            'position' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($request->get('password'));
        $user->approved = $request->get('approved', 0);
        $user->validated = $request->get('validated', 0);
        $user->email_verified_at = $user->validated > 0 ? date('Y-m-d H:i:s') : null;
        $user->accept_terms = $request->get('accept_terms') ?? 'No';
        $user->receive_emails = $request->get('receive_emails') ?? 'No';
        $user->last_ip_addr = "";
        $user->reject_reason = $request->get('reject_reason');

        $user->gender_id = $request->get('gender_id') ?: null;
        $user->age_range_id = $request->get('age_range_id') ?: null;

        if ($user->save()) {
            $role = Role::findById($user->role_id);
            $user->assignRole($role);

            return redirect()
                ->route('admin::users::list')
                ->with(['message_success' => __("Usuario guardado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar el usuario")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'country_id' => 'required',
            'role_id' => 'required',
            'entity' => 'required',
            'position' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::find($request->get('id'));
        if (empty($user)) {
            return back()
                ->with(['message_error' => __("Usuario no encontrado")]);
        }

        $password = $user->password;
        $user->fill($request->all());
        $user->password = $password;
        $approved = $request->get('approved', 0);
        if ($approved == 1 || ($user->approved != 2)) {
            $user->approved = $approved;
        }
        
        $user->validated = $request->get('validated', 0);
        $user->email_verified_at = $user->validated > 0 ? date('Y-m-d H:i:s') : null;
        $user->accept_terms = $request->get('accept_terms') ?? 'No';
        $user->receive_emails = $request->get('receive_emails') ?? 'No';
        $user->last_ip_addr = $user->last_ip_addr ?? "";
        $user->reject_reason = $request->get('reject_reason');

        $user->gender_id = $request->get('gender_id') ?: null;
        $user->age_range_id = $request->get('age_range_id') ?: null;

        if (User::where('email', $user->email)->where('id', '<>', $user->id)->count() > 0) {
            return back()
                ->withInput()
                ->with(['message_error' => __("El correo electrónico que está usando ya está registrado.")]);
        }

        if ($request->has('password') && !empty($request->get('password'))) {
            $user->password = Hash::make($request->get('password'));
        }

        if ($user->save()) {
            $role = Role::findById($user->role_id);

            if (!$user->hasRole($role)) {
                $user->syncRoles([$role]);
            }

            return redirect()
                ->route('admin::users::list')
                ->with(['message_success' => __("Usuario actualizado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar el usuario")]);
        }
    }

    public function delete($id) {
        $this->checkPermission('delete');

        DB::beginTransaction();

        $user = User::find($id);
        if (empty($user)) {
            return back()
                ->with(['message_error' => __("Usuario no encontrado")]);
        }

        $user->sessions()->delete();
        $user->verificationLinks()->delete();

        $user->name = "Deleted";
        $user->email = uniqid() . "@nomail.edu.co";
        $user->email_reason = "";
        $user->role_id = null;
        $user->country_id = null;
        $user->approved = 0;
        $user->validated = 0;
        $user->position = "";
        $user->entity = "";
        $user->email_verified_at = null;
        $user->save();

        if ($roles = $user->getRoleNames()) {
            foreach ($roles as $role) {
                $user->removeRole($role);
            }
        }

        if ($user->delete()) {
            DB::commit();
            return redirect()
                ->route('admin::users::list')
                ->with(['message_success' => __("Usuario borrado exitosamente")]);
        }
        else{
            DB::rollBack();
            return back()
                ->with(['message_error' => __("No se pudo borrar el usuario")]);
        }
    }

    public function downloadUserList() {
        $headers = [
            __("Nombre"),
            __("Correo"),
            __("Razón del correo"),
            __("Razón del rechazo"),
            __("País"),
            __("Aprobado"),
            __("Validado"),
            __("Cargo"),
            __("Organización"),
            __("Género"),
            __("Rango de edad"),
            __("Acepta términos y condiciones"),
            __("Acepta recibir correos de VZAT"),
        ];

        $approvedStatus = [
            0 => __("Pendiente"),
            1 => __("Aprobado"),
            2 => __("Rechazado")
        ];

        $users = User::with(['country', 'gender', 'ageRange'])->get();

        $filename = "UserList_" . date("Ymd_Hi") . ".csv";
        $file = fopen(Storage::path('temp/' . $filename), 'w+');
        fputcsv($file, array_map("utf8_decode", $headers), ";");
        foreach ($users as $user) {
            $data = [
                $user->name,
                $user->email,
                $user->email_reason,
                $user->reject_reason,
                !empty($user->country) ? $user->country->name() : '',
                $approvedStatus[$user->approved] ?? __("Pendiente"),
                $user->validated == 1 ? __("Si") : __("No"),
                $user->position,
                $user->entity,
                !empty($user->gender) ? $user->gender->name() : '',
                !empty($user->ageRange) ? $user->ageRange->name() : '',
                $user->accept_terms,
                $user->receive_emails,
            ];
            fputcsv($file, array_map("utf8_decode", $data), ";");
        }
        fclose($file);

        return Storage::download('temp/' . $filename, $filename);
    }
}
