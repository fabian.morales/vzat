<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\BlogCategory;
use App\Helpers\LocaleHelper;
use Illuminate\Support\Facades\DB;

class BlogCategoryController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'blog_categories';
    }

    public function list() {
        $this->checkPermission('view');

        $categories = BlogCategory::all();
        return view('backoffice.blog_categories.list', compact('categories'));
    }

    private function showForm(BlogCategory $category, $edit = false) {
        if (empty($category)) {
            $category = new BlogCategory();
        }

        return view('backoffice.blog_categories.form', compact('category', 'edit'));
    }

    public function create() {
        $this->checkPermission('create');

        return $this->showForm(new BlogCategory());
    }

    public function edit($id) {
        $this->checkPermission('edit');

        $category = BlogCategory::find($id);
        if (empty($category)) {
            return back()
                ->with(['message_error' => __("Categoría no encontrada")]);
        }

        return $this->showForm($category, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');

        $validator = Validator::make($request->all(), [
            'name_es' => 'required',
            'name_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $category = new BlogCategory();
        $category->fill($request->all());
        //$category->name_en = $category->name_es;
        $category->slug = LocaleHelper::hyphenize($category->name_es);

        if ($category->save()) {
            return redirect()
                ->route('admin::blog_categories::list')
                ->with(['message_success' => __("Categoría guardada exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar la categoría")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');

        $validator = Validator::make($request->all(), [
            'name_es' => 'required',
            'name_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $category = BlogCategory::find($request->get('id'));
        if (empty($category)) {
            return back()
                ->with(['message_error' => __("Categoría no encontrada")]);
        }

        $category->fill($request->all());
        //$category->name_en = $category->name_es;
        $category->slug = LocaleHelper::hyphenize($category->name_es);

        if ($category->save()) {
            return redirect()
                ->route('admin::blog_categories::list')
                ->with(['message_success' => __("Categoría actualizada exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar la categoría")]);
        }
    }

    public function delete($id) {
        $this->checkPermission('delete');

        DB::beginTransaction();

        $category = BlogCategory::find($id);
        if (empty($category)) {
            return back()
                ->with(['message_error' => __("Categoría no encontrada")]);
        }

        $category->posts()->detach();

        if ($category->delete()) {
            DB::commit();
            return redirect()
                ->route('admin::blog_categories::list')
                ->with(['message_success' => __("Categoría borrada exitosamente")]);
        }
        else{
            DB::rollBack();
            return back()
                ->with(['message_error' => __("No se pudo borrar la categoría")]);
        }
    }
}
