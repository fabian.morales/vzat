<?php

namespace App\Http\Controllers;

use App\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SectorController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'sectors';
    }

    public function list() {
        $this->checkPermission('view');

        $sectors = Sector::all();
        return view('backoffice.sectors.list', compact('sectors'));
    }

    private function showForm(Sector $sector, $edit = false) {
        if (empty($sector)) {
            $sector = new Sector();
        }

        return view('backoffice.sectors.form', compact('sector', 'edit'));
    }

    public function create() {
        $this->checkPermission('create');

        return $this->showForm(new Sector());
    }

    public function edit($id) {
        $this->checkPermission('edit');

        $sector = Sector::find($id);
        if (empty($sector)) {
            return back()
                ->with(['message_error' => __("Sector no encontrado")]);
        }

        return $this->showForm($sector, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');

        $validator = Validator::make($request->all(), [
            'name_es' => 'required',
            'name_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $sector = new Sector();
        $sector->fill($request->all());
        //$sector->name_en = $sector->name_es;

        if ($sector->save()) {
            return redirect()
                ->route('admin::sectors::list')
                ->with(['message_success' => __("Sector guardado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar el sector")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');

        $validator = Validator::make($request->all(), [
            'name_es' => 'required',
            'name_en' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $sector = Sector::find($request->get('id'));
        //$sector->name_en = $sector->name_es;
        
        if (empty($sector)) {
            return back()
                ->with(['message_error' => __("Sector no encontrado")]);
        }

        $sector->fill($request->all());
        if ($sector->save()) {
            return redirect()
                ->route('admin::sectors::list')
                ->with(['message_success' => __("Sector actualizado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar el sector")]);
        }
    }
}
