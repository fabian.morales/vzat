<?php

namespace App;

use App\Helpers\LocaleHelper;
use App\Traits\MultiLanguageFieldTrait;
use Illuminate\Database\Eloquent\Model;


class BlogCategory extends Model
{
    use MultiLanguageFieldTrait;
    protected $table = 'blog_category';
    protected $appends = ['name'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_en', 'name_es', 'slug'
    ];

    public function posts() {
        return $this->belongsToMany(BlogContent::class, 'blog_content_blog_category');
    }
}
