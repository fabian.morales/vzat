<?php 

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class LocaleHelper{
    public static function getHispanicCountries() {
        $ret = [
            'MX' => 'México',
            'CO' => 'Colombia',
            'ES' => 'España',
            'AR' => 'Argentina',
            'PE' => 'Perú',
            'VE' => 'Venezuela',
            'CL' => 'Chile',
            'GT' => 'Guatemala',
            'EC' => 'Ecuador',
            'CU' => 'Cuba',
            'BO' => 'Bolivia',
            'DO' => 'República Dominicana',
            'HN' => 'Honduras',
            'SV' => 'El Salvador',
            'PY' => 'Paraguay',
            'NI' => 'Nicaragua',
            'CR' => 'Costa Rica',
            'PA' => 'Panamá',
            'PR' => 'Puerto Rico',
            'UY' => 'Uruguay',
            'GQ' => 'Guinea Ecuatorial',
            'BZ' => 'Belice',
            'EH' => 'Sahara Occidental',
        ];

        return $ret;
    }

    public static function getLocale() {
        return \App::getLocale() == 'es' ? 'es' : 'en';
    }

    public static function hyphenize($string) {
        $invalidSymbols = [' ', ':', '/', '?', '¿', '¡', '!', '[', ']', '.', ',', '"', '‘', '’', '%', '\\', '.', '&', '+'];
        $validSymbols = ['-', '', '', '', '', '', '', '', '', '', '-', '', '', '', '', '', '', '', ''];
        $invalidLetters = ['á', 'é', 'í', 'ó', 'ú', 'ñ', 'ª', '°'];
        $validLetters = ['a', 'e', 'i', 'o', 'u', 'n', 'a', 'o'];
        $multiDash = ['----', '---', '--'];
        $oneDash = ['-', '-', '-'];

        $string = trim(mb_strtolower($string));
        $string = str_replace($invalidSymbols, $validSymbols, $string);
        $string = str_replace("'", "", $string);
        $string = str_replace($invalidLetters, $validLetters, $string);
        $string = str_replace($multiDash, $oneDash, $string);
        return $string;
    }

    public static function getCountry() {
        if (!$ip = Request::ip()) {
            return NULL;
        }

        $endpoint = "http://www.geoplugin.net/json.gp?ip=" . $ip;

        $response = json_decode(Http::get($endpoint));
        if (empty($response)) {
            return NULL;
        }

        $ret = [
            "country_name" => $response->geoplugin_countryName,
            "country_code" => $response->geoplugin_countryCode,
            "lat" => $response->geoplugin_latitude,
            "lng" => $response->geoplugin_longitude,
        ];

        return (object)$ret;
    }
}
