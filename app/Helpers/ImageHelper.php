<?php 

namespace App\Helpers;

class ImageHelper{
    public static function makeImage($original, $dest, $width, $height, $resizeCanvas = false) {
        @unlink($dest);
        
        $img = \Image::make($original);
        $prop = $width / $height;
        $originalProp = $img->width() / $img->height();

        if ($img->height() > $img->width()) {
            $finalHeight = $height;
            $finalWidth = $finalHeight * $originalProp;
        }
        else {
            $finalWidth = $width;
            $finalHeight = $finalWidth / $originalProp;
        }

        /*if ($img->height() > $height) {
            $finalHeight = $img->height();
            $finalWidth = $finalHeight * $prop;
        }

        if ($img->width() > $width) {
            $finalWidth = $img->width();
            $finalHeight = $finalWidth / $prop;
        }*/

        if ($resizeCanvas) {
            $img
                ->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                    //$constraint->upsize();
                })
                /*->fit(round($finalWidth), round($finalHeight), function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })*/
                ->resizeCanvas($width, $height, 'center', true, 'ffffff')
                ->crop($width, $height)
                ->save($dest);
        }
        else {
            $img->fit(round($finalWidth), round($finalHeight), function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save($dest);
        }
    }
}
