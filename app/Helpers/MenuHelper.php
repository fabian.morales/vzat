<?php 

namespace App\Helpers;

use Illuminate\Support\Facades\Request;

class MenuHelper{
    public static function getActiveItems() {
        $item = "";
        $subitem = "";

        if (Request::segment(1) == "admin") {
            $item = Request::segment(2);
            $subitem = Request::segment(3);
            
            /*if (Request::segment(2) != "users") {
                $subitem = $item;
                $item = "masters";
            }*/
        }
        else{
            $item = Request::segment(1);
        }

        return [$item, $subitem];
    }
}
