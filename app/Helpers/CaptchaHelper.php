<?php 

namespace App\Helpers;

use Illuminate\Support\Facades\Request;

class CaptchaHelper{
    public static function renderJS() {
        $siteKey = env('CAPTCHA_SITEKEY');
        return view('captcha.js', compact('siteKey'));
    }

    public static function renderButton() {
        $siteKey = env('CAPTCHA_SITEKEY');
        return view('captcha.html', compact('siteKey'));
    }
}
