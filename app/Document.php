<?php

namespace App;

use App\Helpers\ImageHelper;
use App\Traits\ImageTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;
    use ImageTrait;

    protected $table = 'document';
    protected $fillable = [
        'title', 'description', 'language_id', 'category_id', 'user_id', 'publication_date', 'summary',
    ];
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'language_id' => 'integer',
        'category_id' => 'integer',
    ];
    protected $appends = ['view_url', 'download_url', 'cover_thumbnail'];

    protected $folderName = 'document';
    protected $idField = "id";
    protected $fileField = "cover_image";

    public function author() {
        return $this->belongsTo(User::class);
    }

    public function language() {
        return $this->belongsTo(Language::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function sectors() {
        return $this->belongsToMany(Sector::class);
    }

    public function states() {
        return $this->belongsToMany(State::class);
    }

    public function keywords() {
        return $this->hasMany(DocumentKeyword::class);
    }

    public function getDefaultImage() {
        return config('custom.images.document_default');
    }

    public function getViewUrlAttribute() {
        return route('document::view', ['id' => $this->id]);
    }

    public function getDownloadUrlAttribute() {
        return route('document::download', ['file' => $this->file]);
    }

    public function getCoverThumbnailAttribute() {
        return asset($this->getImageUrl('featured'));
        //return asset($this->getImageUrl('thumbnail'));
    }

    public function generateImages() {
        if (!empty($this->cover_image)) {
            ImageHelper::makeImage($this->getOriginalImagePath(), $this->getImagePath('inner'), 770, 450);
            ImageHelper::makeImage($this->getOriginalImagePath(), $this->getImagePath('featured'), 670, 450);
            ImageHelper::makeImage($this->getOriginalImagePath(), $this->getImagePath('thumbnail'), 370, 220);
            ImageHelper::makeImage($this->getOriginalImagePath(), $this->getImagePath('tiny'), 70, 70);
        }
    }

    /*public static function boot() {
        parent::boot();

        static::created(function($document)
        {            
            $change = new DocumentChange([
                'ip_address' => Request::ip(), 
                'action' => 'create', 
                'user_id' => \Auth::user()->id, 
                'document_id' => $document->id, 
                'before' => '', 
                'after' => $document->toJson()
            ]);

            $change->save();
        });

        static::updating(function($document)
        {
            $before = Document::find($document->id);
            $change = new DocumentChange([
                'ip_address' => Request::ip(), 
                'action' => 'update', 
                'user_id' => \Auth::user()->id, 
                'document_id' => $document->id, 
                'before' => $before->toJson(), 
                'after' => $document->toJson()
            ]);

            $change->save();
        });


    }*/
}
