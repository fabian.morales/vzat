<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogUserAction extends Model
{
    protected $table = 'blog_user_action';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function post() {
        return $this->belongsTo(Document::class);
    }
}
