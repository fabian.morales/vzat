<?php

namespace App;

use App\Helpers\LocaleHelper;
use App\Traits\ImageTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogContent extends Model
{
    use SoftDeletes;
    use ImageTrait;
    
    protected $table = 'blog_content';
    protected $fillable = [
        'slug', 'title_es', 'title_en', 'type', 'body_es', 'body_en', 'weight', 'status', 'image', 'user_id'
    ];
    protected $folderName = 'blog';
    protected $idField = "id";
    protected $fileField = "image";

    public function title() {
        $locale = LocaleHelper::getLocale();
        $field = 'title_' . $locale;
        return $this->$field;
    }

    public function body() {
        $locale = LocaleHelper::getLocale();
        $field = 'body_' . $locale;
        return $this->$field;
    }

    public function scopeIsPost($query) {
        return $query->where('type', 'blog');
    }

    public function scopeActive($query) {
        return $query->where('status', 1);
    }

    public function categories() {
        return $this->belongsToMany(BlogCategory::class, 'blog_content_blog_category');
    }

    public function author() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDescription() {
        $desc = explode(" ", strip_tags($this->body()));
        $desc = implode(" ", array_slice($desc, 0, 20));
        return $desc;
    }

    public function getDefaultImage() {
        return config('custom.images.post_default');
    }
}
