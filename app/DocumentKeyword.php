<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentKeyword extends Model
{
    protected $table = 'document_keyword';
    protected $fillable = [
        'value', 'document_id'
    ];

    public function document() {
        return $this->belongsTo(Document::class);
    }
}
