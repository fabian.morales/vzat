<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenericDomain extends Model
{
    protected $table = 'generic_domain';
    protected $fillable = [
        'domain',
    ];
}
