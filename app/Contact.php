<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
    protected $fillable = [
        'name', 'email', 'entity', 'country_id', 'message'
    ];

    public function country() {
        return $this->belongsTo(Country::class);
    }
}
