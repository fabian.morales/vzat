<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'state';
    protected $fillable = [
        'name',
    ];

    public function documents() {
        return $this->belongsToMany(Document::class);
    }
}
