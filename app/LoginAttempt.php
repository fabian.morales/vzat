<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginAttempt extends Model
{
    protected $table = 'login_attempt';

    public function user() {
        return $this->belongsTo(User::class);
    }
}
