<?php

namespace App\Traits;

use App\Helpers\LocaleHelper;
use Illuminate\Support\Facades\App;

trait MultiLanguageFieldTrait{
    public function getNameAttribute() {
        $locale = LocaleHelper::getLocale();
        $field = 'name_' . $locale;
        return $this->$field;
    }

    public function getDescriptionAttribute() {
        $locale = LocaleHelper::getLocale();
        $field = 'description_' . $locale;
        return $this->$field;
    }

    public function name() {
        $locale = LocaleHelper::getLocale();
        $field = 'name_' . $locale;
        return $this->$field;
    }
}