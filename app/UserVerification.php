<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model
{
    protected $table = 'user_verification';

    public function user() {
        return $this->belongsTo(User::class);
    }
}
