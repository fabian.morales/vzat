<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoimainBlackList extends Model
{
    protected $table = 'domain_black_list';
    protected $fillable = [
        'name',
    ];
}
