<?php

namespace App;

use App\Traits\MultiLanguageFieldTrait;
use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    use MultiLanguageFieldTrait;
    protected $table = 'gender';
    protected $fillable = [
        'name_en', 'name_es',
    ];
}
