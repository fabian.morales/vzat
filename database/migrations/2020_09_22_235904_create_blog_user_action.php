<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogUserAction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_user_action', function (Blueprint $table) {
            $table->id();
            $table->string('ip_address', 30);
            $table->string('action', 30);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('blog_content_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('blog_content_id')->references('id')->on('blog_content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_user_action');
    }
}
