<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchWordSearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_search_word', function (Blueprint $table) {
            $table->unsignedBigInteger('search_word_id');
            $table->unsignedBigInteger('search_id');

            $table->foreign('search_word_id')->references('id')->on('search_word');
            $table->foreign('search_id')->references('id')->on('search');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_word_search');
    }
}
