<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyDocumentAddSummaryCoverImg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document', function (Blueprint $table) {
            $table->mediumText('summary')->nullable()->after('description');
            $table->string('cover_image')->nullable()->after('summary');
        });

        DB::statement('ALTER TABLE document DROP INDEX full');
        DB::statement('ALTER TABLE document ADD FULLTEXT full(title, description, summary)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
