<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFulltextSearch01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE document ADD FULLTEXT full(title, description)');
        DB::statement('ALTER TABLE document_keyword ADD FULLTEXT full(value)');
        DB::statement('ALTER TABLE sector ADD FULLTEXT full(name_es, name_en)');
        DB::statement('ALTER TABLE state ADD FULLTEXT full(name)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
