-- convert Laravel migrations to raw SQL scripts --

-- migration:2014_10_12_000000_create_users_table --
create table `users` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name` varchar(255) not null, 
  `email` varchar(255) not null, 
  `email_verified_at` timestamp null, 
  `password` varchar(255) not null, 
  `remember_token` varchar(100) null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `users` 
add 
  unique `users_email_unique`(`email`);

-- migration:2014_10_12_100000_create_password_resets_table --
create table `password_resets` (
  `email` varchar(255) not null, 
  `token` varchar(255) not null, 
  `created_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `password_resets` 
add 
  index `password_resets_email_index`(`email`);

-- migration:2019_08_19_000000_create_failed_jobs_table --
create table `failed_jobs` (
  `id` bigint unsigned not null auto_increment primary key, 
  `connection` text not null, `queue` text not null, 
  `payload` longtext not null, `exception` longtext not null, 
  `failed_at` timestamp default CURRENT_TIMESTAMP not null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_07_24_032420_create_permission_tables --
create table `permissions` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name` varchar(255) not null, 
  `guard_name` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
create table `roles` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name` varchar(255) not null, 
  `guard_name` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
create table `model_has_permissions` (
  `permission_id` bigint unsigned not null, 
  `model_type` varchar(255) not null, 
  `model_id` bigint unsigned not null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `model_has_permissions` 
add 
  index `model_has_permissions_model_id_model_type_index`(`model_id`, `model_type`);
alter table 
  `model_has_permissions` 
add 
  constraint `model_has_permissions_permission_id_foreign` foreign key (`permission_id`) references `permissions` (`id`) on delete cascade;
alter table 
  `model_has_permissions` 
add 
  primary key `model_has_permissions_permission_model_type_primary`(
    `permission_id`, `model_id`, `model_type`
  );
create table `model_has_roles` (
  `role_id` bigint unsigned not null, 
  `model_type` varchar(255) not null, 
  `model_id` bigint unsigned not null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `model_has_roles` 
add 
  index `model_has_roles_model_id_model_type_index`(`model_id`, `model_type`);
alter table 
  `model_has_roles` 
add 
  constraint `model_has_roles_role_id_foreign` foreign key (`role_id`) references `roles` (`id`) on delete cascade;
alter table 
  `model_has_roles` 
add 
  primary key `model_has_roles_role_model_type_primary`(
    `role_id`, `model_id`, `model_type`
  );
create table `role_has_permissions` (
  `permission_id` bigint unsigned not null, 
  `role_id` bigint unsigned not null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `role_has_permissions` 
add 
  constraint `role_has_permissions_permission_id_foreign` foreign key (`permission_id`) references `permissions` (`id`) on delete cascade;
alter table 
  `role_has_permissions` 
add 
  constraint `role_has_permissions_role_id_foreign` foreign key (`role_id`) references `roles` (`id`) on delete cascade;
alter table 
  `role_has_permissions` 
add 
  primary key `role_has_permissions_permission_id_role_id_primary`(`permission_id`, `role_id`);

-- migration:2020_07_24_214343_alter_users --
alter table 
  `users` 
add 
  `entity` varchar(255) null 
after 
  `email`, 
add 
  `position` varchar(255) null 
after 
  `email`, 
add 
  `validated` tinyint unsigned not null default '0' 
after 
  `email`, 
add 
  `approved` tinyint unsigned not null default '0' 
after 
  `email`, 
add 
  `role_id` bigint unsigned null 
after 
  `email`;
alter table 
  `users` 
add 
  constraint `users_role_id_foreign` foreign key (`role_id`) references `roles` (`id`);

-- migration:2020_08_04_003353_alter_users_email_reason --
alter table 
  `users` 
add 
  `email_reason` mediumtext null 
after 
  `email`;

-- migration:2020_08_04_003629_create_country --
create table `country` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_08_04_003656_alter_users_country --
alter table 
  `users` 
add 
  `country_id` bigint unsigned null 
after 
  `email_reason`;

-- migration:2020_08_04_005243_create_domain_black_list --
create table `domain_black_list` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_08_04_015220_alter_country_name --
alter table 
  `country` 
add 
  `name_en` varchar(255) not null 
after 
  `id`, 
add 
  `name_es` varchar(255) not null 
after 
  `id`;
alter table 
  `country` 
drop 
  `name`;

-- migration:2020_08_05_235506_create_login_attempt --
create table `login_attempt` (
  `id` bigint unsigned not null auto_increment primary key, 
  `ip_address` varchar(30) not null, 
  `result` varchar(30) not null, 
  `user_id` bigint unsigned null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `login_attempt` 
add 
  constraint `login_attempt_user_id_foreign` foreign key (`user_id`) references `users` (`id`);

-- migration:2020_08_06_000044_alter_user_fk01 --
alter table 
  `users` 
add 
  constraint `users_country_id_foreign` foreign key (`country_id`) references `country` (`id`);

-- migration:2020_08_06_000431_create_user_session --
create table `user_session` (
  `id` bigint unsigned not null auto_increment primary key, 
  `ip_address` varchar(30) not null, 
  `sessid` varchar(100) not null, 
  `last_activity` timestamp not null, 
  `user_id` bigint unsigned null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `user_session` 
add 
  constraint `user_session_user_id_foreign` foreign key (`user_id`) references `users` (`id`);

-- migration:2020_08_06_003057_alter_user_session_active --
alter table 
  `user_session` 
add 
  `active` tinyint unsigned not null 
after 
  `last_activity`;

-- migration:2020_08_06_003636_alter_login_attempt_mail --
alter table 
  `login_attempt` 
add 
  `email` varchar(255) not null 
after 
  `user_id`;

-- migration:2020_08_06_011441_alter_users_auth_vars --
alter table 
  `users` 
add 
  `last_ip_addr` varchar(255) not null 
after 
  `remember_token`, 
add 
  `last_login` timestamp null 
after 
  `remember_token`;

-- migration:2020_08_09_004734_create_user_verification --
create table `user_verification` (
  `id` bigint unsigned not null auto_increment primary key, 
  `token` varchar(100) not null, 
  `valid` tinyint unsigned not null default '0', 
  `user_id` bigint unsigned null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `user_verification` 
add 
  constraint `user_verification_user_id_foreign` foreign key (`user_id`) references `users` (`id`);

-- migration:2020_08_11_010047_create_language --
create table `language` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name` varchar(255) not null, 
  `abbreviation` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_08_11_010136_create_category --
create table `category` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name_es` varchar(255) not null, 
  `name_en` varchar(255) not null, 
  `color` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_08_11_010220_create_document --
create table `document` (
  `id` bigint unsigned not null auto_increment primary key, 
  `title` varchar(255) not null, 
  `description` mediumtext not null, 
  `user_id` bigint unsigned not null, 
  `language_id` bigint unsigned not null, 
  `category_id` bigint unsigned not null, 
  `file` varchar(255) not null, 
  `views` bigint unsigned not null default '0', 
  `downloads` bigint unsigned not null default '0', 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `document` 
add 
  constraint `document_user_id_foreign` foreign key (`user_id`) references `users` (`id`);
alter table 
  `document` 
add 
  constraint `document_language_id_foreign` foreign key (`language_id`) references `language` (`id`);
alter table 
  `document` 
add 
  constraint `document_category_id_foreign` foreign key (`category_id`) references `category` (`id`);

-- migration:2020_08_11_010448_create_document_user_action --
create table `document_user_action` (
  `id` bigint unsigned not null auto_increment primary key, 
  `ip_address` varchar(30) not null, 
  `action` varchar(30) not null, 
  `user_id` bigint unsigned not null, 
  `document_id` bigint unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `document_user_action` 
add 
  constraint `document_user_action_user_id_foreign` foreign key (`user_id`) references `users` (`id`);
alter table 
  `document_user_action` 
add 
  constraint `document_user_action_document_id_foreign` foreign key (`document_id`) references `document` (`id`);

-- migration:2020_08_11_010709_create_document_change --
create table `document_change` (
  `id` bigint unsigned not null auto_increment primary key, 
  `ip_address` varchar(30) not null, 
  `action` varchar(30) not null, 
  `user_id` bigint unsigned not null, 
  `document_id` bigint unsigned not null, 
  `before` longtext not null, 
  `after` longtext not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `document_change` 
add 
  constraint `document_change_user_id_foreign` foreign key (`user_id`) references `users` (`id`);
alter table 
  `document_change` 
add 
  constraint `document_change_document_id_foreign` foreign key (`document_id`) references `document` (`id`);

-- migration:2020_08_11_013249_create_document_keyword --
create table `document_keyword` (
  `id` bigint unsigned not null auto_increment primary key, 
  `value` varchar(255) not null, 
  `document_id` bigint unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `document_keyword` 
add 
  constraint `document_keyword_document_id_foreign` foreign key (`document_id`) references `document` (`id`);

-- migration:2020_08_13_013513_alter_user_accept_terms --
alter table 
  `users` 
add 
  `accept_terms` varchar(255) not null 
after 
  `remember_token`;

-- migration:2020_08_13_184737_alter_document_file --
alter table 
  `document` 
add 
  `size` bigint unsigned not null default '0' 
after 
  `file`, 
add 
  `file_name` varchar(255) not null 
after 
  `file`;

-- migration:2020_08_13_210258_create_state --
create table `state` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_08_13_210510_create_sector --
create table `sector` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name_es` varchar(255) not null, 
  `name_en` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_08_13_211619_create_document_sector --
create table `document_sector` (
  `document_id` bigint unsigned not null, 
  `sector_id` bigint unsigned not null, 
  `created_at` timestamp null, `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `document_sector` 
add 
  primary key `document_sector_document_id_sector_id_primary`(`document_id`, `sector_id`);
alter table 
  `document_sector` 
add 
  constraint `document_sector_document_id_foreign` foreign key (`document_id`) references `document` (`id`);
alter table 
  `document_sector` 
add 
  constraint `document_sector_sector_id_foreign` foreign key (`sector_id`) references `sector` (`id`);

-- migration:2020_08_13_211653_create_document_state --
create table `document_state` (
  `document_id` bigint unsigned not null, 
  `state_id` bigint unsigned not null, 
  `created_at` timestamp null, `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `document_state` 
add 
  primary key `document_state_document_id_state_id_primary`(`document_id`, `state_id`);
alter table 
  `document_state` 
add 
  constraint `document_state_document_id_foreign` foreign key (`document_id`) references `document` (`id`);
alter table 
  `document_state` 
add 
  constraint `document_state_state_id_foreign` foreign key (`state_id`) references `state` (`id`);

-- migration:2020_08_15_181426_add_fulltext_search01 --
ALTER TABLE 
  document 
ADD 
  FULLTEXT full(title, description);
ALTER TABLE 
  document_keyword 
ADD 
  FULLTEXT full(value);
ALTER TABLE 
  sector 
ADD 
  FULLTEXT full(name_es, name_en);
ALTER TABLE 
  state 
ADD 
  FULLTEXT full(name);

-- migration:2020_08_20_212409_create_generic_domain --
create table `generic_domain` (
  `id` bigint unsigned not null auto_increment primary key, 
  `domain` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_08_25_014050_create_contact --
create table `contact` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name` varchar(255) not null, 
  `email` varchar(255) not null, 
  `entity` varchar(255) not null, 
  `country_id` bigint unsigned not null, 
  `message` mediumtext not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `contact` 
add 
  constraint `contact_country_id_foreign` foreign key (`country_id`) references `country` (`id`);

-- migration:2020_08_26_225601_create_blog_content --
create table `blog_content` (
  `id` bigint unsigned not null auto_increment primary key, 
  `title_es` varchar(255) not null, 
  `title_en` varchar(255) not null, 
  `type` varchar(255) not null, 
  `slug` varchar(255) not null, 
  `body` varchar(255) not null, 
  `image` varchar(255) null, 
  `weight` varchar(255) not null, 
  `user_id` bigint unsigned null, 
  `status` tinyint not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null, 
  `deleted_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `blog_content` 
add 
  constraint `blog_content_user_id_foreign` foreign key (`user_id`) references `users` (`id`);

-- migration:2020_08_26_230538_create_blog_category --
create table `blog_category` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name_es` varchar(255) not null, 
  `name_en` varchar(255) not null, 
  `slug` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_08_26_230707_create_blog_content_blog_category --
create table `blog_content_blog_category` (
  `id` bigint unsigned not null auto_increment primary key, 
  `blog_content_id` bigint unsigned not null, 
  `blog_category_id` bigint unsigned not null, 
  `created_at` timestamp null, `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `blog_content_blog_category` 
add 
  constraint `blog_content_blog_category_blog_content_id_foreign` foreign key (`blog_content_id`) references `blog_content` (`id`);
alter table 
  `blog_content_blog_category` 
add 
  constraint `blog_content_blog_category_blog_category_id_foreign` foreign key (`blog_category_id`) references `blog_category` (`id`);

-- migration:2020_08_28_003238_alter_blog_content_body --
alter table 
  `blog_content` 
add 
  `body_en` text not null 
after 
  `slug`, 
add 
  `body_es` text not null 
after 
  `slug`;
alter table 
  `blog_content` 
drop 
  `body`;

-- migration:2020_08_29_180105_create_search_word --
create table `search_word` (
  `id` bigint unsigned not null auto_increment primary key, 
  `value` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `search_word` 
add 
  unique `search_word_value_unique`(`value`);

-- migration:2020_08_29_180326_create_search --
create table `search` (
  `id` bigint unsigned not null auto_increment primary key, 
  `text_search` varchar(300) null, 
  `user_id` bigint unsigned not null, 
  `ip_address` varchar(30) not null, 
  `language_id` bigint unsigned null, 
  `category_id` bigint unsigned null, 
  `sector_id` bigint unsigned null, 
  `state_id` bigint unsigned null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `search` 
add 
  constraint `search_user_id_foreign` foreign key (`user_id`) references `users` (`id`);
alter table 
  `search` 
add 
  constraint `search_language_id_foreign` foreign key (`language_id`) references `language` (`id`);
alter table 
  `search` 
add 
  constraint `search_category_id_foreign` foreign key (`category_id`) references `category` (`id`);
alter table 
  `search` 
add 
  constraint `search_sector_id_foreign` foreign key (`sector_id`) references `sector` (`id`);
alter table 
  `search` 
add 
  constraint `search_state_id_foreign` foreign key (`state_id`) references `state` (`id`);

-- migration:2020_08_29_180731_create_search_word_search --
create table `search_search_word` (
  `search_word_id` bigint unsigned not null, 
  `search_id` bigint unsigned not null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `search_search_word` 
add 
  constraint `search_search_word_search_word_id_foreign` foreign key (`search_word_id`) references `search_word` (`id`);
alter table 
  `search_search_word` 
add 
  constraint `search_search_word_search_id_foreign` foreign key (`search_id`) references `search` (`id`);

-- migration:2020_08_29_181144_alter_search_hash --
alter table 
  `search` 
add 
  `hash` text not null 
after 
  `text_search`;

-- migration:2020_09_07_232012_alter_blog_content_views --
alter table 
  `blog_content` 
add 
  `views` int not null default '0' 
after 
  `user_id`;

-- migration:2020_09_07_232520_alter_blog_category_views --
alter table 
  `blog_category` 
add 
  `views` int not null default '0' 
after 
  `slug`;

-- migration:2020_09_22_235904_create_blog_user_action --
create table `blog_user_action` (
  `id` bigint unsigned not null auto_increment primary key, 
  `ip_address` varchar(30) not null, 
  `action` varchar(30) not null, 
  `user_id` bigint unsigned not null, 
  `blog_content_id` bigint unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `blog_user_action` 
add 
  constraint `blog_user_action_user_id_foreign` foreign key (`user_id`) references `users` (`id`);
alter table 
  `blog_user_action` 
add 
  constraint `blog_user_action_blog_content_id_foreign` foreign key (`blog_content_id`) references `blog_content` (`id`);

-- migration:2020_09_23_210203_add_softdeletes_document --
alter table 
  `document` 
add 
  `deleted_at` timestamp null;

-- migration:2020_09_23_210239_add_softdeletes_user --
alter table 
  `users` 
add 
  `deleted_at` timestamp null;

-- migration:2020_09_24_044929_alter_document_pubdate --
alter table 
  `document` 
add 
  `publication_date` date not null 
after 
  `downloads`;

-- migration:2020_09_24_052635_alter_user_receive_emails --
alter table 
  `users` 
add 
  `receive_emails` varchar(255) not null 
after 
  `accept_terms`;

-- migration:2021_12_21_231936_create_age_range --
create table `age_range` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name_es` varchar(255) not null, 
  `name_en` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2021_12_21_232508_create_gender --
create table `gender` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name_es` varchar(255) not null, 
  `name_en` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2021_12_21_232528_modify_user_add_age_range_gender --
alter table 
  `users` 
add 
  `age_range_id` bigint unsigned null 
after 
  `entity`, 
add 
  `gender_id` bigint unsigned null 
after 
  `entity`, 
add 
  `reject_reason` mediumtext null 
after 
  `email_reason`;
alter table 
  `users` 
add 
  constraint `users_gender_id_foreign` foreign key (`gender_id`) references `gender` (`id`);

-- migration:2021_12_23_155715_modify_document_add_summary_cover_img --
alter table 
  `document` 
add 
  `summary` mediumtext null 
after 
  `description`, 
add 
  `cover_image` varchar(255) null 
after 
  `summary`;
ALTER TABLE 
  document 
DROP 
  INDEX full;
ALTER TABLE 
  document 
ADD 
  FULLTEXT full(title, description, summary);

-- migration:2021_12_27_130622_modify_user_add_fk_age_range --
alter table 
  `users` 
add 
  constraint `users_age_range_id_foreign` foreign key (`age_range_id`) references `age_range` (`id`);

-- migration:2022_01_23_172149_modify_category_add_description --
alter table 
  `category` 
add 
  `description_es` varchar(255) null 
after 
  `name_en`, 
add 
  `description_en` varchar(255) null 
after 
  `description_es`;
