<?php

use App\Language;
use Illuminate\Database\Seeder;

class LanguageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langs = [
            'Español', 
            'English', 
        ];
        
        $created = DB::table('language')->select('name')->get()->pluck('name')->all();
        $diff = array_diff($langs, $created);
        foreach ($diff as $lang) {
            Language::create(['name' => $lang, 'abbreviation' => strtolower(substr($lang, 0, 2))]);
        }
    }
}
