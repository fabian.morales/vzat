<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories_es = [
            'Reporte temático mensual', 
            'Reporte de incidente', 
            'Reporte especial',
            'Reporte flash',
        ];

        $categories_en = [
            'Thematic monthly report', 
            'Incident report', 
            'Special report',
            'Flash report',
        ];
        
        $created = DB::table('category')->select('name_es')->get()->pluck('name_es')->all();
        $diff = array_diff($categories_es, $created);
        foreach ($diff as $i => $category) {
            Category::create(['name_es' => $category, 'name_en' => $categories_en[$i], 'color' => '#fff']);
        }
    }
}
