<?php

use App\AgeRange;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgeRangeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $age_ranges_es = [
            '18 - 19',
            '20 - 24',
            '25 - 34',
            '35 - 49',
            '50+',
            'Prefiero no decir',
        ];

        $age_ranges_en = [
            '18 - 19',
            '20 - 24',
            '25 - 34',
            '35 - 49',
            '50+',
            'Prefer not to say',
        ];
        
        $created = DB::table('age_range')->select('name_es')->get()->pluck('name_es')->all();
        $diff = array_diff($age_ranges_es, $created);
        foreach ($diff as $i => $ageRange) {
            AgeRange::create(['name_es' => $ageRange, 'name_en' => $age_ranges_en[$i]]);
        }
    }
}
