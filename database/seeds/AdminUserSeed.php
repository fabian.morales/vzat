<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AdminUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cnt = DB::table('users')->where('name', 'Administrador')->count();
        if ($cnt == 0) {
            DB::table('users')->insert([
                'name' => 'Administrador',
                'email' =>'admin@vzat.org',
                'password' => Hash::make('123456'),
                'approved' => 1,
                'validated' => 1,
                'email_verified_at' => Carbon::now(),
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
