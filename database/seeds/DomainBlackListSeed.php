<?php

use App\DoimainBlackList;
use Illuminate\Database\Seeder;

class DomainBlackListSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cnt = DB::table('domain_black_list')->count();
        if ($cnt == 0) {
            $file = __DIR__ . DIRECTORY_SEPARATOR . 'domain_black_list.json';
            $data = json_decode(file_get_contents($file));
            $rows = array_chunk($data->rows, 1000);
            foreach ($rows as $group) {
                DB::table('domain_black_list')->insert(array_map(function($o) {
                    return ['name' => $o->name, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
                }, $group));
            }
        }
    }
}
