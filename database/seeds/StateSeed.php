<?php

use App\State;
use Illuminate\Database\Seeder;

class StateSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            'Amazonas',
            'Anzoátegui',
            'Apure',
            'Aragua',
            'Barinas',
            'Bolívar',
            'Carabobo',
            'Cojedes',
            'Delta Amacuro',
            'Distrito Capital',
            'Falcón',
            'Guárico',
            'Lara',
            'Mérida',
            'Miranda',
            'Monagas',
            'Nueva Esparta',
            'Portuguesa',
            'Sucre',
            'Táchira',
            'Trujillo',
            'La Guaira',
            'Yaracuy',
            'Zulia',
            'Nacional',            
        ];
        
        $created = DB::table('state')->select('name')->get()->pluck('name')->all();
        $diff = array_diff($states, $created);
        foreach ($diff as $state) {
            State::create(['name' => $state]);
        }
    }
}
