<?php

use App\GenericDomain;
use Illuminate\Database\Seeder;

class GenericDomainSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domains = [
            'gmail.com', 
            'yahoo.com', 
            'yahoo.es', 
            'outlook.com', 
            'hotmail.com', 
            'hotmail.es', 
        ];
        
        $created = DB::table('generic_domain')->select('domain')->get()->pluck('domain')->all();
        $diff = array_diff($domains, $created);
        foreach ($diff as $domain) {
            GenericDomain::create(['domain' => $domain]);
        }
    }
}
