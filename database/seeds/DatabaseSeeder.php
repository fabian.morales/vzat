<?php

use App\Sector;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(AdminUserSeed::class);
        // $this->call(RoleUserSeed::class);
        // $this->call(DomainBlackListSeed::class);
        // $this->call(CategorySeed::class);
        // $this->call(LanguageSeed::class);
        // $this->call(SectorSeed::class);
        // $this->call(StateSeed::class);
        // $this->call(GenericDomainSeed::class);
        // $this->call(CountrySeed::class);
        $this->call(AgeRangeSeed::class);
        $this->call(GenderSeed::class);
    }
}
