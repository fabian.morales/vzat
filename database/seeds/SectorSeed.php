<?php

use App\Sector;
use Illuminate\Database\Seeder;

class SectorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectors_es = [
            'Salud',
            'Educación',
            'Seguridad',
            'Economía',
            'WASH',
            'Seguridad Alimentaria',
            'Manutenciones',
            'Protección',
            'Logística',
        ];

        $sectors_en = [
            'Health',
            'Education',
            'Security',
            'Economy',
            'WASH',
            'Food Security',
            'Livelihoods',
            'Protection',
            'Logistics',
        ];
        
        $created = DB::table('sector')->select('name_es')->get()->pluck('name_es')->all();
        $diff = array_diff($sectors_es, $created);
        foreach ($diff as $i => $sector) {
            Sector::create(['name_es' => $sector, 'name_en' => $sectors_en[$i]]);
        }
    }
}
