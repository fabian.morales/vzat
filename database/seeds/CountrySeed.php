<?php

use Illuminate\Database\Seeder;

class CountrySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cnt = DB::table('country')->count();
        if ($cnt == 0) {
            $file = __DIR__ . DIRECTORY_SEPARATOR . 'countries.csv';
            $data = [];

            if (($handler = fopen($file, "r")) !== FALSE) {
                while (($line = fgetcsv($handler, 1000, ",")) !== FALSE) {
                    $data[] = ['name_es' => $line[0], 'name_en' => $line[1], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
                }

                fclose($handler);
            }

            $rows = array_chunk($data, 1000);
            foreach ($rows as $group) {
                DB::table('country')->insert($group);
            }
        }
    }
}
