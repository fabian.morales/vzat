<?php

use App\Gender;
use Illuminate\Database\Seeder;

class GenderSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genders_es = [
            'Masculino',
            'Femenino',
            'Prefiero no decir',
        ];

        $genders_en = [
            'Male',
            'Female',
            'Prefer not to say',
        ];
        
        $created = DB::table('gender')->select('name_es')->get()->pluck('name_es')->all();
        $diff = array_diff($genders_es, $created);
        foreach ($diff as $i => $gender) {
            Gender::create(['name_es' => $gender, 'name_en' => $genders_en[$i]]);
        }
    }
}
