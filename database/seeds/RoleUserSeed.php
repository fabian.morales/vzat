<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $rolesToCreate = ['Administrador', 'Usuario'];
        $createdRoles = DB::table('roles')->select('name')->get()->pluck('name')->all();
        $diffRoles = array_diff($rolesToCreate, $createdRoles);

        foreach ($diffRoles as $role) {
            Role::create(['name' => $role]);
        }

        DB::table('roles')
            ->where('guard_name', '')
            ->update(['guard_name' => 'web', 'updated_at' => Carbon::now()]);

        $adminRole = Role::where('name', 'Administrador')->first();
        $userRole = Role::where('name', 'Usuario')->first();

        $modules = [
            'documents', 
            'categories', 
            'users', 
            'users_approval', 
            'countries', 
            'languages', 
            'states', 
            'sectors', 
            'generic_domains',
            'contacts',
            'blog_categories',
            'blog_posts',
            'reports',
        ];
        
        $permissions = ['view', 'create', 'edit', 'delete'];

        $permToCreate = [];
        foreach ($modules as $module) {
            foreach ($permissions as $permission) {
                $permToCreate[] = $module . '.' . $permission;
            }
        }

        $createdPerm = DB::table('permissions')->select('name')->get()->pluck('name')->all();
        $diffPerm = array_diff($permToCreate, $createdPerm);
        foreach ($diffPerm as $permission) {
            $createdPermission = Permission::create(['name' => $permission]);
            $adminRole->givePermissionTo($createdPermission);

            if (strpos($permission, 'documents') !== FALSE) {
                $userRole->givePermissionTo($createdPermission);
            }
        }

        $adminUser = App\User::where('name', 'Administrador')->first();
        if (!$adminUser->hasRole($adminRole)) {
            $adminUser->assignRole($adminRole);
        }

        $adminUser->role_id = $adminRole->id;
        $adminUser->save();
    }
}
