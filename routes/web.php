<?php

use App\Http\Controllers\PublicApiDataController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'locale'], function () {
    Auth::routes(['verify' => true]);

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/about', 'StaticPageController@showAboutPage')->name('about');
    Route::any('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
    Route::get('/products', 'StaticPageController@showProductsPage')->name('products');
    Route::get('/privacy-policy', 'StaticPageController@showPrivacyPolicyPage')->name('terms');
    Route::get('/dashboard', 'StaticPageController@showDashboard')->name('front::dashboard')->middleware(['auth']);
    Route::get('/locale/switch/{lang}', 'StaticPageController@localeSwitch')->name('locale::switch');

    Route::get('/categories', [PublicApiDataController::class, 'getCategories'])->name('api.front.categories');
    Route::get('/languages', [PublicApiDataController::class, 'getLanguages'])->name('api.front.languages');
    Route::get('/states', [PublicApiDataController::class, 'getStates'])->name('api.front.states');
    Route::get('/sectors', [PublicApiDataController::class, 'getSectors'])->name('api.front.sectors');

    Route::group(['prefix' => 'contact'], function() {
        Route::get('/', 'ContactController@showContactPage')->name('contact');
        Route::post('/submit', 'ContactController@saveContact')->name('contact::submit');
    });

    Route::group(['prefix' => 'user'], function() {
        Route::get('/verification/{token}', 'UserVerificationController@verify')->name('user_verification');
    });

    Route::group(['prefix' => 'search'], function() {
        Route::any('/', 'SearchController@index')->name('search::index');
        Route::any('/latest', 'SearchController@getRecentDocuments')->name('search::latest');
        Route::any('/all', 'SearchController@getAllDocuments')->name('search::all');
        Route::post('/documents', 'SearchController@searchDocuments')->name('search::documents');
        Route::post('/update', 'SearchController@updateSearch')->name('search::update-search');
    });

    Route::group(['prefix' => 'document'], function() {
        Route::any('/get/{file}', 'ViewDocumentController@download')->name('document::download');
        Route::get('/pdf', 'ViewDocumentController@showPdfViewer')->name('document::pdf_viewer');
        Route::any('/view/{id}', 'ViewDocumentController@view')->name('document::view');
        Route::any('/related/{categoryId}', 'ViewDocumentController@getRelatedDocuments')->name('document.related');
    });

    Route::group(['prefix' => 'blog'], function () {
        Route::get('/', 'BlogController@showHome')->name('blog::home');
        Route::get('/all', 'BlogController@showAllPosts')->name('blog::all');
        Route::get('/category/{id}-{slug}', 'BlogController@showCategoryPosts')->name('blog::category');
        Route::get('/post/{id}-{slug}', 'BlogController@showPost')->name('blog::post');
    });

    Route::group(['prefix' => 'admin', 'middleware' => ['role:Administrador|Superadmin']], function() {
        Route::get('/', 'HomeController@indexAdmin')->name('dashboard');

        Route::group(['prefix' => 'languages'], function(){
            Route::get('/', 'LanguageController@list')->name('admin::languages::list');
    
            Route::get('/create', 'LanguageController@create')->name('admin::languages::create');
            Route::post('/store', 'LanguageController@store')->name('admin::languages::store');
    
            Route::get('/edit/{id}', 'LanguageController@edit')->name('admin::languages::edit');
            Route::post('/update/{id}', 'LanguageController@update')->name('admin::languages::update');
        });

        Route::group(['prefix' => 'countries'], function(){
            Route::get('/', 'CountryController@list')->name('admin::countries::list');
    
            Route::get('/create', 'CountryController@create')->name('admin::countries::create');
            Route::post('/store', 'CountryController@store')->name('admin::countries::store');
    
            Route::get('/edit/{id}', 'CountryController@edit')->name('admin::countries::edit');
            Route::post('/update/{id}', 'CountryController@update')->name('admin::countries::update');
        });

        Route::group(['prefix' => 'states'], function(){
            Route::get('/', 'StateController@list')->name('admin::states::list');
    
            Route::get('/create', 'StateController@create')->name('admin::states::create');
            Route::post('/store', 'StateController@store')->name('admin::states::store');
    
            Route::get('/edit/{id}', 'StateController@edit')->name('admin::states::edit');
            Route::post('/update/{id}', 'StateController@update')->name('admin::states::update');
        });

        Route::group(['prefix' => 'sectors'], function(){
            Route::get('/', 'SectorController@list')->name('admin::sectors::list');
    
            Route::get('/create', 'SectorController@create')->name('admin::sectors::create');
            Route::post('/store', 'SectorController@store')->name('admin::sectors::store');
    
            Route::get('/edit/{id}', 'SectorController@edit')->name('admin::sectors::edit');
            Route::post('/update/{id}', 'SectorController@update')->name('admin::sectors::update');
        });

        Route::group(['prefix' => 'categories'], function(){
            Route::get('/', 'CategoryController@list')->name('admin::categories::list');
    
            Route::get('/create', 'CategoryController@create')->name('admin::categories::create');
            Route::post('/store', 'CategoryController@store')->name('admin::categories::store');
    
            Route::get('/edit/{id}', 'CategoryController@edit')->name('admin::categories::edit');
            Route::post('/update/{id}', 'CategoryController@update')->name('admin::categories::update');

            Route::get('/delete/{id}', 'CategoryController@delete')->name('admin::categories::delete');
        });
    
        Route::group(['prefix' => 'users'], function(){
            Route::get('/', 'UserController@list')->name('admin::users::list');
            Route::any('/data', 'UserController@dataTable')->name('admin::users::data');
            Route::any('/download', 'UserController@downloadUserList')->name('admin::users::download');
    
            Route::get('/create', 'UserController@create')->name('admin::users::create');
            Route::post('/store', 'UserController@store')->name('admin::users::store');
    
            Route::get('/edit/{id}', 'UserController@edit')->name('admin::users::edit');
            Route::post('/update/{id}', 'UserController@update')->name('admin::users::update');

            Route::get('/delete/{id}', 'UserController@delete')->name('admin::users::delete');

            Route::get('/approval', 'UserApprovalController@showIndex')->name('admin::users_approval::list');
            Route::post('/approval/save', 'UserApprovalController@saveApproval')->name('admin::users_approval::save');
            Route::any('/approval/data', 'UserApprovalController@returnDataTable')->name('admin::users_approval::data');
            
            Route::any('/verification/send/{userId}', 'UserApprovalController@sendVerificationLink')->name('admin::users_verification::send');
        });

        Route::group(['prefix' => 'contact'], function() {
            Route::get('/', 'AdminContactController@list')->name('admin::contact::list');
            Route::any('/data', 'AdminContactController@returnDataTable')->name('admin::contact::data');
            Route::any('/view/{id}', 'AdminContactController@view')->name('admin::contact::view');
        });
    
        Route::group(['prefix' => 'blog'], function() {
            Route::group(['prefix' => 'categories'], function() {
                Route::get('/', 'BlogCategoryController@list')->name('admin::blog_categories::list');
                Route::any('/data', 'BlogCategoryController@dataTable')->name('admin::blog_categories::data');
        
                Route::get('/create', 'BlogCategoryController@create')->name('admin::blog_categories::create');
                Route::post('/store', 'BlogCategoryController@store')->name('admin::blog_categories::store');
        
                Route::get('/edit/{id}', 'BlogCategoryController@edit')->name('admin::blog_categories::edit');
                Route::post('/update/{id}', 'BlogCategoryController@update')->name('admin::blog_categories::update');

                Route::get('/delete/{id}', 'BlogCategoryController@delete')->name('admin::blog_categories::delete');
            });

            Route::get('/', 'BlogContentController@list')->name('admin::blog_posts::list');
            Route::any('/data', 'BlogContentController@dataTable')->name('admin::blog_posts::data');
    
            Route::get('/create', 'BlogContentController@create')->name('admin::blog_posts::create');
            Route::post('/store', 'BlogContentController@store')->name('admin::blog_posts::store');
    
            Route::get('/edit/{id}', 'BlogContentController@edit')->name('admin::blog_posts::edit');
            Route::post('/update/{id}', 'BlogContentController@update')->name('admin::blog_posts::update');

            Route::get('/delete/{id}', 'BlogContentController@delete')->name('admin::blog_posts::delete');
        });

        Route::group(['prefix' => 'documents'], function() {
            Route::get('/', 'DocumentController@list')->name('admin::documents::list');
            Route::any('/data', 'DocumentController@dataTable')->name('admin::documents::data');
    
            Route::get('/create', 'DocumentController@create')->name('admin::documents::create');
            Route::post('/store', 'DocumentController@store')->name('admin::documents::store');
    
            Route::get('/edit/{id}', 'DocumentController@edit')->name('admin::documents::edit');
            Route::post('/update/{id}', 'DocumentController@update')->name('admin::documents::update');

            Route::get('/delete/{id}', 'DocumentController@delete')->name('admin::documents::delete');
        });

        Route::group(['prefix' => 'reports'], function() {
            Route::any('/general', 'ReportController@showGeneralReports')->name('report::general');
            Route::any('/documents_stats', 'ReportController@getStatsDocuments')->name('report::documents_stats');
            Route::any('/blog_stats', 'ReportController@getStatsBlog')->name('report::blog_stats');
            Route::any('/search_category_stats', 'ReportController@getStatsCategorySearch')->name('report::search_category_stats');
            Route::any('/search_word_stats', 'ReportController@getStatsWordSearch')->name('report::search_word_stats');
            Route::any('/user_logins', 'ReportController@getUserLogins')->name('report::user_logins');
            Route::any('/general_searches', 'ReportController@getGeneralSearches')->name('report::general_searches');
            Route::any('/general_downloads', 'ReportController@getGeneralDownloads')->name('report::general_downloads');
            Route::get('/summary_reports', 'ReportController@showSummaryForm')->name('report::summary_reports');
            Route::post('/summary_reports', 'ReportController@getSummaryReports')->name('report::summary_reports::post');

            Route::get('/documents_trends', 'ReportController@showDocTrendReport')->name('report::page::documents_trends');
            Route::any('/documents_trends/data', 'ReportController@getTrendDocuments')->name('report::documents_trends');

            Route::get('/user_activity', 'ReportController@showUserReport')->name('report::page::user_activity');
            Route::any('/user_activity/data', 'ReportController@getUserActivity')->name('report::user_activity');

            Route::post('/dowload', 'ReportController@downloadReport')->name('report::download');
        });
    });
});
