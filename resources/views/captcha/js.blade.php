<script src="https://www.google.com/recaptcha/api.js?render={{ $siteKey }}"></script>
<script type="text/javascript">
    function recaptchaAction(e) {       
        /*grecaptcha.ready(() => {
            grecaptcha.execute('{{ $siteKey }}', {action: 'submit'}).then(token => {
                document.getElementById('g-recaptcha-response').value = token;
                document.getElementsByClassName('captcha-form')[0].submit();
            });
        });*/
    };

    ($ => {
        $(document).ready(() => {
            console.log($);
            $(".captcha-form").submit((e) => {
                let $form = $(e.target);

                grecaptcha.ready(() => {
                    grecaptcha.execute('{{ $siteKey }}', {action: 'submit'}).then(token => {
                        $('#g-recaptcha-response').val(token);
                        $form.unbind('submit').submit();
                    });
                });
            });
        });
    })(jQuery);
</script>