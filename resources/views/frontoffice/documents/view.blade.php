@extends('frontoffice.layouts.app')

@section('js')
<script src="{{ asset('/front/js/appRelatedDocs.js') }}"></script>
@endsection

@section('content')
<div id="app-search">
    @component('frontoffice.components.search_form', ['states' => $states])
    @endcomponent
    
    <div class="container view_document mb-5">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-lg-10">
                <div class="community-posts-wrapper bb-radius">
                    <div class="community-post style-two row">
                        <div class="post-content col-md-9">
                            <div class="entry-content">
                                <h3 class="post-title">
                                    {{ $document->title }}
                                </h3>
                                <ul class="meta mb-2">
                                    <li class="category" style="background-color: {{ $document->category->color }}">{{ $document->category->name() }}</li>
                                </ul>
                                <ul class="meta">
                                    <li><span class="flag {{ $document->language->abbreviation }}"></span> {{ $document->language->name }}</li>
                                    <li><i class="icon_calendar"></i>{{ __("Actualizado") }} {{ $document->created_at }}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-meta-wrapper col-md-3 text-sm-left-custom">
                            <ul class="post-meta-info">
                                <li class="circle-gray"><a href="{{ route('document::download', ['file' => $document->file]) }}"><i class="fas fa-cloud-download-alt"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.community-post -->
                </div>
                <br />
                <div id="canvases">
                    <iframe class="document_pdf_viewer" src="{{ route("document::pdf_viewer") }}?file={{ route("document::download", ["file" => $document->file]) }}?view"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="gray-section pt-5">
        <div class="container">
            <div class="row justify-content-md-center">
                <h2>{{ __("Documentos relacionados") }}</h2>
            </div>
            <div class="row">
                <input type="hidden" id="documentsUrl" value="{{ route("document.related", ['categoryId' => $document->category_id]) }}" />
                <input type="hidden" id="locale" value="{{ App\Helpers\LocaleHelper::getLocale() }}" />

                <div id="app-doc-list">
                    <document-list />
                </div>
            </div>
        </div>
    </div>
</div>
@stop