@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
@php
/*@component('frontoffice.components.search_form', ['categories' => $docCategories])
@endcomponent*/
@endphp

<section class="mt-40">
    <div class="container">
        <div class="breadcrumb_content">
            <br />
            <h2>{{ $post->title() }}</h2>
            <div class="single_post_author">
                <!--img class="author_img" src="img/blog-single/author_single.jpg" alt=""-->
                <div class="text">
                    <a href="#">
                        <h4>{{ $post->author->name }}</h4>
                    </a>
                    <div class="post_tag">
                        <a href="#">{{ $post->created_at }}</a>
                        @foreach ($post->categories as $category)
                        <a class="c_blue" href="{{ route('blog::category', ['id' => $category->id, 'slug' => $category->slug]) }}">
                            {{ $category->name() }}
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="blog_single_info">
                    <div class="blog_single_item">
                        <div class="blog_single_img">
                            <img src="{{ asset($post->getImageUrl('inner')) }}" alt="" class="img-fluid w-100" />
                        </div>
                        {!! $html !!}
                    </div>

                    @if(!empty($related) && count($related) > 0)
                    <div class="blog_related_post">
                        <h2 class="c_head">{{ __("Post relacionados") }}</h2>
                        <div class="row">
                            @include('frontoffice.blog.post_list', ['posts' => $related])
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog_sidebar pl-40">
                    <div class="widget categorie_widget">
                        <h4 class="c_head">{{ __("Categorías") }}</h4>
                        <ul class="list-unstyled categorie_list">
                            @foreach ($categories as $category)
                            <li>
                                <a href="{{ route('blog::category', ['id' => $category->id, 'slug' => $category->slug]) }}">
                                    {{ $category->name() }} <span>({{ $category->posts_count }})</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @if(!empty($recents) && count($recents) > 0)
                    <div class="widget recent_news_widget">
                        <h4 class="c_head">{{ __("Posts recientes") }}</h4>
                        @foreach($recents as $recent)
                        <div class="media recent_post_item">
                            <img src="{{ asset($recent->getImageUrl('tiny')) }}" alt="" class="img-fluid" />
                            <div class="media-body">
                                <a href="{{ route("blog::post", ['id' => $recent->id, 'slug' => $recent->slug]) }}">
                                    <h5>{{ $recent->title() }}</h5>
                                </a>
                                <div class="entry_post_date">{{ $recent->created_at }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                    <!--div class="widget tag_widget">
                        <h4 class="c_head">Tags</h4>
                        <ul class="list-unstyled w_tag_list">
                            <li><a href="#">Design</a></li>
                            <li><a href="#">Apps</a></li>
                            <li><a href="#">Photography</a></li>
                        </ul>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</section>
@stop
