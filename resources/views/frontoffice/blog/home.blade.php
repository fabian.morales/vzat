@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
@php
/*@component('frontoffice.components.search_form', ['categories' => $docCategories])
@endcomponent*/
@endphp

<section class="blog_top_post_area sec_pad bg_color">
    <div class="container">
        <div class="row blog_top_post flex-row-reverse">
            <div class="col-lg-7 p_top_img">
                <img class="p_img img-fluid" src="{{ asset($lastPost->getImageUrl('featured')) }}" alt="" />
            </div>
            <div class="col-lg-5 p-0">
                <div class="b_top_post_content">
                    <div class="post_tag">
                        @foreach ($lastPost->categories as $category)
                        <a class="c_blue" href="{{ route('blog::category', ['id' => $category->id, 'slug' => $category->slug]) }}">
                            {{ $category->name() }}
                        </a>
                        @endforeach
                    </div>
                    <a href="{{ route("blog::post", ['id' => $lastPost->id, 'slug' => $lastPost->slug]) }}">
                        <h3>{{ $lastPost->title() }}</h3>
                    </a>
                    <p>{{ $lastPost->getDescription() }}</p>
                    <a href="{{ route("blog::post", ['id' => $lastPost->id, 'slug' => $lastPost->slug]) }}" class="learn_btn">{{ __("Continuar leyendo") }}<i class="arrow_right"></i></a>
                    <div class="media post_author">
                        <!--div class="round_img">
                            <img src="img/blog-grid/ansley.jpg" alt="">
                        </div-->
                        <div class="media-body author_text">
                            <h4>{{ $lastPost->author->name }}</h4>
                            <div class="date">{{ $lastPost->created_at }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="doc_blog_grid_area">
    <div class="blog_grid_inner bg_color">
        <div class="container">
            <ul class="nav blog_tab nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#all-posts" data-toggle="tab" role="tab" aria-controls="all-posts-tab" aria-selected="true">
                        {{ __("Todos") }}
                    </a>
                </li>
                @foreach($categories as $category)
                <li class="nav-item">
                    <a class="nav-link" href="#{{ $category->slug }}" data-toggle="tab" role="tab" aria-controls="{{ $category->slug }}" aria-selected="{{ $loop->first ? 'true' : 'false' }}">
                        {{ $category->name() }}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade active show" id="all-posts" role="tabpanel" aria-labelledby="all-posts-tab">
            <div class="container">
                <div class="row blog_grid_tab">
                    @include('frontoffice.blog.post_list', ['posts' => $posts])
                    <div class="col-sm-12 text-center">
                        <br />
                        <p><a href="{{ route('blog::all') }}" class="btn btn-blue wide">{{ __("Más entradas") }} <i class="fas fa-arrow-right"></i></a></p>
                    </div>
                </div>
            </div>
        </div>

        @foreach($categories as $category)

        @if(!empty($category->posts))
        <div class="tab-pane fade" id="{{ $category->slug }}" role="tabpanel" aria-labelledby="{{ $category->slug }}-tab">
            <div class="container">
                <div class="row blog_grid_tab">
                    @include('frontoffice.blog.post_list', ['posts' => $category->posts->take(6)])
                    <div class="col-sm-12 text-center">
                        <br />
                        <p><a href="{{ route('blog::category', ['id' => $category->id, 'slug' => $category->slug]) }}" class="btn btn-blue wide">{{ __("Más entradas") }} <i class="fas fa-arrow-right"></i></a></p>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @endforeach
    </div>
</section>
@stop
