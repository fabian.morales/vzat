@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
@php
/*@component('frontoffice.components.search_form', ['categories' => $docCategories])
@endcomponent*/
@endphp

<section class="mt-40">
    <div class="container">
        <br />
        <h2 class="text-center">{{ $category->name() }}</h2>
    </div>
</div>
<section class="doc_blog_grid_area">
    <div class="container">
        <div class="row blog_grid_tab">
        @include('frontoffice.blog.post_list', ['posts' => $category->posts])
        </div>
    </div>
</section>
@stop