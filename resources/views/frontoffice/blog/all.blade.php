@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
@php
/*@component('frontoffice.components.search_form', ['categories' => $docCategories])
@endcomponent*/
@endphp

<section class="doc_blog_grid_area">
    <div class="container">
        <div class="row blog_grid_tab">
        @include('frontoffice.blog.post_list', ['posts' => $posts])
        </div>
    </div>
</section>
@stop