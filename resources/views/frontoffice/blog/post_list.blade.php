@foreach ($posts as $post)
<div class="col-lg-4 col-sm-6">
    <div class="blog_grid_post fadeInUp">
        <img src="{{ asset($post->getImageUrl('thumbnail')) }}" alt="" class="img-fluid" />
        <div class="grid_post_content">
            <div class="post_tag">
                @foreach ($post->categories as $category)
                <a class="c_blue" href="{{ route('blog::category', ['id' => $category->id, 'slug' => $category->slug]) }}">
                    {{ $category->name() }}
                </a>
                @endforeach
            </div>
            <a href="{{ route("blog::post", ['id' => $post->id, 'slug' => $post->slug]) }}">
                <h4 class="b_title">{{ $post->title() }}</h4>
            </a>
            <p>{{ $post->getDescription() }}...</p>
            <div class="media post_author">
                <!--div class="round_img">
                    <img src="img/blog-grid/author_1.jpg" alt="">
                </div-->
                <div class="media-body author_text">
                    <h4>{{ $post->author->name }}</h4>
                    <div class="date">{{ $post->created_at }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
