<div class="container">
    <div class="row relative">
        <div class="alert_container">
            <div class="alert media message_alert alert-{{ $class }} fade show" role="alert">
                <div class="media-body">
                    <h5>{{ $title }}</h5>
                    <p>{{ $message }}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="icon_close"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>