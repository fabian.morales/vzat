@php
    $auth = Auth::check();
    $lang = App\Helpers\LocaleHelper::getLocale();
@endphp

<span class="btn-lang {{ $class }}">
    @if ($lang == "en")
    <a href="{{ route("locale::switch", ["lang" => "es"]) }}">{{ $auth ? "ES" : "Español" }}</a>
    @else
    <span>{{ $auth ? "ES" : "Español" }}</span>
    @endif

    &nbsp;|&nbsp;

    @if ($lang == "es")
    <a href="{{ route("locale::switch", ["lang" => "en"]) }}">{{ $auth ? "EN" : "English" }}</a>
    @else
    <span>{{ $auth ? "EN" : "English" }}</span>
    @endif

</span>