<section class="contact_block">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8 text-center text-md-left">
                        <h2>{{ $title }}</h2>
                        <p>{{ $text }}</p>
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="{{ route('contact') }}" class="btn btn-yellow wide">{{ __("Contáctanos") }} &nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>