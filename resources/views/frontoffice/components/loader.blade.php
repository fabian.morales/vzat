<!--?xml version="1.0" encoding="utf-8"?-->
<!-- Generator: Adobe Illustrator 26.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)    -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve" width="400" height="100%" style="max-width: 400px;">
<style type="text/css">
	.st0{fill:#254061;stroke:#254061;stroke-width:0.75;stroke-miterlimit:10;}
</style>
<g>
	<g>
		<path class="st0 svg-elem-1" d="M692,474.8v-47.5h43.6v47.5H692z"></path>
	</g>
	<g>
		<path class="st0 svg-elem-2" d="M237.1,649.5l-54.3-158.6h45.7l33.4,108.3h0.6L296,490.9h43.3l-53.7,158.6H237.1z"></path>
		<path class="st0 svg-elem-3" d="M430,523.7h-76.4v-32.8h133.8v32.8l-82.5,93h88.4v32.8H347.5v-32.8L430,523.7z"></path>
		<path class="st0 svg-elem-4" d="M508.6,539.6c0.6-10.2,3.2-18.7,7.7-25.5s10.2-12.2,17.2-16.3c7-4.1,14.8-7,23.5-8.7
			c8.7-1.7,17.4-2.6,26.2-2.6c8,0,16,0.6,24.2,1.7c8.2,1.1,15.7,3.3,22.4,6.6c6.8,3.3,12.3,7.8,16.6,13.6c4.3,5.8,6.4,13.5,6.4,23.2
			v82.5c0,7.2,0.4,14,1.2,20.6c0.8,6.5,2.2,11.5,4.3,14.7h-44.2c-0.8-2.5-1.5-5-2-7.5c-0.5-2.5-0.9-5.2-1.1-7.8
			c-7,7.2-15.1,12.2-24.5,15c-9.4,2.9-19,4.3-28.8,4.3c-7.6,0-14.6-0.9-21.2-2.8c-6.6-1.8-12.3-4.7-17.2-8.6
			c-4.9-3.9-8.7-8.8-11.5-14.7c-2.8-5.9-4.1-13-4.1-21.2c0-9,1.6-16.4,4.8-22.2c3.2-5.8,7.3-10.5,12.3-14c5-3.5,10.7-6.1,17.2-7.8
			s12.9-3.1,19.5-4.1c6.5-1,13-1.8,19.3-2.5c6.3-0.6,12-1.5,16.9-2.8s8.8-3,11.7-5.4c2.9-2.3,4.2-5.8,4-10.3c0-4.7-0.8-8.4-2.3-11.2
			c-1.5-2.8-3.6-4.9-6.1-6.4c-2.6-1.5-5.5-2.5-8.9-3.1c-3.4-0.5-7-0.8-10.9-0.8c-8.6,0-15.3,1.8-20.2,5.5s-7.8,9.8-8.6,18.4
			L508.6,539.6L508.6,539.6z M609.2,571.8c-1.8,1.6-4.1,2.9-6.9,3.8c-2.8,0.9-5.7,1.7-8.9,2.3c-3.2,0.6-6.5,1.1-10,1.5
			c-3.5,0.4-7,0.9-10.4,1.5c-3.3,0.6-6.5,1.4-9.7,2.5c-3.2,1-5.9,2.4-8.3,4.1c-2.4,1.7-4.2,3.9-5.7,6.6s-2.2,6-2.2,10.1
			c0,3.9,0.7,7.2,2.2,9.8c1.4,2.7,3.4,4.8,5.8,6.3c2.5,1.5,5.3,2.6,8.6,3.2s6.6,0.9,10.1,0.9c8.6,0,15.2-1.4,19.9-4.3
			c4.7-2.9,8.2-6.3,10.4-10.3c2.2-4,3.6-8,4.1-12.1c0.5-4.1,0.8-7.4,0.8-9.8V571.8z"></path>
		<path class="st0 svg-elem-5" d="M767.5,489.5v34.2h-31.9v75.2c0,7.4,1.2,12.3,3.7,14.7c2.5,2.5,7.4,3.7,14.7,3.7c2.5,0,4.8-0.1,7.1-0.3
			c2.2-0.2,4.4-0.5,6.4-0.9v33.8c-3.7,0.6-7.8,1-12.3,1.2c-4.5,0.2-8.9,0.3-13.2,0.3c-6.8,0-13.1-0.5-19.2-1.4
			c-6-0.9-11.3-2.7-16-5.4c-4.6-2.7-8.2-6.4-10.9-11.3c-2.7-4.9-4-11.3-4-19.3v-90.2h-26.4v-34.2h26.4l0,0h43.6l0,0L767.5,489.5
			L767.5,489.5z"></path>
	</g>
</g>
</svg>