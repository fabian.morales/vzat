<section class="breadcrumb_area search">
    <div class="container custom_container">
        <form action="{{ route('search::index') }}" class="banner_search_form banner_search_form_two" method="POST">
            @csrf
            <div class="input-group">
                <input type="search" name="text_search" id="text_search" class="form-control" placeholder="{{ __("Buscar reportes") }}">
                <div class="input-group-append">
                    <select class="custom-select" id="inlineFormCustomSelect" name="state_id" id="state_id">
                        <option value="" selected>{{ __("Todos los estados") }}</option>
                        @foreach($states as $state)
                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit"><i class="icon_search"></i></button>
            </div>
        </form>
    </div>
</section>