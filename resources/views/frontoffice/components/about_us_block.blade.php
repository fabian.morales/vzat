<section class="about_us_block">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h2>{{ __("Conoce más sobre Venezuela Analysis Team") }}</h2>
                        <a href="{{ route('about') }}" class="btn btn-yellow wide">{{ __("Quiénes somos") }} &nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>