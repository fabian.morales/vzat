<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    {!! SEOMeta::generate() !!}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset("/front/img/favicon.ico") }}" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset("/front/assets/bootstrap/css/bootstrap.min.css") }}">
    <!-- icon css-->
    <link rel="stylesheet" href="{{ asset("/front/assets/font-awesome/css/all.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/assets/elagent-icon/style.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/assets/animation/animate.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/assets/niceselectpicker/nice-select.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/css/style.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/css/responsive.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/css/preloader.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/css/app.css") }}">

    @section('css')
    @show

    @if(App::environment(['staging', 'production']))
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-54371236-32"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-54371236-32');
    </script>
    
    @endif
</head>
@php
    $isHome = Request::is('/');
    $isLogin = Request::is('login');
@endphp
<body data-scroll-animation="true">
    {{-- <div id="preloader">
        <div id="ctn-preloader" class="ctn-preloader">
            <div class="round_spinner">
                <div class="spinner"></div>
                <div class="text">
                    <img src="{{ asset("/front/img/spinner_logo.png") }}" alt="">
                    <!--h4><span>VZAT</span></h4-->
                </div>
            </div>
        </div>
    </div> --}}

    <div id="preloader">
        @include('frontoffice.components.loader')
    </div>

    <div class="body_wrapper @if($isLogin) login-page @endif">
        <nav class="navbar navbar-expand-lg menu_two @if($isLogin) transparent @endif" id="sticky">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home') }}">
                    @if($isLogin)
                        <img class="img-logo" src="{{ asset("/front/img/white-logo.png") }}" alt="logo" />
                    @else
                        <img class="img-logo" src="{{ asset("/front/img/logo.png") }}" srcset="{{ asset("/front/img/logo-2x.png") }} 2x" alt="logo" />
                    @endif
                </a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="menu_toggle">
                        <span class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                        <span class="hamburger-cross">
                            <span></span>
                            <span></span>
                        </span>
                    </span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav menu dk_menu ml-auto">
                        <li class="nav-item">
                            <a href="{{ route('home') }}" class="nav-link" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ __("Inicio") }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('products') }}" class="nav-link" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ __("Nuestros productos") }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('about') }}" class="nav-link" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ __("Quiénes somos") }}
                            </a>
                        </li>
                        @hasanyrole('Administrador|Usuario')
                        <li class="nav-item">
                            <a href="{{ route('front::dashboard') }}" class="nav-link" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ __("Tableros") }}
                            </a>
                        </li>
                        @endhasanyrole
                        <li class="nav-item">
                            <a href="{{ route('contact') }}" class="nav-link" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ __("Contacto") }}
                            </a>
                        </li>
                    </ul>
                    @hasanyrole('Administrador')
                    <a class="nav_btn" href="{{ route('dashboard') }}"><i class="fas fa-user-cog"></i>{{ __("Administrador") }}</a>
                    @endhasanyrole

                    @hasanyrole('Administrador|Usuario')
                    <a class="nav_btn nav_btn_red" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i>{{ __("Cerrar sesión") }}</a>
                    @endhasanyrole

                    @include('frontoffice.components.language_button', ['class' => "box"])

                </div>
            </div>
        </nav>

        @if(session('message_error'))
        @component('frontoffice.components.alert')
            @slot('title')
            {{ __("Atención") }}
            @endslot

            @slot('class')
            danger
            @endslot

            @slot('message')
            {{ session('message_error') }}
            @endslot
        @endcomponent
        @endif

        @if(session('message_success'))
        @component('frontoffice.components.alert')
            @slot('title')
            {{ __("Aviso") }}
            @endslot

            @slot('class')
            success
            @endslot

            @slot('message')
            {{ session('message_success') }}
            @endslot
        @endcomponent
        @endif

        @if(session('status'))
        @component('frontoffice.components.alert')
            @slot('title')
            {{ __("Aviso") }}
            @endslot

            @slot('class')
            success
            @endslot

            @slot('message')
            {{ session('status') }}
            @endslot
        @endcomponent
        @endif

        @if(session('status.error'))
        @component('frontoffice.components.alert')
            @slot('title')
            {{ __("Error") }}
            @endslot

            @slot('class')
            danger
            @endslot

            @slot('message')
            {{ session('status.error') }}
            @endslot
        @endcomponent
        @endif

        @yield('content')

        @section('footer')
        <footer class="footer_area footer_p_top f_bg_color">
            @php
            /*<div class="footer_top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            
                        </div>
                        <div class="col-lg-3 col-sm-6">

                        </div>
                    </div>
                    <div class="border_bottom"></div>
                </div>
            </div>*/
            @endphp
            <div class="footer_bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 text-center text-md-left"><p>© {{ date('Y') }} {{ __("Todos los derechos reservados por") }} <a href="{{ route("home") }}">VZAT</a></p></div>
                        <div class="col-md-5 text-center text-md-right">
                            <p>
                                <a href="{{ route('contact') }}">{{ __("Contacto") }}</a> &bull;
                                @include('frontoffice.components.language_button', ['class' => ""])
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        @show
    </div>

    @php
    /*<!-- Back to top button -->
    <a id="back-to-top" title="Back to Top"></a>*/
    @endphp

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset("/front/js/jquery-3.2.1.min.js") }}"></script>
    <script src="{{ asset("/front/js/pre-loader.js") }}"> </script>
    <script src="{{ asset("/front/assets/bootstrap/js/popper.min.js") }}"></script>
    <script src="{{ asset("/front/assets/bootstrap/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset("/front/assets/wow/wow.min.js") }}"></script>
    <script src="{{ asset("/front/assets/niceselectpicker/jquery.nice-select.min.js") }}"></script>
    <script src="{{ asset("/front/js/main.js") }}"></script>

    @section('js')
    @show
</body>
</html>
