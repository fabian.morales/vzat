@extends('frontoffice.layouts.app')

@section('js')
@if(App::environment(['staging', 'production']))
{!! Captcha::renderJs() !!}
@endif
@endsection

@section('content')
<section class="breadcrumb_area contact">
    <div class="container custom_container">
        <div class="text-center">
            <h2>{{ __("Contacto") }}</h2>
        </div>
    </div>
</section>

<section class="mb-4">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-9">
                <br />
                <div class="text-center">
                    @php /*<h2>{{ __("Póngase en contacto con nosotros") }}</h2>*/ @endphp
                    <p>&nbsp;</p>
                    <p>{{ __("Estaremos en contacto lo más pronto posible") }}</p>
                    <p>{{ __("Si desea crear una cuenta en VZAT por favor regístrese en nuestra") }} <a href="{{ route('home') }}">{{ __("página de inicio") }}</a></p>
                </div>
                <br />
                <form method="POST" action="{{ route('contact::submit') }}" class="row login_form captcha-form">
                    @csrf
                    <div class="col-md-6 form-group">
                        <div class="small_text">{{ __('Nombre') }}</div>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="{{ __("Nombre completo") }}" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="small_text">{{ __("Su correo") }}</div>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __("info@ejemplo.com") }}">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="small_text">{{ __('Organización') }}</div>
                        <input id="entity" type="text" class="form-control @error('entity') is-invalid @enderror" name="entity" value="{{ old('entity') }}" required autocomplete="entity" autofocus placeholder="{{ __("Organización para la que trabaja") }}">

                        @error('entity')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="small_text">{{ __('País') }}</div>
                        <select id="country_id" class="form-control @error('country_id') is-invalid @enderror" name="country_id" required autocomplete="country" autofocus>
                            <option>{{ __("Seleccionar país") }}</option>
                            @foreach($countries as $country)
                            <option value="{{ $country->id }}" @if($country->id == old('country_id')) selected @endif>{{ $country->name() }}</option>
                            @endforeach
                        </select>
                        @error('country_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __("Mensaje") }}</div>
                        <textarea id="message" class="form-control @error('message') is-invalid @enderror" name="message" placeholder="{{ __("Escriba aquí su mensaje") }}" rows="5">{{ old('message') }}</textarea>
                        @error('message')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <div class="col-lg-12 text-center">
                        @if(App::environment(['staging', 'production']))
                            {!! Captcha::renderButton() !!}
                        @endif
                        <button type="submit" class="btn action_btn thm_btn btn-blue">{{ __('Enviar') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop