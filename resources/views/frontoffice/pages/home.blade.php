@extends('frontoffice.layouts.app')
@section('js')
<script src="{{ asset('/front/js/app.js') }}"></script>
@endsection
@section('content')
<div>
    <input type="hidden" id="searchUrl" value="{{ route("search::documents") }}" />
    <input type="hidden" id="latestUrl" value="{{ route("search::latest") }}" />
    <input type="hidden" id="allDocumentsUrl" value="{{ route("search::all") }}" />
    <input type="hidden" id="locale" value="{{ App\Helpers\LocaleHelper::getLocale() }}" />
    <input type="hidden" id="isPost" value="{{ $isPost ? 'true' : 'false' }}" />
    <input type="hidden" id="textSearch" value="{{ $textSearch }}" />
    <input type="hidden" id="categoryId" value="{{ $categoryId }}" />
    <input type="hidden" id="stateId" value="{{ $stateId }}" />
    <input type="hidden" id="aboutUsUrl" value="{{ route('about') }}" />
    <input type="hidden" id="updateSearchUrl" value="{{ route('search::update-search') }}" />    

    <div id="app-search">
        <search-container />
    </div>
</div>
@stop