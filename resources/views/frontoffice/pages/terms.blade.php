@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
<section class="doc_documentation_area">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-9">
                <div class="content">
                    <h1>Términos y condiciones</h1>
                    <br />
                    <p><strong>Créditos</strong></p>
                    <p>Este sitio web y su contenido fueron desarrollados con la asistencia financiera de la Unión Europea. Las opiniones expresadas en él no deben considerarse, en modo alguno, como una expresión de la opinión oficial de la Unión Europea, y la Comisión Europea no se hace responsable del uso que pueda hacerse de la información que contiene.</p>
                    <p>Los informes del Equipo de Análisis de Venezuela (VZAT), así como toda la información contenida en este sitio web son propiedad del VZAT.  El acceso a esta información y a los análisis contenidos en este sitio web, son proporcionados sobre una base estrictamente no atribuible y únicamente para el uso interno de los usuarios, dirigido con el fin de informar sobre la elaboración de políticas de asistencia humanitaria en Venezuela.  No se permite la copia o distribución de esta información sin la aprobación previa de la VZAT.</p>
                    <p>El Equipo de Análisis de Venezuela (VZAT) se compromete a respetar y proteger los derechos de privacidad de los visitantes de nuestro sitio web, y a ser transparente sobre el tratamiento de sus datos. La siguiente información detalla cómo la VZAT recoge, protege, administra y utiliza los datos que recibimos.</p>
                    <br />
                    <h1>Política de privacidad</h1>
                    <br />
                    <p><strong>¿Cómo obtenemos su información personal?</strong></p>
                    <p>Usted nos proporciona su información cuando registra una cuenta a través de un formulario en nuestra página web, cuando se pone en contacto con nosotros por correo electrónico, cuando habla con nosotros en persona o cuando nos proporciona información a través de otros medios.</p>
                    <p><strong>¿Qué tipo de información recopilamos?</strong></p>
                    <p>La información personal es toda aquella que puede ser usada para identificarle. Por ejemplo, puede incluir su nombre, dirección, número de teléfono, dirección de correo electrónico personal o laboral. Sólo recopilamos información personal que es relevante para el tipo de interacción que usted tiene con VZAT.</p>
                    <p><strong>¿Cómo utilizamos su información?</strong></p>
                    <p>Podemos utilizar su información de las siguientes maneras:</p>
                    <ul>
                        <li>Para mantener un registro de tu interacción con VZAT.</li>
                        <li>Para atender quejas o situaciones especiales.</li>
                        <li>Para proporcionarle la información que usted haya solicitado.</li>
                        <li>Para entender sus necesidades y deseos con el fin de proporcionarle el mejor servicio personalizado posible.</li>
                        <li>Para llevar a cabo análisis e investigaciones que nos ayuden a entender nuestro desempeño y para optimizar nuestra comunicación con usted</li>
                    </ul>
                    <p>El sistema utilizado para gestionar la administración de nuestra base de datos del sitio web se llama PhpMyAdmin y es proporcionado por Colombia Hosting S.A.S. Su política de privacidad se puede encontrar <a href="https://www.colombiahosting.com.co/politica-datos-personales" target="_blank">aquí</a>.</p>
                    <p>Cada vez que nos proporcione sus datos, se incluirán preguntas claras sobre sus preferencias en cuanto a nuestras comunicaciones con usted. En cualquier momento, usted puede hacernos saber si desea retirar su consentimiento o si quiere cambiar sus preferencias contactando con info@vzat.org.</p>
                    <p><strong>¿Cuánto tiempo conservamos su información?</strong></p>
                    <p>Conservaremos su información personal en nuestros sistemas durante el tiempo que sea necesario para la actividad pertinente.  En general, trataremos cualquier consentimiento que nos dé con una duración no superior a 36 meses (3 años) a partir de la fecha en que otorgue su consentimiento, o de la fecha de la última interacción que tenga con nosotros.</p>
                    <p>Si en algún momento retira su consentimiento o cambia sus preferencias, se actuará de inmediato.</p>
                    <p><strong>¿Con quién compartimos su información?</strong></p>
                    <p>VZAT sólo utilizará su información dentro de nuestra organización para los fines para los que fue proporcionada y para nuestra propia planificación y análisis. No venderemos, bajo ninguna circunstancia, sus datos personales a terceros. No recibirá marketing de ninguna otra compañía, organización benéfica u organización como resultado de darnos sus datos.</p>
                    <p><strong>Acceso y actualización de su información personal</strong></p>
                    <p>Si nos avisa de cualquier cambio en sus circunstancias personales, siempre cumpliremos con sus deseos. Nuestros niveles de servicio para la comunicación es:</p>
                    <ul>
                        <li>Correo electrónico - dos días hábiles a partir de la recepción de su solicitud</li>
                    </ul>
                    <p>En ocasiones podríamos ponernos en contacto con usted por algo que no esté relacionado con nuestros informes. La exclusión de las comunicaciones por correo electrónico no detendrá este tipo de comunicación. Si tiene alguna preocupación sobre este tipo de contacto, por favor póngase en contacto con nosotros.</p>
                    <p><strong>Contáctenos</strong></p>
                    <p>Usted tiene el control de sus datos personales y tiene el derecho de solicitar acceso a la información que tenemos sobre usted. En caso de que lo desee, póngase en contacto con nosotros en info@vzat.org</p>
                    <p><i>Oficina del Comisionado de Información</i></p>
                    <p>Si no hemos podido tratar adecuadamente su consulta o reclamación, usted puede remitir su asunto a la Oficina del Comisionado de Información (ICO). Sus datos de contacto son:</p>
                    <ul>
                        <li>Por correo: Oficina del Comisionado de Información</li>
                        <li>Casa Wycliffe</li>
                        <li>Water Lane</li>
                        <li>Wilmslow</li>
                        <li>Cheshire</li>
                        <li>SK9 5AF</li>
                        <li>Teléfono: 0303 123 1113 o 01625 545745</li>
                    </ul>
                    <p>Más información sobre la OIC y sobre cómo presentar una queja, o hacer una pregunta general sobre la protección de datos, está disponible en <a href="https://ico.org.uk/" target="_blank">su sitio web.</a></p>

                    <br />
                    <br />
                    <br />

                    <h1>Terms and Conditions</h1>
                    <br />
                    <p><strong>Acknowledgements</strong></p>
                    <p>This website and its contents were developed with the financial assistance of the European Union. The views expressed herein should not be taken, in any way, to reflect the official opinion of the European Union, and the European Commission is not responsible for any use that may be made of the information it contains.</p>
                    <p>All reporting from the Venezuela Analysis Team (VZAT), and any information contained in this website remains the property of VZAT.  Access to this information and analysis is provided on a strictly non-attributable basis for your own internal use only to inform policy making on humanitarian assistance in Venezuela.  No copying or distribution of this reporting is permitted without prior approval from VZAT.</p>
                    <br />
                    <h1>Privacy Policy</h1>
                    <br />
                    <p>The Venezuela Analysis Team (VZAT) is committed to respecting and protecting the privacy rights of visitors to our website, and to being transparent about what we do with your data. The following information details how VZAT collects, protects, manages and uses the data we receive.<p>
                    <p><strong>How do we obtain your personal information?</strong></p>
                    <p>You will give us your information when you register an account through a secure form on our website, when you contact us by email, speaking to you in person, or when you provide information to us via other media.</p>
                    <p><strong>What information do we collect?</strong></p>
                    <p>Personal information is any information that can be used to identify you. For example, this can include your name, address, telephone number, personal or work email address. We only collect personal information that is relevant to the type of interaction you have with VZAT.</p>
                    <p><strong>How do we use your information?</strong></p>
                    <p>We may use your information in the following ways:</p>
                    <ul>
                        <li>To keep a record of your engagement with VZAT.</li>
                        <li>To look into complaints or legal issues.</li>
                        <li>To give you information you have asked for.</li>
                        <li>To understand your needs and wishes in order to give you the best possible personalised service.</li>
                        <li>To carry out analysis and research which helps us understand how we are doing and improve our communications with you.</li>
                    </ul>
                    <p>The system used to manage the administration of our contact database is called <strong>PhpMyAdmin</strong> and is provided by <strong>Colombia Hosting S.A.S.</strong> Their privacy policy can be found <a href="https://www.colombiahosting.com.co/politica-datos-personales" target="_blank">here</a>.</p>
                    <p>Any time you provide your details to us, we will always include clear questions asking you about your preferences regarding our communications with you. You can also let us know if you wish to withdraw your consent or if you want to change your preferences at any time by contacting info@vzat.org.</p>
                    <p><strong>How long do we keep your information?</strong></p>
                    <p>We will hold your personal information on our systems for as long as is necessary for the relevant activity.  We will generally treat any consent that you give us as lasting no more than 36 months (3 years) from the date you give your consent, or from the date of the last interaction you have with us.</p>
                    <p>If at any time you withdraw your consent or change your preferences, your wishes will be acted on immediately.</p>
                    <p><strong>Who do we share your information with?</strong></p>
                    <p>VZAT will only use your information within our organisation for the purposes for which it was provided and for our own planning and analysis. We will not, under any circumstances, sell your personal data to any third party. You will not receive marketing from any other companies, charities or organisations as a result of giving your details to us.</p>
                    <p><strong>Accessing and Updating Your Personal Information</strong></p>
                    <p>If you let us know about any changes to your personal circumstances, we will always comply with your wishes. Our service levels for communication is:</p>
                    <ul>
                        <li>Email – two working days from the receipt of your request</li>
                    </ul>
                    <p>Sometimes we may have to contact you about something that is not related to our reports. Opting out of email communications will not stop this type of communication. If you have any concerns about this type of contact, please get in touch with us.</p>
                    <p><strong>Contact Us</strong></p>
                    <p>You are in control of your personal data and you have the right to request access to the information we hold about you. Should you wish to, you can use this please contact us at info@vzat.org</p>
                    <p><i>Information Commissioner’s Office</i></p>
                    <p>If we have been unable to deal appropriately with your question or complaint you have a right to refer the matter to the Information Commissioner’s Office (ICO). Their contact details are:</p>
                    <ul>
                        <li>By post: Information Commissioner's Office</li>
                        <li>Wycliffe House</li>
                        <li>Water Lane</li>
                        <li>Wilmslow</li>
                        <li>Cheshire</li>
                        <li>SK9 5AF</li>
                        <li>Telephone: 0303 123 1113 or 01625 545745</li>
                    </ul>
                    <p>More information about the ICO and about how to make a complaint, or ask a general question about data protection, is available on <a href="https://ico.org.uk/" target="_blank">their website</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@stop