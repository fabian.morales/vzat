@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
<section class="doc_documentation_area">
    <div class="inner_head about_us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center title">About VZAT</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-12 mb-5">
                <div class="content">
                    <h2 class="mb-5">The Venezuelan Analysis Team (VZAT) is an independent and impartial specialist analytical unit 
                    dedicated to a better understanding of the humanitarian crisis in Venezuela.</h2>
                    <p>Unlike other analysis 
                    agencies that solely reflect the needs and results of a crisis, VZAT takes a broader analytical 
                    approach by looking at the drivers, creating an accurate, detailed and current body of reporting 
                    that can be used to develop donor funding streams, programmatic responses and advocacy strategies 
                    that are sensitive to the causes and context of the emergency, as well as the needs. The goal of 
                    all VZAT products include:</p>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="card-info">
                    <img src="{{ asset('/front/img/info01.png') }}" class="icon" alt="" />
                    <p>Explaining the drivers, the impact, and the future trends of the Venezuelan humanitarian crisis.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-info">
                    <img src="{{ asset('/front/img/info02.png') }}" class="icon" alt="" />
                    <p>Addressing the requirements of decision-makers in the NGO sector, donor governments, and the public policy and advocacy communities for analytics and advice.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-info">
                    <img src="{{ asset('/front/img/info03.png') }}" class="icon" alt="" />
                    <p>Adding value to the international response by mapping and demystifying the changing crisis dynamics within the country.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@component('frontoffice.components.contact_block')
    @slot('title')
        {{ __("Si desea contactarse con el VZAT") }}
    @endslot

    @slot('text')
        {{ __("Puede visitar a nuestra sección de contacto") }}
    @endslot
@endcomponent

@stop