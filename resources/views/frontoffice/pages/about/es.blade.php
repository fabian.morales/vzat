@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
<section class="doc_documentation_area">
    <div class="inner_head about_us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center title">Acerca del VZAT</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-12 mb-5">
                <div class="content">
                    <h2 class="mb-5">El Equipo de An&aacute;lisis de Venezuela (VZAT) es una unidad anal&iacute;tica independiente, 
                    no partidista y sin fines de lucro, dedicada a facilitar la comprensi&oacute;n de la situaci&oacute;n 
                    humanitaria en Venezuela.</h1>
                    <p>El VZAT aplica un enfoque anal&iacute;tico que indaga respecto de los 
                    factores generadores de las crisis, a diferencia de otros equipos de an&aacute;lisis que principalmente 
                    reflejan las necesidades que afronta la poblaci&oacute;n y sus efectos humanitarios. A partir de este 
                    tipo de enfoque, el VZAT ha generado un repertorio de reportes precisos, minuciosos y oportunos a fin 
                    de facilitar la formulaci&oacute;n de programas de respuesta humanitaria, as&iacute; como el 
                    dise&ntilde;o de iniciativas de abogac&iacute;a sensibles, tanto a los factores determinantes y al 
                    contexto de la emergencia, como a las necesidades de las comunidades. Los productos elaborados por 
                    el VZAT persiguen los siguientes objetivos:</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-info">
                    <img src="{{ asset('/front/img/info01.png') }}" class="icon" alt="" />
                    <p>Determinar los factores desencadenantes, el impacto y las tendencias futuras de la situaci&oacute;n humanitaria en Venezuela.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-info">
                    <img src="{{ asset('/front/img/info02.png') }}" class="icon" alt="" />
                    <p>Responder a la demanda de an&aacute;lisis y servicios de asesoramiento, expresada por las instancias decisorias de las ONGs, los gobiernos  donantes y otras organizaciones dedicadas al dise&ntilde;o de pol&iacute;ticas p&uacute;blicas y a la abogac&iacute;a.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-info">
                    <img src="{{ asset('/front/img/info03.png') }}" class="icon" alt="" />
                    <p>Agregar valor a la respuesta internacional, al estudiar el dinamismo y la volatilidad de la situaci&oacute;n humanitaria en el pa&iacute;s.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@component('frontoffice.components.contact_block')
    @slot('title')
        {{ __("Si desea contactarse con el VZAT") }}
    @endslot

    @slot('text')
        {{ __("Puede visitar a nuestra sección de contacto") }}
    @endslot
@endcomponent

@stop