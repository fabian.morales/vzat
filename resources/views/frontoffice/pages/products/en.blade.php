@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
<section class="doc_documentation_area">
    <div class="inner_head products">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center title">Our products</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-6">
                <div class="content">
                    <h2 class="mb-5">VZAT applies a multi-disciplined and cross-cutting analysis of quantitative and qualitative data 
                    to provide granular holistic assessments and broader strategic analysis, scenario planning, and 
                    forecasting.</h2>
                    <p>This comprehensive approach focuses on themes such as governance and service provision, 
                    economic and social sectoral assessments, inter-communal relationships, context assessments, 
                    Venezuelan modern history, key stakeholders, and an understanding of the national and international 
                    political context that shapes the environment. VZAT provides a data-driven, up-to-date, conceptual 
                    framework across the entire Venezuela crisis responsive to the needs of decision-makers.</p>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <img src="{{ asset('/front/img/products.jpg') }}" class="img-fluid" alt="" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card-info">
                    <img src="{{ asset('/front/img/products.png') }}" class="icon" alt />
                    <h2>Products</h2>
                    <ul>
                        <li>Periodic reporting of crisis trends</li>
                        <li>Specific alerts and flash reports</li>
                        <li>Thematic deep-dive reports</li>
                        <li>Scenario and contingency plans</li>
                        <li>Risk analysis</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-info">
                    <img src="{{ asset('/front/img/services.png') }}" class="icon" alt />
                    <h2>Advisory Services</h2>
                    <ul>
                        <li>Country and regional strategies</li>
                        <li>Adaptive management decisions</li>
                        <li>Emergency preparedness planning</li>
                        <li>Early warning indicators monitoring</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@component('frontoffice.components.contact_block')
    @slot('title')
        {{ __("Si desea contactarse con el VZAT") }}
    @endslot

    @slot('text')
        {{ __("Puede visitar a nuestra sección de contacto") }}
    @endslot
@endcomponent

@stop