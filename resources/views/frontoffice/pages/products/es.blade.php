@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
<section class="doc_documentation_area">
    <div class="inner_head products">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center title">Nuestros productos</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-6">
                <div class="content">
                    <h2 class="mb-5">El VZAT emplea datos cuantitativos y cualitativos de manera multidisciplinar y transversal, 
                    con el fin de generar tanto an&aacute;lisis integrales detallados, como evaluaciones de escenarios 
                    y prospecciones de amplio alcance.</h2>
                    <p>A trav&eacute;s de este enfoque, se abordan temas como la 
                    gobernanza, la prestaci&oacute;n de servicios, el an&aacute;lisis de asuntos econ&oacute;micos y 
                    sociales, las relaciones comunitarias, los diagn&oacute;sticos de contexto, el estudio de la 
                    historia reciente del pa&iacute;s, los grupos de inter&eacute;s y la comprensi&oacute;n del 
                    escenario pol&iacute;tico internacional y sus repercusiones en el contexto pol&iacute;tico 
                    venezolano. El VZAT produce un marco conceptual basado en datos recientes, que le permite 
                    analizar la situaci&oacute;n humanitaria del pa&iacute;s y ajustarse a la demanda anal&iacute;tica 
                    de los tomadores de decisiones.</p>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <img src="{{ asset('/front/img/productos.jpg') }}" class="img-fluid" alt="" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card-info">
                    <img src="{{ asset('/front/img/products.png') }}" class="icon" alt />
                    <h2>Productos</h2>
                    <ul>
                        <li>Reportes peri&oacute;dicos de tendencias en el sector humanitario</li>
                        <li>Boletines informativos y alertas focalizadas</li>
                        <li>Informes tem&aacute;ticos de car&aacute;cter anal&iacute;tico</li>
                        <li>An&aacute;lisis de escenarios potenciales y Planes de contingencia</li>
                        <li>An&aacute;lisis de riesgo</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-info">
                    <img src="{{ asset('/front/img/services.png') }}" class="icon" alt />
                    <h2>Servicios de asesoramiento</h2>
                    <ul>
                        <li>Estrategias nacionales y regionales</li>
                        <li>Toma de decisiones y gesti&oacute;n adaptativa</li>
                        <li>Planes de preparaci&oacute;n ante emergencias</li>
                        <li>Monitoreo de indicadores de alerta temprana</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
@component('frontoffice.components.contact_block')
    @slot('title')
        {{ __("Si desea contactarse con el VZAT") }}
    @endslot

    @slot('text')
        {{ __("Puede visitar a nuestra sección de contacto") }}
    @endslot
@endcomponent

@stop