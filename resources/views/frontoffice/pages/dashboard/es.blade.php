@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
<section class="dashboard doc_documentation_area">
    <div class="inner_head about_us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center title">Tableros</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-md-center mb-5">
            <div class="col-md-12">
                <div class="content">
                    <h2>Tableros interactivos - Impacto de la pandemia sobre las mujeres venezolanas</h2>
                    <br />
                    <p>Para complementar la Serie: Impacto de la pandemia sobre las mujeres venezolanas, 
                    el VZAT ha consolidado los principales datos de esta Serie, en un conjunto de <strong>tableros 
                    interactivos</strong>. A través de estos tableros, sus lectores no solo podrán interactuar con 
                    estas estadísticas, sino segmentarlas según diversos criterios demográficos, entre ellos, 
                    el sexo de las personas encuestadas. Ahora bien, a fin de aprovechar al máximo estos 
                    datos, el VZAT también anima a sus lectores a articular estos resultados, con otros 
                    reportes analíticos publicados por organismos y organizaciones que operan en el país.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <h3>Índice de los tableros</h3>
            </div>
            <div class="col-6 col-md-3">
                Pág 1. Demográficos<br />
                Pág 2. ASH<br />
                Pág 3. Salud<br />
                Pág 4. Acceso a alimentos
            </div>
            <div class="col-6 col-md-3">
                Pág 5. Medios de vida<br />
                Pág 6. Asistencia comunitaria<br />
                Pág 7. Amenazas de violencia
            </div>
        </div>
    </div>
    <div class="gray-section">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                    <div class="content">
                        <div class="text-center">
                            <iframe class="dashboard-iframe" src="https://app.powerbi.com/view?r=eyJrIjoiMzRjY2I0MmItYTQxYS00ZjQxLWIyYzAtNzE1YTczMzMyZDhiIiwidCI6ImQ4NTg3N2QxLTEyMDgtNDU1Mi04NDdiLTA0OGU1Y2VhZjIyMSJ9&pageName=ReportSection757ef9ec0165a0e31020" frameborder="0" allowFullScreen="true"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@component('frontoffice.components.contact_block')
    @slot('title')
        {{ __("Si desea contactarse con el VZAT") }}
    @endslot

    @slot('text')
        {{ __("Puede visitar a nuestra sección de contacto") }}
    @endslot
@endcomponent

@stop