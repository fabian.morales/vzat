@extends('frontoffice.layouts.app')

@section('js')
@endsection

@section('content')
<section class="dashboard doc_documentation_area">
    <div class="inner_head about_us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center title">Dashboard</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-md-center mb-5">
            <div class="col-md-12">
                <div class="content">
                    <h2>Impact of the COVID pandemic on Venezuelan women – Interactive Dashboard</h2>
                    <br />
                    <p>To support its series on the impact of the COVID-19 pandemic on Venezuelan women, 
                    VZAT has designed a suite of <strong>interactive dashboards</strong> that summarize 
                    the core data to enable the reader to interact with the statistics and isolate the 
                    responses and demographic data according to the sex of the respondents. To make the most 
                    of this data, however, VZAT also strongly encourages all readers to combine the statistics 
                    contained in these dashboards with other analytical research and assessments published by 
                    reputable agencies and organizations operating in the country.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <h3>Dashboard Index</h3>
            </div>
            <div class="col-6 col-md-3">
                Page 1. Demographics<br />
                Page 2. WASH<br />
                Page 3. Health<br />
                Page 4. Access to food
            </div>
            <div class="col-6 col-md-3">
                Page 5. Livelihoods<br />
                Page 6. Community assistance<br />
                Page 7. Threats of violence
            </div>
        </div>
    </div>
    <div class="gray-section">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                    <div class="content">
                        <div class="text-center">
                            <iframe class="dashboard-iframe" src="https://app.powerbi.com/view?r=eyJrIjoiMzRjY2I0MmItYTQxYS00ZjQxLWIyYzAtNzE1YTczMzMyZDhiIiwidCI6ImQ4NTg3N2QxLTEyMDgtNDU1Mi04NDdiLTA0OGU1Y2VhZjIyMSJ9&pageName=ReportSection757ef9ec0165a0e31020" frameborder="0" allowFullScreen="true"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@component('frontoffice.components.contact_block')
    @slot('title')
        {{ __("Si desea contactarse con el VZAT") }}
    @endslot

    @slot('text')
        {{ __("Puede visitar a nuestra sección de contacto") }}
    @endslot
@endcomponent

@stop