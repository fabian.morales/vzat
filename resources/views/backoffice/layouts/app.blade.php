<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'VZAT') }}</title>

    <link rel="stylesheet" href="{{ asset('back/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('back/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('back/css/style.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    @section('css')
    @show
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-black navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ url('/') }}" class="nav-link">{{ __("Inicio") }}</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-user"></i>
                        <!--span class="badge badge-warning navbar-badge">15</span-->
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">{{ Auth::user()->name }}</span>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" class="dropdown-item">
                            <i class="fas fa-sign-out-alt mr-2"></i> {{ __("Cerrar sesión") }}
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('home') }}" class="dropdown-item">
                            <i class="fas fa-home mr-2"></i> {{ __("Ver sitio") }}
                        </a>
                        @php
                        /*<div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>*/
                        @endphp
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
                <li class="nav-item">
                    @include('frontoffice.components.language_button', ['class' => "box"])
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{ route('dashboard') }}" class="brand-link">
                <img src="{{ asset('back/img/logo.png') }}" alt="VZAT" class="brand-image" style="opacity: .8">
                <span class="brand-text font-weight-light">VZAT</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{ asset('back/img/default_user.png') }}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                    </div>
                </div>
                <!-- Sidebar Menu -->
                @php
                    list($menuItem, $menuSubitem) = App\Helpers\MenuHelper::getActiveItems();
                @endphp

                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        @role('Administrador')
                        <li class="nav-item has-treeview @if($menuItem == "users") menu-open @else menu-close @endif">
                            <a href="#" class="nav-link @if($menuItem == "users") active @endif">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    {{ __("Usuarios") }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('admin::users::list') }}" class="nav-link @if($menuItem == "users" && $menuSubitem == "users") active @endif">
                                        <i class="fas fa-user nav-icon"></i>
                                        <p>{{ __("Gestionar") }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin::users_approval::list') }}" class="nav-link @if($menuItem == "users" && $menuSubitem == "approval") active @endif">
                                        <i class="fas fa-check nav-icon"></i>
                                        <p>{{ __("Aprobar") }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview @if($menuItem == "masters") menu-open @else menu-close @endif">
                            <a href="#" class="nav-link @if($menuItem == "masters") active @endif">
                                <i class="nav-icon fas fa-database"></i>
                                <p>
                                    {{ __("Parámetros") }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('admin::categories::list') }}" class="nav-link @if($menuItem == "masters" && $menuSubitem == "categories") active @endif">
                                        <i class="fas fa-sitemap nav-icon"></i>
                                        <p>{{ __("Categorías") }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin::states::list') }}" class="nav-link @if($menuItem == "masters" && $menuSubitem == "states") active @endif">
                                        <i class="fas fa-map-marked-alt nav-icon"></i>
                                        <p>{{ __("Estados") }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin::languages::list') }}" class="nav-link @if($menuItem == "masters" && $menuSubitem == "languages") active @endif">
                                        <i class="fas fa-language nav-icon"></i>
                                        <p>{{ __("Idiomas") }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin::countries::list') }}" class="nav-link @if($menuItem == "masters" && $menuSubitem == "countries") active @endif">
                                        <i class="fas fa-globe-americas nav-icon"></i>
                                        <p>{{ __("Países") }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin::sectors::list') }}" class="nav-link @if($menuItem == "masters" && $menuSubitem == "sectors") active @endif">
                                        <i class="fas fa-layer-group nav-icon"></i>
                                        <p>{{ __("Sectores") }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('admin::documents::list') }}" class="nav-link @if($menuItem == "documents") active @endif">
                                <i class="far fa-file-pdf nav-icon"></i>
                                <p>{{ __("Documentos") }}</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('admin::contact::list') }}" class="nav-link @if($menuItem == "contact") active @endif">
                                <i class="fas fa-headset nav-icon"></i>
                                <p>{{ __("Contacto") }}</p>
                            </a>
                        </li>

                        <li class="nav-item has-treeview @if($menuItem == "blog") menu-open @else menu-close @endif">
                            <a href="#" class="nav-link @if($menuItem == "blog") active @endif">
                                <i class="nav-icon fas fa-blog"></i>
                                <p>
                                    {{ __("Blog") }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('admin::blog_categories::list') }}" class="nav-link @if($menuItem == "blog" && $menuSubitem == "categories") active @endif">
                                        <i class="fas fa-sitemap nav-icon"></i>
                                        <p>{{ __("Categorías") }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin::blog_posts::list') }}" class="nav-link @if($menuItem == "blog" && $menuSubitem == "") active @endif">
                                        <i class="nav-icon far fa-newspaper"></i>
                                        <p>{{ __("Posts") }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview @if($menuItem == "reports") menu-open @else menu-close @endif">
                            <a href="#" class="nav-link @if($menuItem == "reports") active @endif">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>
                                    {{ __("Reportes") }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('report::general') }}" class="nav-link @if($menuItem == "reports" && $menuSubitem == "general") active @endif">
                                        <i class="nav-icon fas fa-book-open"></i>
                                        <p>{{ __("Generales") }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('report::summary_reports') }}" class="nav-link @if($menuItem == "reports" && $menuSubitem == "summary_reports") active @endif">
                                        <i class="nav-icon fas fa-book-open"></i>
                                        <p>{{ __("Reportes de resúmenes") }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('report::page::documents_trends') }}" class="nav-link @if($menuItem == "reports" && $menuSubitem == "documents_trends") active @endif">
                                        <i class="nav-icon far fa-newspaper"></i>
                                        <p>{{ __("Tendencias docs.") }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('report::page::user_activity') }}" class="nav-link @if($menuItem == "reports" && $menuSubitem == "user_activity") active @endif">
                                        <i class="nav-icon far fa-newspaper"></i>
                                        <p>{{ __("Actividad de usuario") }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endrole

                        @php
                        /*<li class="nav-item">
                            <a href="{{ route('admin::user::list') }}" class="nav-link @if($menuItem == "users") active @endif">
                                <i class="fas fa-user-friends nav-icon"></i>
                                <p>Usuarios</p>
                            </a>
                        </li>*/
                        @endphp

                        <!--li class="nav-header">EXAMPLES</li-->
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <div class="content-wrapper">
            @yield('content_header')

            @if(session('message_error'))
            @component('backoffice.components.alert')
                @slot('class')
                danger
                @endslot

                @slot('message')
                {{ session('message_error') }}
                @endslot
            @endcomponent
            @endif

            @if(session('message_success'))
            @component('backoffice.components.alert')
                @slot('class')
                success
                @endslot

                @slot('message')
                {{ session('message_success') }}
                @endslot
            @endcomponent
            @endif

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    @yield('content')
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2020 <a href="http://vzat">vzat</a></strong>
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>

    <!-- jQuery -->
    <script src="{{ asset('back/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('back/plugins/jquery/index.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('back/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('back/js/adminlte.js') }}"></script>

    <script>
        ((w, $) => {
            w.frontier_admin = {
                base_url: "{{ url('/') }}"
            };
        })(window, jQuery);
    </script>
    @section('js')
    @show
</body>
</html>
