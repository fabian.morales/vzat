<div class="row">
    @php
        $idMassAction = rand();
    @endphp
    <div class="col-md-4">
        <div class="input-group input-group-sm">
            <select class="form-control" rel="mass_actions" id="select_ma_{{ $idMassAction }}">
                @foreach($options as $key => $option)
                <option value="{{ $key }}">{{ $option }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <input type="submit" rel="mass_actions_button" data-select="select_ma_{{ $idMassAction }}" class="btn btn-info btn-flat btn-sm" value="{{ __("Aplicar") }}" />
    </div>
</div>