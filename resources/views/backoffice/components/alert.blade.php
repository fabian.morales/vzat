<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-12">
            <div class="alert alert-{{ $class }} alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</div>
