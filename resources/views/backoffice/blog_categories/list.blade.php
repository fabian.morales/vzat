@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        {{ __("Categorías de blog") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Categorías de blog") }}
    @endslot
@endcomponent
@stop

@section('js')
<script>
(($) => {
    $(document).ready(() => {
        $("a[rel='delete']").click((e) => {
            e.preventDefault();
            if (confirm('{{ __("¿Está seguro de borrar este elemento?") }}')) {
                window.location.href = $(e.currentTarget).attr('href');
            }
        });
    });
})(jQuery);
</script>
@endsection

@section('content')
<!-- Page Heading -->
<p class="mb-4">
    <a href="{{ route('admin::blog_categories::create') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-plus-square"></i> {{ __("Crear categoría") }}</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Categorías registradas") }}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ __("Nombre") }}</th>
                        <th class="no-sort">{{ __("Acciones") }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories ?? '' as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name() }}</td>
                        <td>
                            <a href="{{ route('admin::blog_categories::edit', ['id' => $category->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Editar categoría") }}">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>
                            <a href="{{ route('admin::blog_categories::delete', ['id' => $category->id]) }}" rel="delete" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Borrar categoría") }}">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
