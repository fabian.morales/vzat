@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        @if($edit)
        <h1>{{ __("Editar categoría") }}</h1>
        @else
        <h1>{{ __("Crear categoría") }}</h1>
        @endif
    @endslot

    @slot('breadcrumb')
        {{ __("Categorías") }}
    @endslot
@endcomponent
@stop

@section('content')

<form method="post" action="{{ $edit ? route('admin::blog_categories::update', ['id' => $category->id]) : route('admin::blog_categories::store') }}" accept-charset="utf-8" class="form-row">
    <input type="hidden" id="id" name="id" value="{{ $category->id }}" />
    @csrf

    <div class="form-group col-md-6">
        <label>{{ __("Nombre") }} (Espa&ntilde;ol)</label>
        <input type="text" name="name_es" id="name_es" class="form-control @error('name_es') is-invalid @enderror" value="{{ old('name_es') ? old('name_es') : $category->name_es }}" required="required" />
        @error('name_es')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-6">&nbsp;</div>

    <div class="form-group col-md-6">
        <label>{{ __("Nombre") }} (English)</label>
        <input type="text" name="name_en" id="name_en" class="form-control @error('name_en') is-invalid @enderror" value="{{ old('name_en') ? old('name_en') : $category->name_en }}" required="required" />
        @error('name_en')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-6">&nbsp;</div>

    <div class="col-md-6">
        <a href="{{ route('admin::blog_categories::list') }}" class="btn btn-danger btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">{{ __("Regresar") }}</a>
        </a>
        <div class="float-left">&nbsp;</div>
        <button type="submit" class="btn btn-primary btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-save"></i>
            </span>
            <span class="text">{{ __("Guardar") }}</a>
        </button>
    </div>
</form>
@stop