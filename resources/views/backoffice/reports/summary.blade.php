@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        {{ __("Reportes de resumen") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Reportes de resumen") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('js')
<!-- Select2 -->
<script src="{{ asset('back/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('back/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
($ => {
    @if(!empty($graphs))
    $(() => {
        let charts = [];
        const graphsData = JSON.parse('{!! $graphs->toJson() !!}');
        
        let getRandomColor = () => {
            const r = Math.floor(Math.random() * 2) * 100 + Math.floor(Math.random() * 9) * 10 + Math.floor(Math.random() * 9);
            const g = Math.floor(Math.random() * 2) * 100 + Math.floor(Math.random() * 9) * 10 + Math.floor(Math.random() * 9);
            const b = Math.floor(Math.random() * 2) * 100 + Math.floor(Math.random() * 9) * 10 + Math.floor(Math.random() * 9);

            return "rgba(" + r + ", " + g + ", " + b + ", :xx:)";
        }

        const makeGraphics = () => {
            if (charts !== null && charts.length > 0) {
                charts.forEach(o => {
                    if (o !== null) {
                        o.destroy();
                    }
                });

                charts = [];
            }

            let container = document.getElementById('canvas_container');
            container.textContent = '';

            const cols = Math.ceil(12 / graphsData.length);
            if (cols < 1 || cols > 12) {
                cols = 4;
            }

            graphsData.forEach((o, i) => {
                if (o.data !== null && typeof o.data !== 'undefined' && o.data.length > 0) {
                    let colors = [];
                    for (let i = 0; i <= o.data.length; i++) {
                        colors.push(getRandomColor());
                    }

                    let gi = {
                        data: o.data,
                        borderColor: [],
                        backgroundColor: [],
                        borderWidth: 2,
                        label: o.title,
                    };

                    if ($("#graph_type").val() == "pie") {
                        gi.backgroundColor = colors.map(item => {
                            return item.replace(':xx:', 0.3);   
                        });
                        gi.borderColor = colors.map(item => {
                            return item.replace(':xx:', 1);
                        });
                    }
                    else{
                        gi.borderColor = colors[0].replace(':xx:', 1);
                        gi.backgroundColor = colors[0].replace(':xx:', 0.3);
                    }

                    let graphContainer = document.createElement('div');
                    let canvas = document.createElement('canvas');
                    graphContainer.classList.add('mb-5');
                    graphContainer.classList.add('col-md-' + cols);
                    graphContainer.classList.add('p-2');
                    
                    let divTitle = document.createElement('div');
                    divTitle.classList.add('text-bold');
                    divTitle.classList.add('h5');
                    divTitle.classList.add('text-center');
                    divTitle.classList.add('mb-3');
                    divTitle.textContent = o.title;
                    graphContainer.appendChild(divTitle);
                    graphContainer.appendChild(canvas);

                    container.appendChild(graphContainer);

                    let context = canvas.getContext('2d');

                    let chart = new Chart(context, {
                        type: $("#graph_type").val(),
                        data: {
                            labels: o.labels,
                            datasets: [gi] 
                        },
                        options: {
                            scales: {
                                xAxes: [{
                                    ticks: {
                                        callback: function(value) {
                                            return value.substr(0, 25);
                                        },
                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            },
                            tooltips: {
                                enabled: true,
                                mode: 'label',
                                callbacks: {
                                    title: function(tooltipItems, data) {
                                        var idx = tooltipItems[0].index;
                                        return data.labels[idx];
                                    },
                                }
                            },
                        }
                    });

                    charts.push(chart);
                    canvas.style.backgroundColor = '#fff';
                }
            });
        }

        makeGraphics();

        $("#graph_type").on("change", e => {
            e.preventDefault();
            makeGraphics();
        })
    });
    @endif
})(jQuery);
</script>
@stop

@section('content')
<!-- Page Heading -->

<!-- DataTales Example -->
<form id="summary_form" method="post" action="{{ route('report::summary_reports::post') }}">
    @csrf
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ __("Selección de datos") }}</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3 form-group">
                    <label>{{ __("Seleccione el tipo de consulta") }}</label>
                    <select class="form-control" id="type" name="type">
                        <option value="user_login" @if($type == 'user_login') selected @endif>{{ __("Inicios de sesión") }}</option>
                        <option value="search" @if($type == 'search') selected @endif>{{ __("Búsquedas") }}</option>
                        <option value="document_download" @if($type == 'document_download') selected @endif>{{ __("Descarga de documentos") }}</option>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label>{{ __("Seleccione el tipo") }}</label>
                    <select class="form-control" id="category_id" name="category_id">
                        <option value="">{{ __("Ninguno") }}</option>
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}" @if($category->id == $categoryId) selected @endif>{{ $category->name() }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label>{{ __("Seleccione el estado") }}</label>
                    <select class="form-control" id="state_id" name="state_id">
                        <option value="">{{ __("Ninguno") }}</option>
                        @foreach ($states as $state)
                        <option value="{{ $state->id }}" @if($state->id == $stateId) selected @endif>{{ $state->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label>{{ __("Seleccione el sector") }}</label>
                    <select class="form-control" id="sector_id" name="sector_id">
                        <option value="">{{ __("Ninguno") }}</option>
                        @foreach ($sectors as $sector)
                        <option value="{{ $sector->id }}" @if($sector->id == $sectorId) selected @endif>{{ $sector->name() }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label>{{ __("Tipo de gráfico") }}</label>
                    <select class="form-control" id="graph_type" name="graph_type">
                        <option value="bar">{{ __("Barras") }}</option>
                        <option value="pie">{{ __("Torta") }}</option>
                        <option value="line">{{ __("Líneas") }}</option>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label>{{ __("Fecha de inicio") }}</label>
                    <input class="form-control" type="date" name="start_date" id="start_date" value="{{ $startDate ?: '2000-01-01' }}" />
                </div>
                <div class="col-md-3 form-group">
                    <label>{{ __("Fecha final") }}</label>
                    <input class="form-control" type="date" name="end_date" id="end_date" value="{{ date('Y-m-d', strtotime($endDate ?: '2099-12-31')) }}" />
                </div>
                <div class="col-md-3 form-group"></div>
                <div class="col-md-2 form-group">
                    <input type="submit" class="btn btn-primary form-control" value="{{ __("Mostrar") }}" />
                </div>
                <div class="col-md-2 form-group">
                    <input type="submit" class="btn btn-primary form-control" name="download_xlsx" value="{{ __("Descargar") }}" />
                </div>
            </div>
        </div>
    </div>
</form>

<div class="card shadow mb-4" id="grap-container">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Gráfico") }}</h6>
    </div>
    <div class="card-body">
        <div class="row" id="canvas_container"></div>
    </div>
</div>

@if(!empty($tables))

@foreach ($tables as $table)
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ $table['title'] }}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        @foreach ($table['head'] as $item)
                        <th>{{ $item }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($table['data'] as $row)
                    <tr>
                        @foreach(get_object_vars($row) as $field)
                            <td>{{ $field }}</td>
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endforeach

@endif
@stop
