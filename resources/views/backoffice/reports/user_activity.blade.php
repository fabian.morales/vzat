@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        {{ __("Actividad de usuario") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Actividad de usuario") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('back/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('back/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('js')
<!-- Select2 -->
<script src="{{ asset('back/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('back/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="{{ asset('back/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
(($) => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.select2').select2({
        theme: 'bootstrap4'
    });

    $(document).ready(() => {
        $("#filter").click(e => {
            e.preventDefault();
            
            $('#dataTable').DataTable().clear().destroy();

            let $dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("report::user_activity") }}?datatable=1',
                    type: 'post',
                    data: {
                        start_date: $("#start_date").val(),
                        end_date: $("#end_date").val(),
                        user_id: $("#user").val(),
                    },
                    complete: () => {
                        //createGraph();
                        //console.log($dataTable.rows().data());
                        //hookDataTable();
                    }
                },
                @if(App\Helpers\LocaleHelper::getLocale() != "en")
                language: {
                    url: '{{ asset("back/plugins/datatables/" . App\Helpers\LocaleHelper::getLocale() . ".json") }}'
                },
                @endif
                columns: [
                    { data: 'created_at', name: 'created_at' },
                    { data: 'ip_address', name: 'ip_address' },
                    { data: 'action', name: 'acition' },
                    { data: 'info', name: 'info' },
                ],
                /*initComplete: (settings, json) => {
                    let api = $('#dataTable').dataTable().api();
                    let $filter = $('#dataTable_filter input');
                    $('#dataTable_filter input').unbind();
                    $('#dataTable_filter input').bind('keyup', (e) => {
                        if(e.keyCode == 13) {
                            api.search($filter.val()).draw();
                        }
                    });
                }*/
            });
        });

        $("#btnDescargar").click((e) => {
            e.preventDefault();
            
            $("#report_name").val('user_activity');
            $("#start_date_download").val($("#start_date").val());
            $("#end_date_download").val($("#end_date").val());
            $("#user_download").val($("#user").val());
            $("#form_download").submit();
        });
    });
})(jQuery);
</script>
@stop

@section('content')
<!-- Page Heading -->

<form id="form_download" method="post" action="{{ route('report::download') }}">
    @csrf
    <input type="hidden" id="report_name" name="report" />
    <input type="hidden" name="start_date" id="start_date_download" value="" />
    <input type="hidden" name="end_date" id="end_date_download" value="" />
    <input type="hidden" name="user_id" id="user_download" value="" />
</form>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Selección de datos") }}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-5 form-group">
                <label>{{ __("Seleccione el usuario") }}</label>
                <select class="form-control select2" id="user" name="user">
                    @foreach ($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 form-group">
                <label>{{ __("Fecha de inicio") }}</label>
                <input class="form-control" type="date" name="start_date" id="start_date" value="" />
            </div>
            <div class="col-md-3 form-group">
                <label>{{ __("Fecha final") }}</label>
                <input class="form-control" type="date" name="end_date" id="end_date" value="" />
            </div>
            <div class="col-md-2 form-group">
                <button id="filter" class="btn btn-primary form-control">{{ __("Mostrar") }}</button>
            </div>
            <div class="col-md-2 form-group">
                <button class="btn-primary btn form-control" id="btnDescargar">{{ __("Descargar") }}</button>
            </div>
        </div>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Actividad realizada") }}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>{{ __("Fecha") }}</th>
                        <th>{{ __("Dirección IP") }}</th>
                        <th>{{ __("Acción") }}</th>
                        <th>{{ __("Información adicional") }}</th>
                    </tr>
                </thead>
                <tbody>
                   
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
