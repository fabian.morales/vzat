@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        {{ __("Reportes generales") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Reportes generales") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('js')
<!-- Select2 -->
<script src="{{ asset('back/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('back/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
(($) => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(() => {
        let reports = { @foreach($reports as $index => $report){{ $index }}: '{{ route("report::" . $index) }}', @endforeach };
        let columns = { @foreach($columnsReport as $index => $report){{ $index }}: [ @foreach($report as $key => $column) { data: '{{ $key }}', name: '{{ $key }}' }, @endforeach ] , @endforeach };
        let labels = { @foreach($columnsReport as $index => $report){{ $index }}: [ @foreach($report as $key => $column) '{{ $column }}', @endforeach ] , @endforeach };
        let datasets = { @foreach($datasets as $index => $report){{ $index }}: [ @foreach($report as $ds) { @foreach($ds as $key => $column) {{ $key }} : '{{ $column }}', @endforeach }, @endforeach ] , @endforeach };
        let chart = null;

        let getRandomColor = () => {
            /*let letters = '0123456789ABCDEF';
            let color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;*/

            let r = Math.floor(Math.random() * 2) * 100 + Math.floor(Math.random() * 9) * 10 + Math.floor(Math.random() * 9);
            let g = Math.floor(Math.random() * 2) * 100 + Math.floor(Math.random() * 9) * 10 + Math.floor(Math.random() * 9);
            let b = Math.floor(Math.random() * 2) * 100 + Math.floor(Math.random() * 9) * 10 + Math.floor(Math.random() * 9);

            return "rgba(" + r + ", " + g + ", " + b + ", :xx:)";
        }

        $("#filter").click(e => {
            e.preventDefault();
            let url = reports[$("#report").val()];
            let cols = columns[$("#report").val()];
            let lbl = labels[$("#report").val()];
            let ds = datasets[$("#report").val()];

            $('#dataTable').DataTable().clear().destroy();

            $('#dataTable thead').html('');
            let $tr = $("<tr></tr>");
            $.each(lbl, (i, o) => {
                let $th = $("<th>" + o + "</th>");
                $tr.append($th);
            });

            $('#dataTable thead').append($tr);

            let $dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: url + '?datatable=1',
                    type: 'post',
                    data: {
                        start_date: $("#start_date").val(),
                        end_date: $("#end_date").val(),
                    },
                    complete: () => {
                        //createGraph();
                        //console.log($dataTable.rows().data());
                        //hookDataTable();
                    }
                },
                @if(App\Helpers\LocaleHelper::getLocale() != "en")
                language: {
                    url: '{{ asset("back/plugins/datatables/" . App\Helpers\LocaleHelper::getLocale() . ".json") }}'
                },
                @endif
                columns: cols,
                /*initComplete: (settings, json) => {
                    let api = $('#dataTable').dataTable().api();
                    let $filter = $('#dataTable_filter input');
                    $('#dataTable_filter input').unbind();
                    $('#dataTable_filter input').bind('keyup', (e) => {
                        if(e.keyCode == 13) {
                            api.search($filter.val()).draw();
                        }
                    });
                }*/
            });

            if (ds.length === 0) {
                if (chart !== null) {
                    chart.destroy();
                }

                $("#grap-container").hide();

                return;
            }

            $("#grap-container").show();

            $.ajax({
                url: url,
                type: 'post',
                data: {
                    start_date: $("#start_date").val(),
                    end_date: $("#end_date").val(),
                },
                dataType: 'json',
                cache: 'false',
            }).done(res => {
                let graphDs = [];
                let graphLbl = [];
                let colors = [];
                for (let i = 0; i <= Math.min(ds.length, res.length); i++) {
                    colors.push(getRandomColor());
                }

                $.each(ds, (i, o) => {
                    let gi = {
                        data: [],
                        borderColor: [],
                        backgroundColor: [],
                        borderWidth: 2,
                        label: o.label,
                    };

                    gi.data = res.map(item => {
                        return item[o.value];
                    });

                    if ($("#type").val() == "pie") {
                        gi.backgroundColor = colors.map(item => {
                            return item.replace(':xx:', 0.3);   
                        });
                        gi.borderColor = colors.map(item => {
                            return item.replace(':xx:', 1);
                        });
                    }
                    else{
                        gi.borderColor = colors[i].replace(':xx:', 1);
                        gi.backgroundColor = colors[i].replace(':xx:', 0.3);
                    }

                    graphDs.push(gi);

                    graphLbl = res.map(item => {
                        return item[o.field];
                    });
                });

                let canvas = document.getElementById('canvas_graph');
                let context = canvas.getContext('2d');

                if (chart !== null) {
                    chart.destroy();
                }

                chart = new Chart(context, {
                    type: $("#type").val(),
                    data: {
                        labels: graphLbl,
                        datasets: graphDs 
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                ticks: {
                                    callback: function(value) {
                                        return value.substr(0, 25);
                                    },
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        },
                        tooltips: {
                            enabled: true,
                            mode: 'label',
                            callbacks: {
                                title: function(tooltipItems, data) {
                                    var idx = tooltipItems[0].index;
                                    return data.labels[idx];
                                },
                            }
                        },
                    }
                });

                canvas.style.backgroundColor = '#fff';
            })
        });

        $("#btnDownload").click((e) => {
            e.preventDefault();
            
            $("#report_name").val($("#report").val());
            $("#start_date_download").val($("#start_date").val());
            $("#end_date_download").val($("#end_date").val());
            $("#form_download").submit();
        });
    });
})(jQuery);
</script>
@stop

@section('content')
<!-- Page Heading -->

<form id="form_download" method="post" action="{{ route('report::download') }}">
    @csrf
    <input type="hidden" id="report_name" name="report" />
    <input type="hidden" name="start_date" id="start_date_download" value="" />
    <input type="hidden" name="end_date" id="end_date_download" value="" />
</form>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Selección de datos") }}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-3 form-group">
                <label>{{ __("Seleccione el reporte") }}</label>
                <select class="form-control" id="report" name="report">
                    @foreach ($reports as $index => $report)
                    <option value="{{ $index }}">{{ $report }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 form-group">
                <label>{{ __("Tipo de gráfico") }}</label>
                <select class="form-control" id="type" name="type">
                    <option value="bar">{{ __("Barras") }}</option>
                    <option value="pie">{{ __("Torta") }}</option>
                    <option value="line">{{ __("Líneas") }}</option>
                </select>
            </div>
            <div class="col-md-3 form-group">
                <label>{{ __("Fecha de inicio") }}</label>
                <input class="form-control" type="date" name="start_date" id="start_date" value="" />
            </div>
            <div class="col-md-3 form-group">
                <label>{{ __("Fecha final") }}</label>
                <input class="form-control" type="date" name="end_date" id="end_date" value="" />
            </div>
            <div class="col-md-2 form-group">
                <button id="filter" class="btn btn-primary form-control">{{ __("Mostrar") }}</button>
            </div>
            <div class="col-md-2 form-group">
                <button class="btn-primary btn form-control" id="btnDownload">{{ __("Descargar") }}</button>
            </div>
        </div>
    </div>
</div>

<div class="card shadow mb-4" id="grap-container">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Gráfico") }}</h6>
    </div>
    <div class="card-body">
        <canvas id="canvas_graph"></canvas>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Tabla de datos") }}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ __("Nombre") }}</th>
                        <th>{{ __("Cantidad") }}</th>
                    </tr>
                </thead>
                <tbody>
                   
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
