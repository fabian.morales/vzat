@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        {{ __("Sectores") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Sectores") }}
    @endslot
@endcomponent
@stop

@section('content')
<!-- Page Heading -->
<p class="mb-4">
    <a href="{{ route('admin::sectors::create') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-plus-square"></i> {{ __("Crear sector") }}</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Sectores registrados") }}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ __("Nombre") }}</th>
                        <th class="no-sort">{{ __("Acciones") }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sectors ?? '' as $sector)
                    <tr>
                        <td>{{ $sector->id }}</td>
                        <td>{{ $sector->name() }}</td>
                        <td>
                            <a href="{{ route('admin::sectors::edit', ['id' => $sector->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Editar sector") }}">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
