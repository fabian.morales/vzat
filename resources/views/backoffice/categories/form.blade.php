@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        @if($edit)
        <h1>{{ __("Editar categoría") }}</h1>
        @else
        <h1>{{ __("Crear categoría") }}</h1>
        @endif
    @endslot

    @slot('breadcrumb')
        {{ _("Categorías") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/farbtastic/farbtastic.css') }}">
@stop

@section('js')
<script src="{{ asset('back/plugins/farbtastic/farbtastic.js') }}"></script>
<script>
(($) => {
    $(document).ready(() => {
        $('#colorpicker').farbtastic('#color');
    });
})(jQuery);
</script>
@stop

@section('content')

<form method="post" action="{{ $edit ? route('admin::categories::update', ['id' => $category->id]) : route('admin::categories::store') }}" class="form-row">
    <input type="hidden" id="id" name="id" value="{{ $category->id }}" />
    @csrf

    <div class="form-group col-md-6">
        <label>{{ __("Nombre") }} (Espa&ntilde;ol)</label>
        <input type="text" name="name_es" id="name_es" class="form-control @error('name_es') is-invalid @enderror" value="{{ old('name_es') ? old('name_es') : $category->name_es }}" required="required" />
        @error('name_es')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-6">&nbsp;</div>

    <div class="form-group col-md-6">
        <label>{{ __("Nombre") }} (English)</label>
        <input type="text" name="name_en" id="name_en" class="form-control @error('name_en') is-invalid @enderror" value="{{ old('name_en') ? old('name_en') : $category->name_en }}" required="required" />
        @error('name_es')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-6">&nbsp;</div>

    <div class="form-group col-md-6">
        <label>{{ __("Color") }}</label>
        <input type="text" name="color" id="color" class="form-control @error('color') is-invalid @enderror" value="{{ old('color') ? old('color') : ($category->color ?? '#000') }}" required="required" />
        @error('color')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-6">
        <div id="colorpicker"></div>
    </div>

    <div class="form-group col-md-6">
        <label>{{ __("Descripción") }} (Espa&ntilde;ol)</label>
        <input type="text" name="description_es" id="description_es" class="form-control @error('description_es') is-invalid @enderror" value="{{ old('description_es') ? old('description_es') : $category->description_es }}" />
        @error('description_es')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-6">&nbsp;</div>

    <div class="form-group col-md-6">
        <label>{{ __("Descripción") }} (English)</label>
        <input type="text" name="description_en" id="description_en" class="form-control @error('description_en') is-invalid @enderror" value="{{ old('description_en') ? old('description_en') : $category->description_en }}" />
        @error('description_en')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-6">&nbsp;</div>

    <div class="col-md-6">
        <a href="{{ route('admin::categories::list') }}" class="btn btn-danger btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">{{ __("Regresar") }}</a>
        </a>
        <div class="float-left">&nbsp;</div>
        <button type="submit" class="btn btn-primary btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-save"></i>
            </span>
            <span class="text">{{ __("Guardar") }}</a>
        </button>
    </div>
</form>
@stop