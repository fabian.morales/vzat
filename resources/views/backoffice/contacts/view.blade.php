@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
       {{ __("Detalle de solicitud de contacto") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Solicitudes de contacto") }}
    @endslot
@endcomponent
@stop

@section('css')
@stop

@section('js')
@stop

@section('content')
<!-- Page Heading -->

<!-- DataTables -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Detalle de solicitud de contacto") }}</h6>
    </div>
    <div class="card-body">
        <br />
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <tbody>
                    <tr>
                        <td><strong>{{ __("Nombre") }}: </strong></td>
                        <td>{{ $contact->name }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Correo") }}: </strong></td>
                        <td>{{ $contact->email }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Organización") }}: </strong></td>
                        <td>{{ $contact->entity }}</td>
                    </tr>
                    @if(!empty($contact->country))
                    <tr>
                        <td><strong>{{ __("País") }}: </strong></td>
                        <td>{{ $contact->country->name() }}</td>
                    </tr>
                    @endif
                    <tr>
                        <td><strong>{{ __("Fecha") }}: </strong></td>
                        <td>{{ $contact->created_at }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Mensaje") }}: </strong></td>
                        <td>{{ $contact->message }}</td>
                    </tr>
                </tbody>
            </table>
            <br />
            <a href="{{ route('admin::contact::list') }}" class="btn btn-danger btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">{{ __("Regresar") }}</a>
            </a>
        </div>
    </div>
</div>
@stop
