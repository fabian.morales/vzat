@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
       {{ __("Solicitudes de contacto") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Solicitudes de contacto") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('js')
    <script src="{{ asset('/back/plugins/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    (($, $mo) => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let $dataTable = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("admin::contact::data") }}',
                type: 'post',
                complete: () => {
                    //hookDataTable();
                }
            },
            @if(App\Helpers\LocaleHelper::getLocale() != "en")
            language: {
                url: '{{ asset("back/plugins/datatables/" . App\Helpers\LocaleHelper::getLocale() . ".json") }}'
            },
            @endif
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'created_at', name: 'created_at', 
                    render: (data) => {
                        return $mo(String(data), "YYYY-MM-DD hh:mm")
                            .locale('{{ App\Helpers\LocaleHelper::getLocale() }}')
                            .format('MM/DD/YYYY hh:mm');
                    }
                },
                { actions: 'actions', name: 'actions', searchable: false, orderable: false,
                    render: (data, type, row) => {
                        let url = '{{ route("admin::contact::view", ["id" => ":xx:"]) }}/';
                        return `<a href="` + url.replace(":xx:", row.id) + `" rel="view" data-action="view" data-id="` + row.id + `" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Ver") }}">
                            <i class="far fa-eye"></i>
                        </a>`;
                    } 
                }
            ],
            initComplete: (settings, json) => {
                let api = $('#dataTable').dataTable().api();
                let $filter = $('#dataTable_filter input');
                $('#dataTable_filter input').unbind();
                $('#dataTable_filter input').bind('keyup', (e) => {
                    if(e.keyCode == 13) {
                        api.search($filter.val()).draw();
                    }
                });

                //hookDataTable();
            }
        });
    })(jQuery, moment);
    </script>
@stop

@section('content')
<!-- Page Heading -->

<!-- DataTables -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Solicitudes de contactos") }}</h6>
    </div>
    <div class="card-body">
        <br />
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>    
                        <th>ID</th>
                        <th>{{ __("Nombre") }}</th>
                        <th>{{ __("Correo") }}</th>
                        <th>{{ __("Fecha") }}</th>
                        <th class="no-sort">{{ __("Acciones") }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@stop
