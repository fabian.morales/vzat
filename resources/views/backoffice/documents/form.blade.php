@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        @if($edit)
        <h1>{{ __("Editar documento") }}</h1>
        @else
        <h1>{{ __("Crear documento") }}</h1>
        @endif
    @endslot

    @slot('breadcrumb')
        {{ __("Documentos") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/tag-editor/jquery.tag-editor.css') }}">
@stop

@section('js')
<script src="{{ asset('back/plugins/tag-editor/jquery.tag-editor.min.js') }}"></script>
<script src="{{ asset('back/plugins/tag-editor/jquery.caret.min.js') }}"></script>
<script>
($ => {
    $(document).ready(() => {
        $("#keywords").tagEditor();
    });
})(jQuery);
</script>
@stop

@section('content')
<form method="post" action="{{ $edit ? route('admin::documents::update', ['id' => $document->id]) : route('admin::documents::store') }}" class="form-row" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $document->id }}" />
    @csrf

    <div class="container">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ __("Información básica") }}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>{{ __("Título") }}</label>
                        <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ? old('title') : $document->title }}" required="required" />
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-md-6">
                        <div class="form-group">
                            <label>{{ __("Idioma") }}</label>
                            <select class="form-control select2 @error('language_id') is-invalid @enderror" name="language_id" id="language_id">
                                <option value="">{{ __("Seleccione un idioma") }}</option>
                                @foreach($languages as $language)
                                <option value="{{ $language->id }}" @if($language->id == $document->language_id) selected @endif>{{ $language->name }}</option>
                                @endforeach
                            </select>
                            @error('language_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    {{-- <div class="form-group col-md-12">
                        <div class="form-group">
                            <label>{{ __("Descripción") }}</label>
                            <textarea name="description" id="description" rows="5" class="form-control @error('description') is-invalid @enderror" required>{{ old('description') ?? $document->description }}</textarea>
                            
                            @error('language_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div> --}}

                    <div class="form-group col-md-12">
                        <div class="form-group">
                            <label>{{ __("Texto para búsqueda") }}</label>
                            <textarea name="summary" id="summary" rows="5" class="form-control @error('summary') is-invalid @enderror">{{ old('summary') ?? $document->summary }}</textarea>
                            
                            @error('summary')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="form-group">
                            <label>{{ __("Categoría") }}</label>
                            <select class="form-control select2 @error('category_id') is-invalid @enderror" name="category_id" id="category_id">
                                <option value="">{{ __("Seleccione una categoría") }}</option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if($category->id == $document->category_id) selected @endif>{{ $category->name() }}</option>
                                @endforeach
                            </select>
                            @error('category_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label>{{ __("Fecha de publicación") }}</label>
                        <input type="date" name="publication_date" id="publication_date" class="form-control @error('publication_date') is-invalid @enderror" value="{{ old('publication_date') ? old('publication_date') : $document->publication_date }}" required="required" />
                        @error('publication_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-md-6">
                        <label>{{ __("Archivo") }}</label>
                        <input type="file" name="document" id="document" class="form-control @error('document') is-invalid @enderror" />
                        @error('document')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group col-md-6">
                        <label>{{ __("Imagen de portada") }}</label>
                        <input type="file" name="image" id="document" class="form-control @error('image') is-invalid @enderror" />
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <br />
                        <img src="{{ asset($document->getImageUrl('thumbnail')) }}" alt="" class="img-fluid" />
                    </div>

                    <div class="form-group col-md-12">
                        <label>{{ __("Palabras clave") }}, {{ __("separadas por coma") }}</label>
                        @php
                            $keywords = sizeof($document->keywords) ? implode(",", $document->keywords->pluck("value")->all()) : "";
                        @endphp
                        <input type="text" name="keywords" id="keywords" class="form-control" value="{{ $keywords }}" />
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ __("Sectores") }}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <p>{{ __("Seleccione los sectores del documento") }}</p>
                        <br />
                    </div>
                    @foreach($sectors as $sector)
                    <div class="form-group col-md-4 com-sm-6">
                        <label>
                            <input type="checkbox" name="sectors[]" id="sector_{{ $sector->id }}" value="{{ $sector->id }}" @if(sizeof($sector->documents) && ($sector->documents[0]->id == $document->id)) checked @endif />
                            {{ $sector->name() }}
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ __("Estados") }}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <p>{{ __("Seleccione los estados del documento") }}</p>
                        <br />
                    </div>
                    @foreach($states as $state)
                    <div class="form-group col-md-4 com-sm-6">
                        <label>
                            <input type="checkbox" name="states[]" id="state_{{ $state->id }}" value="{{ $state->id }}" @if(sizeof($state->documents) && ($state->documents[0]->id == $document->id)) checked @endif />
                            {{ $state->name }}
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <a href="{{ route('admin::documents::list') }}" class="btn btn-danger btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">{{ __("Regresar") }}</a>
        </a>
        <div class="float-left">&nbsp;</div>
        <button type="submit" class="btn btn-primary btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-save"></i>
            </span>
            <span class="text">{{ __("Guardar") }}</a>
        </button>
    </div>
</form>
@stop

{{-- @section('js')
<script src="{{ asset('/back/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('/back/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script>
(function(w, $){
    $(document).ready(function() {
        tinymce.init({
            selector: "textarea#description",
            //content_css : 'css/editor.css',
            height: 400,
            theme: 'silver',
            language: '{{ App\Helpers\LocaleHelper::getLocale() }}',
            relative_urls: false,
            document_base_url: w.frontier_admin.base_url,
            remove_script_host: false,
            extended_valid_elements: 'span,iframe[src|frameborder|style|scrolling|class|width|height|name|align]',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor ",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste textcolor"
            ],
            toolbar: "undo redo | styleselect | forecolor backcolor | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | visualblocks | table",
            image_advtab: true
        });
    });
})(window, jQuery);
</script>
@stop --}}

