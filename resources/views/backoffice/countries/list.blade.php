@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        {{ __("Países") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Países") }}
    @endslot
@endcomponent
@stop

@section('content')
<!-- Page Heading -->
<p class="mb-4">
    <a href="{{ route('admin::countries::create') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-plus-square"></i> {{ __("Crear país") }}</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Países registrados") }}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ __("Nombre") }}</th>
                        <th class="no-sort">{{ __("Acciones") }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($countries ?? '' as $country)
                    <tr>
                        <td>{{ $country->id }}</td>
                        <td>{{ $country->name() }}</td>
                        <td>
                            <a href="{{ route('admin::countries::edit', ['id' => $country->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Editar país") }}">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
