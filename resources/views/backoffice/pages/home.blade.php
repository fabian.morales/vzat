@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
@slot('title')
{{ __("Panel de control") }}
@endslot

@slot('breadcrumb')
{{ __("Panel de control") }}
@endslot
@endcomponent
@stop

@section('content')
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{ App\User::count() }}</h3>

                <p>{{ __("Usuarios registrados") }}</p>
            </div>
            <div class="icon">
                <i class="fas fa-users"></i>
            </div>
            <a href="{{ route("admin::users::list") }}" class="small-box-footer">{{ __("Ver más") }} <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{ App\User::where('approved', 0)->count() }}</h3>

                <p>{{ __("Solicitudes de registro") }}</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="{{ route("admin::users_approval::list") }}" class="small-box-footer">{{ __("Ver más") }} <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>{{ App\Document::count() }}</h3>

                <p>{{ __("Documentos") }}</p>
            </div>
            <div class="icon">
                <i class="fas fa-file-alt"></i>
            </div>
            <a href="{{ route("admin::documents::list") }}" class="small-box-footer">{{ __("Ver más") }} <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header border-0">
                <h3 class="card-title">{{ __("Documentos recientes") }}</h3>
                <div class="card-tools">
                    <!--a href="#" class="btn btn-tool btn-sm">
                        <i class="fas fa-bars"></i>
                    </a-->
                </div>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-striped table-valign-middle">
                    <thead>
                        <tr>
                            <th>{{ __("Título") }}</th>
                            <th>{{ __("Tipo") }}</th>
                            <th>{{ __("Fecha") }}</th>
                            <th>{{ __("Ver") }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($documents as $document)
                        <tr>
                            <td>
                                <span class="flag {{ $document->language->abbreviation }}"></span>
                                {{ $document->title }}
                            </td>
                            <td>{{ $document->category->name() }}</td>
                            <td>{{ $document->created_at }}</td>
                            <td> 
                                <a href="{{ route('dashboard') }}" class="text-muted">
                                    <i class="far fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
