@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        {{ __("Usuarios") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Usuarios") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('js')
    <!-- Select2 -->
    <script src="{{ asset('back/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    (($) => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(() => {
            const approvedStatus = [
                '{{ __("Pendiente") }}',
                '{{ __("Aprobado") }}',
                '{{ __("Rechazado") }}',
            ];

            let hookDelete = () => {
                $("a[rel='delete']").unbind();
                $("a[rel='delete']").bind('click', (e) => {
                    e.preventDefault();
                    if (confirm('{{ __("¿Está seguro de borrar este elemento?") }}')) {
                        window.location.href = $(e.currentTarget).attr('href');
                    }
                });
            };

            let $dataTable = $('#dataTable')
                .on('order.dt', () => hookDelete())
                .on('search.dt', () => hookDelete())
                .on('page.dt', () => hookDelete())
                .DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: '{{ route("admin::users::data") }}',
                        type: 'post',
                        data: (data) => {
                            data.searchByApproved = $("#approval_status").val();
                        },
                        complete: () => {
                            //hookDataTable();
                        }
                    },
                    @if(App\Helpers\LocaleHelper::getLocale() != "en")
                    language: {
                        url: '{{ asset("back/plugins/datatables/" . App\Helpers\LocaleHelper::getLocale() . ".json") }}'
                    },
                    @endif
                    columns: [
                        { data: 'id', name: 'id' },
                        { data: 'name', name: 'name' },
                        { data: 'email', name: 'email' },
                        { data: 'approved',name: 'approved', render: (data, type, row) => approvedStatus[row.approved]},
                        { actions: 'actions', name: 'actions', searchable: false, orderable: false,
                            render: (data, type, row) => {
                                let urlEdit = '{{ route("admin::users::edit", ["id" => ":xx:"]) }}'.replace(':xx:', row.id);
                                let urlDelete = '{{ route("admin::users::delete", ["id" => ":xx:"]) }}'.replace(':xx:', row.id);
                                return `<a href="${urlEdit}" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Editar usuario") }}">
                                    <i class="fas fa-fw fa-edit"></i>
                                </a>
                                <a href="${urlDelete}" rel="delete" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Borrar usuario") }}">
                                    <i class="fas fa-fw fa-trash"></i>
                                </a>`;
                            } 
                        }
                    ],
                    initComplete: (settings, json) => {
                        let api = $('#dataTable').dataTable().api();
                        let $filter = $('#dataTable_filter input');
                        $('#dataTable_filter input').unbind();
                        $('#dataTable_filter input').bind('keyup', (e) => {
                            if(e.keyCode == 13) {
                                api.search($filter.val()).draw();
                            }
                        });

                        hookDelete();
                    }
                });
            
            $("#approval_status").on('change', e=> {
                e.preventDefault();
                $dataTable.draw();
            });
        });
    })(jQuery);
    </script>
@stop

@section('content')
<!-- Page Heading -->

<!-- DataTables -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary float-left">{{ __("Usuarios registrados") }}</h6>
        <div class="text-right">
            <a href="{{ route('admin::users::download') }}" class="btn btn-primary inline-block"><i class="fas fa-download"></i> {{ __("Descargar") }}</a>
            <a href="{{ route('admin::users::create') }}" class="btn btn-primary inline-block"><i class="fas fa-fw fa-plus-square"></i> {{ __("Crear usuario") }}</a>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label>{{ __("Seleccione el estado de aprobación") }}</label>
                    <select id="approval_status" class="form-control">
                        <option value="">{{ __("Ninguno") }}</option>
                        <option value="0">{{ __("Pendiente") }}</option>
                        <option value="1">{{ __("Aprobado") }}</option>
                        <option value="2">{{ __("Rechazado") }}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ __("Nombre") }}</th>
                        <th>{{ __("Correo") }}</th>
                        <th>{{ __("Aprobado") }}</th>
                        <th class="no-sort">{{ __("Acciones") }}</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    /*@foreach($users ?? '' as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <a href="{{ route('admin::user::edit', ['id' => $user->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="Editar usuario">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach*/
                    @endphp
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
