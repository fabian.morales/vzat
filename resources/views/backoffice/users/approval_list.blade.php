@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
       {{ __("Usuarios por aprobar") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Usuarios por aprobar") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('js')
    <!-- Select2 -->
    <script src="{{ asset('back/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    (($) => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let $dataTable = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("admin::users_approval::data") }}',
                type: 'post',
                complete: () => {
                    hookDataTable();
                }
            },
            @if(App\Helpers\LocaleHelper::getLocale() != "en")
            language: {
                url: '{{ asset("back/plugins/datatables/" . App\Helpers\LocaleHelper::getLocale() . ".json") }}'
            },
            @endif
            columns: [
                { actions: 'checkbox', name: 'checkbox', searchable: false, orderable: false,
                    render: (data, type, row) => {
                        return `<input type="checkbox" name="users[` + row.id + `]" id="users_` + row.id + `" value="` + row.id + `" />`;
                    } 
                },
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { actions: 'actions', name: 'actions', searchable: false, orderable: false,
                    render: (data, type, row) => {
                        let url = '{{ route("admin::users_approval::save") }}/';
                        return `<a href="` + url + `" rel="approval" data-action="approve" data-id="` + row.id + `" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Aprobar") }}">
                            <i class="far fa-thumbs-up"></i>
                        </a>
                        <a href="` + url + `" rel="approval" data-action="disapprove" data-id="` + row.id + `" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Desaprobar") }}">
                            <i class="far fa-thumbs-down"></i>
                        </a>`;
                    } 
                }
            ],
            initComplete: (settings, json) => {
                let api = $('#dataTable').dataTable().api();
                let $filter = $('#dataTable_filter input');
                $('#dataTable_filter input').unbind();
                $('#dataTable_filter input').bind('keyup', (e) => {
                    if(e.keyCode == 13) {
                        api.search($filter.val()).draw();
                    }
                });

                hookDataTable();
            }
        });

        let sendData = ($data) => {
            $.ajax({
                url:  '{{ route("admin::users_approval::save") }}',
                type: 'post',
                dataType: 'json',
                cache: 'false',
                data: $data
            })
            .done((response) => {
                alert(response.message);
                $dataTable.ajax.reload();
            })
            .fail((jqXHR, status) => {
                alert(status);
            });
        };

        let hookDataTable = () => {
            $('a[rel="approval"]').unbind('click');
            $('a[rel="approval"]').click((e) => {
                e.preventDefault();

                if (!confirm('{{ __("¿Está seguro de realizar esta acción?") }}')) {
                    return;
                }

                let $data = [
                    {'name': 'users[]', 'value': parseInt($(e.currentTarget).attr('data-id')) },
                    {'name': 'action', 'value': $(e.currentTarget).attr('data-action') }
                ];

                if ($(e.currentTarget).attr('data-action') === 'disapprove') {
                    let reason = prompt('{{ __("Ingrese la razón del rechazo") }}', '{{ __("Razón del rechazo") }}');
                    if (reason !== '') {
                        $data.push({'name': 'reject_reason', 'value': reason });
                    }
                }

                sendData($data);
            });
        };

        $('input[rel="mass_actions_button"]').click((e) => {
            e.preventDefault();

            if (!confirm('{{ __("¿Está seguro de realizar esta acción?") }}')) {
                return;
            }

            let selectId = $(e.currentTarget).attr('data-select');
            let action = $('#' + selectId).val();
            let $data = $("#dataTable input[type='checkbox']:checked").serializeArray();

            if ($data.length === 0) {
                alert('{{ __("Debe seleccionar los usuarios por aprobar") }}');
                return;
            }

            $data.push({'name': 'action', 'value': action});
            sendData($data);
        });
    })(jQuery);
    </script>
@stop

@section('content')
<!-- Page Heading -->

<!-- DataTables -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Usuarios por aprobar") }}</h6>
    </div>
    <div class="card-body">
        @php
            $options = [
                "approve" => __("Aprobar seleccionados"),
                "disapprove" => __("Desaprobar seleccionados"),
            ];
        @endphp

        @component('backoffice.components.mass_actions', ['options' => $options])
        @endcomponent
        <br />
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="no-sort">&nbsp;</th>
                        <th>ID</th>
                        <th>{{ __("Nombre") }}</th>
                        <th>{{ __("Correo") }}</th>
                        <th class="no-sort">{{ __("Acciones") }}</th>
                    </tr>
                </thead>
            </table>
        </div>
        <br />
        @component('backoffice.components.mass_actions', ['options' => $options])
        @endcomponent
    </div>
</div>
@stop
