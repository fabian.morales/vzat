@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        @if($edit)
        <h1>{{ __("Editar usuario") }}</h1>
        @else
        <h1>{{ __("Crear usuario") }}</h1>
        @endif
    @endslot

    @slot('breadcrumb')
        {{ __("Usuarios") }}
    @endslot
@endcomponent
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('back/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('back/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@stop

@section('js')
    <!-- Select2 -->
    <script src="{{ asset('back/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
    (function($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.select2').select2({
            theme: 'bootstrap4'
        });
    })(jQuery);
    </script>
@stop

@section('content')

<form method="post" action="{{ $edit ? route('admin::users::update', ['id' => $user->id]) : route('admin::users::store') }}" class="form-row">
    <input type="hidden" id="id" name="id" value="{{ $user->id }}" />
    @csrf

    <div class="container">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ __("Datos básicos") }}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>{{ __("Nombre") }}</label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ? old('name') : $user->name }}" required="required" />
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>{{ __("Correo") }}</label>
                        <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') ? old('email') : $user->email }}" required="required" />
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>{{ __("Clave") }}</label>
                        <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') ? old('password') : '' }}" />
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">&nbsp;</div>
                    <div class="form-group col-md-12">
                        <label>{{ __("Razón del correo") }}</label>
                        <textarea name="email_reason" id="email_reason" class="form-control" rows="5">{{ $user->email_reason }}</textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ __("Otros datos") }}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>{{ __('Género') }}</label>
                        <select id="gender_id" class="form-control @error('gender_id') is-invalid @enderror" name="gender_id" required autocomplete="gender" autofocus>
                            <option value="">{{ __("Seleccionar género") }}</option>
                            @foreach($genders as $gender)
                            <option value="{{ $gender->id }}" @if($gender->id == $user->gender_id) selected @endif>{{ $gender->name() }}</option>
                            @endforeach
                        </select>
                        @error('gender_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <label>{{ __('Rango de edad') }}</label>
                        <select id="age_range_id" class="form-control @error('age_range_id') is-invalid @enderror" name="age_range_id" required autocomplete="age_range" autofocus>
                            <option value="">{{ __("Seleccionar rango de edad") }}</option>
                            @foreach($ageRanges as $range)
                            <option value="{{ $range->id }}" @if($range->id == $user->age_range_id) selected @endif>{{ $range->name() }}</option>
                            @endforeach
                        </select>
                        @error('age_range_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <div class="form-group">
                            <label>{{ __("País") }}</label>
                            <select class="form-control select2 @error('country_id') is-invalid @enderror" name="country_id" id="country_id">
                                <option value="">{{ __("Seleccione un país") }}</option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" @if($country->id == $user->country_id) selected @endif>{{ $country->name() }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('country_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>{{ __("Organización") }}</label>
                        <input type="text" name="entity" id="entity" class="form-control @error('entity') is-invalid @enderror" value="{{ old('entity') ? old('entity') : $user->entity }}" required="required" />
                        @error('entity')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>{{ __("Cargo") }}</label>
                        <input type="text" name="position" id="position" class="form-control @error('position') is-invalid @enderror" value="{{ old('position') ? old('position') : $user->position }}" required="required" />
                        @error('position')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <div class="form-group">
                            <label>{{ __("Rol") }}</label>
                            <select class="form-control select2 @error('role_id') is-invalid @enderror" name="role_id" id="role_id">
                                <option value="">{{ __("Seleccione un rol") }}</option>
                                @foreach($roles as $role)
                                <option value="{{ $role->id }}" @if($role->id == (old('role_id') ? old('role_id') : $user->role_id)) selected @endif>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('role_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-12">
                        <label>{{ __("Razón del rechazo") }}</label>
                        <textarea name="reject_reason" id="reject_reason" rows="5" class="form-control @error('reject_reason') is-invalid @enderror">{{ old('reject_reason') ? old('reject_reason') : $user->reject_reason }}</textarea>                        
                        @error('reject_reason')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label><input type="checkbox" name="approved" id="approved" value="1" @if((old('approved') ? old('approved') : $user->approved) == 1) checked @endif /> {{ __("Aprobado") }}</label>
                        <br />
                        <label><input type="checkbox" name="validated" id="validated" value="1" @if((old('validated') ? old('validated') : $user->validated) == 1) checked @endif /> {{ __("Validado") }}</label>
                    </div>
                    <div class="form-group col-md-6">
                        <label><input type="checkbox" name="accept_terms" id="accept_terms" value="Si" @if((old('accept_terms') ? old('accept_terms') : $user->accept_terms) == 'Si') checked @endif /> {{ __("Acepta términos y condiciones") }}</label>
                        <br />
                        <label><input type="checkbox" name="receive_emails" id="receive_emails" value="Si" @if((old('receive_emails') ? old('receive_emails') : $user->receive_emails) == 'Si') checked @endif /> {{ __("Acepta recibir correos de VZAT") }}</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('admin::users::list') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">{{ __("Regresar") }}</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">{{ __("Guardar") }}</a>
                </button>
            </div>
        </div>
    </div>
</form>
<br />
@stop
