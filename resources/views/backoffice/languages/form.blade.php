@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        @if($edit)
        <h1>{{ __("Editar idioma") }}</h1>
        @else
        <h1>{{ __("Crear idioma") }}</h1>
        @endif
    @endslot

    @slot('breadcrumb')
        {{ __("Idiomas") }}
    @endslot
@endcomponent
@stop

@section('content')

<form method="post" action="{{ $edit ? route('admin::languages::update', ['id' => $language->id]) : route('admin::languages::store') }}" class="form-row">
    <input type="hidden" id="id" name="id" value="{{ $language->id }}" />
    @csrf

    <div class="form-group col-md-6">
        <label>{{ __("Nombre") }}</label>
        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ? old('name') : $language->name }}" required="required" />
        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-6">&nbsp;</div>

    <div class="form-group col-md-6">
        <label>{{ __("Abreviación") }}</label>
        <input type="text" name="abbreviation" id="abbreviation" class="form-control @error('abbreviation') is-invalid @enderror" value="{{ old('abbreviation') ? old('abbreviation') : $language->abbreviation }}" required="required" />
        @error('abbreviation')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-6">&nbsp;</div>

    <div class="col-md-6">
        <a href="{{ route('admin::languages::list') }}" class="btn btn-danger btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">{{ __("Regresar") }}</a>
        </a>
        <div class="float-left">&nbsp;</div>
        <button type="submit" class="btn btn-primary btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-save"></i>
            </span>
            <span class="text">{{ __("Guardar") }}</a>
        </button>
    </div>
</form>
@stop