@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        @if($edit)
        <h1>{{ __("Editar post") }}</h1>
        @else
        <h1>{{ __("Crear post") }}</h1>
        @endif
    @endslot

    @slot('breadcrumb')
        {{ __("Posts") }}
    @endslot
@endcomponent
@stop

@section('content')

<form method="post" action="{{ $edit ? route('admin::blog_posts::update', ['id' => $post->id]) : route('admin::blog_posts::store') }}" class="form-row" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $post->id }}" />
    @csrf

    <div class="container">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ __("Información básica") }}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>{{ __("Título") }} (Espa&ntilde;ol)</label>
                        <input type="text" name="title_es" id="title_es" class="form-control @error('title_es') is-invalid @enderror" value="{{ old('title_es') ? old('title_es') : $post->title_es }}" required="required" />
                        @error('title_es')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>{{ __("Título") }} (English)</label>
                        <input type="text" name="title_en" id="title_en" class="form-control @error('title_en') is-invalid @enderror" value="{{ old('title_en') ? old('title_en') : $post->title_en }}" required="required" />
                        @error('title_en')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-md-6">
                        <label>{{ __("URL") }}</label>
                        <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') ? old('slug') : $post->slug }}" readonly />
                    </div>
                    <div class="col-md-6">&nbsp;</div>

                    <div class="form-group col-md-12">
                        <label>{{ __("Cuerpo") }} (Espa&ntilde;ol)</label>
                        <textarea rows="10" name="body_es" id="body_es" class="form-control @error('body_es') is-invalid @enderror" required="required">
                            {{ old('body_es') ? old('body_es') : $post->body_es }}
                        </textarea>
                        @error('body_es')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-md-12">
                        <label>{{ __("Cuerpo") }} (English)</label>
                        <textarea rows="10" name="body_en" id="body_es" class="form-control @error('body_en') is-invalid @enderror" required="required">
                            {{ old('body_en') ? old('body_en') : $post->body_en}}
                        </textarea>
                        @error('body_en')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-md-12">
                        <label>
                            <input type="checkbox" name="status" id="status" value="1" @if($post->status == 1) checked @endif />
                            {{ __("Activo") }}
                        </label>                
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ __("Categorías") }}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <p>{{ __("Seleccione las categorías por asignar a este post") }}</p>
                        <br />
                    </div>
                    @foreach($categories as $category)
                    <div class="form-group col-md-4 com-sm-6">
                        <label>
                            <input type="checkbox" name="categories[]" id="category_{{ $category->id }}" value="{{ $category->id }}" @if(sizeof($category->posts) && ($category->posts[0]->id == $post->id)) checked @endif />
                            {{ $category->name() }}
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ __("Imagen") }}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>{{ __("Imagen") }}</label>
                        <input type="file" name="image" id="image" />
                    </div>
                    <div class="col-md-6">
                        @if(!empty($post->image))
                            <img width="200" src="{{ asset($post->getOriginalImageUrl()) }}" alt="{{ __("Imagen") }}" />
                        @else
                            &nbsp;
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <a href="{{ route('admin::blog_posts::list') }}" class="btn btn-danger btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">{{ __("Regresar") }}</a>
        </a>
        <div class="float-left">&nbsp;</div>
        <button type="submit" class="btn btn-success btn-icon-split float-left" name="save_and_stay">
            <span class="icon text-white-50">
                <i class="fas fa-save"></i>
            </span>
            <span class="text">{{ __("Guardar y permanecer") }}</a>
        </button>
        <div class="float-left">&nbsp;</div>
        <button type="submit" class="btn btn-primary btn-icon-split float-left">
            <span class="icon text-white-50">
                <i class="fas fa-save"></i>
            </span>
            <span class="text">{{ __("Guardar") }}</a>
        </button>
    </div>
</form>
@stop

@section('js')
<script src="{{ asset('/back/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('/back/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script>
(function(w, $){
    $(document).ready(function() {
        tinymce.init({
            selector: "textarea#body_es, textarea#body_en",
            //content_css : 'css/editor.css',
            height: 400,
            theme: 'silver',
            language: '{{ App\Helpers\LocaleHelper::getLocale() }}',
            relative_urls: false,
            document_base_url: w.frontier_admin.base_url,
            remove_script_host: false,
            extended_valid_elements: 'span,iframe[src|frameborder|style|scrolling|class|width|height|name|align]',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor ",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste textcolor"
            ],
            toolbar: "undo redo | styleselect | forecolor backcolor | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | visualblocks | table",
            image_advtab: true
        });
    });
})(window, jQuery);
</script>
@stop
