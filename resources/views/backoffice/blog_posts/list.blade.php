@extends('backoffice.layouts.app')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        {{ __("Posts") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Posts") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('js')
    <!-- Select2 -->
    <script src="{{ asset('back/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    (($) => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(() => {
            let hookDelete = () => {
                $("a[rel='delete']").unbind();
                $("a[rel='delete']").bind('click', (e) => {
                    e.preventDefault();
                    if (confirm('{{ __("¿Está seguro de borrar este elemento?") }}')) {
                        window.location.href = $(e.currentTarget).attr('href');
                    }
                });
            };

            let $dataTable = $('#dataTable')
                .on('order.dt', () => hookDelete())
                .on('search.dt', () => hookDelete())
                .on('page.dt', () => hookDelete())
                .DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin::blog_posts::data") }}',
                    type: 'post',
                    complete: () => {
                        //hookDataTable();
                    }
                },
                @if(App\Helpers\LocaleHelper::getLocale() != "en")
                language: {
                    url: '{{ asset("back/plugins/datatables/" . App\Helpers\LocaleHelper::getLocale() . ".json") }}'
                },
                @endif
                columns: [
                    { data: 'id', name: 'id' },
                    @if(App\Helpers\LocaleHelper::getLocale() != "en")
                    { data: 'title_es', name: 'title_es' },
                    @else
                    { data: 'title_en', name: 'title_en' },
                    @endif
                    { data: 'slug', name: 'slug' },
                    { actions: 'actions', name: 'actions', searchable: false, orderable: false,
                        render: (data, type, row) => {
                            let urlEdit = '{{ route("admin::blog_posts::edit", ["id" => ":xx:"]) }}'.replace(':xx:', row.id);
                            let urlDelete = '{{ route("admin::blog_posts::delete", ["id" => ":xx:"]) }}'.replace(':xx:', row.id);
                            return `<a href="` + urlEdit + `" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Editar post") }}">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>
                            <a href="` + urlDelete + `" rel="delete" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Borrar post") }}">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>`;
                        } 
                    }
                ],
                initComplete: (settings, json) => {
                    let api = $('#dataTable').dataTable().api();
                    let $filter = $('#dataTable_filter input');
                    $('#dataTable_filter input').unbind();
                    $('#dataTable_filter input').bind('keyup', (e) => {
                        if(e.keyCode == 13) {
                            api.search($filter.val()).draw();
                        }
                    });

                    hookDelete();
                }
            });
        });
    })(jQuery);
    </script>
@stop

@section('content')
<!-- Page Heading -->
<p class="mb-4">
    <a href="{{ route('admin::blog_posts::create') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-plus-square"></i> {{ __("Crear post") }}</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Posts registrados") }}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ __("Nombre") }}</th>
                        <th>{{ __("URL") }}</th>
                        <th class="no-sort">{{ __("Acciones") }}</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
