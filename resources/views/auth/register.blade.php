@extends('frontoffice.layouts.app')

@section('js')
<script>
(($) => {
    $(document).ready(() => {
        let genericDomains = JSON.parse('{!! App\GenericDomain::select("domain")->get()->pluck("domain")->toJson() !!}');

        let checkEmail = (email) => {
            if (email !== '') {
                let values = email.split("@");
                if (values.length >= 2) {
                    let find = genericDomains.filter((domain) => {
                        return domain === values[1];
                    });

                    return find.length > 0;
                }
            }

            return false;
        };

        let setVisibilityReason = ($input) => {
            if (checkEmail($input.val())) {
                $("#reason_container").removeClass("d-none");
                $("#email_reason").prop("required", true);
                $("#email_reason").focus();
            }
            else{
                $("#reason_container").addClass("d-none");
                $("#email_reason").prop("required", false);
            }
        };

        $('#email').blur((e) => {
            let $input = $(e.target);
            setVisibilityReason($input);
        });

        setVisibilityReason($('#email'));
    });
})(jQuery);
</script>

@if(App::environment(['staging', 'production']))
{!! Captcha::renderJs() !!}
@endif

@endsection

@section('content')
<section class="register signup_area signup_area_height">
    <div class="row ml-0 mr-0">
        <div class="sign_right signup_right">
            <div class="sign_inner signup_inner">
                <div class="text-center">
                    <h3>{{ __('Registro') }}</h3>
                    <p>{{ __("¿Ya tiene una cuenta?") }} <a href="{{ route("login") }}">{{ __("Inicie sesión aquí") }}</a></p>
                </div>
                <form method="POST" action="{{ route('register') }}" class="row login_form captcha-form">
                    @csrf
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __('Nombre') }}</div>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="{{ __("Nombre completo") }}" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __('Organización') }}</div>
                        <input id="entity" type="text" class="form-control @error('entity') is-invalid @enderror" name="entity" value="{{ old('entity') }}" required autocomplete="entity" autofocus placeholder="{{ __("Organización para la que trabaja") }}">

                        @error('entity')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __('Cargo') }}</div>
                        <input id="position" type="text" class="form-control @error('position') is-invalid @enderror" name="position" value="{{ old('position') }}" required autocomplete="position" autofocus placeholder="{{ __("Su cargo") }}">

                        @error('position')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __('País') }}</div>
                        <select id="country_id" class="form-control @error('country_id') is-invalid @enderror" name="country_id" required autocomplete="country" autofocus>
                            <option>{{ __("Seleccionar país") }}</option>
                            @foreach($countries as $country)
                            <option value="{{ $country->id }}" @if($country->id == old('country_id')) selected @endif>{{ $country->name() }}</option>
                            @endforeach
                        </select>
                        @error('country_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __('Género') }}</div>
                        <select id="gender_id" class="form-control @error('gender_id') is-invalid @enderror" name="gender_id" required autocomplete="gender" autofocus>
                            <option>{{ __("Seleccionar género") }}</option>
                            @foreach($genders as $gender)
                            <option value="{{ $gender->id }}" @if($gender->id == old('gender_id')) selected @endif>{{ $gender->name() }}</option>
                            @endforeach
                        </select>
                        @error('gender_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __('Rango de edad') }}</div>
                        <select id="age_range_id" class="form-control @error('age_range_id') is-invalid @enderror" name="age_range_id" required autocomplete="age_range" autofocus>
                            <option>{{ __("Seleccionar rango de edad") }}</option>
                            @foreach($ageRanges as $range)
                            <option value="{{ $range->id }}" @if($range->id == old('age_range_id')) selected @endif>{{ $range->name() }}</option>
                            @endforeach
                        </select>
                        @error('age_range_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __("Correo laboral") }}</div>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __("info@ejemplo.com") }}">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div id="reason_container" class="col-lg-12 form-group d-none">
                        <div class="small_text">{{ __("Por razones de seguridad, el VZAT normalmente no permite el registro de usuarios con correos electrónicos genéricos. Por favor, proporcione detalles adicionales.") }}</div>
                        <input id="email_reason" type="email_reason" class="form-control @error('email_reason') is-invalid @enderror" name="email_reason" value="{{ old('email_reason') }}" autocomplete="email" placeholder="">
                        @error('email_reason')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __("Clave") }}</div>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="{{ __("5+ caracteres requeridos") }}" required autocomplete="off">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __('Confirmar clave') }}</div>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{ __("5+ caracteres requeridos") }}" required autocomplete="off">
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="check_box">
                            <input type="checkbox" value="Si" name="accept_terms" id="accept_terms" @if(old('accept_terms') == 'Si') checked @endif>
                            <label class="l_text" for="accept_terms">{{ __("Acepto") }} <span><a href="{{ route("terms") }}" target="_blank">{{ __("los términos y condiciones") }}</a></span></label>
                        </div>
                        <br />
                        <div class="check_box">
                            <input type="checkbox" value="Si" name="receive_emails" id="receive_emails" @if(old('receive_emails') == 'Si') checked @endif>
                            <label class="l_text" for="receive_emails">{{ __("Deseo recibir los productos del VZAT al correo electrónico") }}</span></label>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center">
                        @if(App::environment(['staging', 'production']))
                            {!! Captcha::renderButton() !!}
                        @endif
                        <button type="submit" class="btn action_btn thm_btn btn-blue">{{ __('Registrarse') }}</button>
                        <div class="mt-20"></div>
                        <p>{{ __("Si experimenta problemas para registrarse por favor contáctenos a info@vzat.org") }}</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br />
    @component('frontoffice.components.contact_block')
        @slot('title')
            {{ __("¿Problemas para registrarse?") }}
        @endslot

        @slot('text')
            {{ __("Contáctenos para obtener ayuda") }}
        @endslot
    @endcomponent
</section>
@endsection
