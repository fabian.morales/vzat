@extends('frontoffice.layouts.app')

@section('content')
<section class="login signup_area">
    <div class="container">
        <div class="row no-gutters ml-0 mr-0">
            <div class="sign_left signin_left col-md-6">
                <div class="doc_banner_text text-md-left">
                    <h2 class="wow fadeInUp mb-2" data-wow-delay="0.3s">Venezuela Analysis Team</h2>
                    <p class="wow fadeInUp" data-wow-delay="0.5s">{{ __("Nos complace compartir con ustedes los reportes desarrollados por nuestro equipo") }}</p>
                    <p class="mt-5"><a href="{{ route('about') }}" class="btn btn-yellow wide">{{ __("Conozca más sobre nosotros") }} <i class="fas fa-arrow-right"></i></a></p>
                    <br />
                </div>
            </div>
            <div class="sign_right signup_right col-md-6">
                <div class="sign_inner signup_inner">
                    <div class="text-center">
                        <h3>{{ __("Ingreso") }}</h3>
                        <p>{{ __("¿No tiene una cuenta?") }} <a href="{{ route("register") }}">{{ __("Regístrese aquí") }}</a></p>
                    </div>
                    <form method="POST" action="{{ route('login') }}" class="row login_form">
                        @csrf
                        <div class="col-lg-12 form-group">
                            <div class="small_text">{{ __("Correo") }}</div>                        
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __("info@ejemplo.com") }}">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-lg-12 form-group">
                            <div class="small_text">{{ __("Clave") }}</div>
                            <div class="confirm_password">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="{{ __("5+ caracteres requeridos") }}" autocomplete="off">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <a href="{{ route('password.request') }}" class="forget_btn">{{ __("¿Olvidaste tu clave?") }}</a>
                            </div>
                        </div>

                        @if(App::environment(['staging', 'production']))
                        <div class="col-lg-12">
                            @php /*{!! NoCaptcha::display() !!}*/ @endphp
                            @error('g-recaptcha-response')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        @endif

                        @php
                        /*<div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Recordarme') }}
                                </label>
                            </div>
                        </div>*/
                        @endphp

                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn action_btn thm_btn btn-blue">{{ __('Iniciar sesión') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_area_login">
        <div class="footer_bottom text-center">
            <div class="container">
                <div class="row align-items-end">
                    <div class="col-md-6 text-md-left text-center">
                        <img src="{{ asset('front/img/logo_ue.png') }}" alt="" />
                    </div>
                    <div class="col-md-6 text-md-right text-center">
                        <br />
                        <p>&copy; {{ date('Y') }} {{ __("Todos los derechos reservados por") }} <a href="{{ route("home") }}">VZAT</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('footer')
@stop
