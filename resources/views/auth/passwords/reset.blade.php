@extends('frontoffice.layouts.app')

@section('content')
<section class="register signup_area signup_area_height">
    <div class="row ml-0 mr-0">
        <div class="sign_right signup_right">
            <div class="sign_inner signup_inner">
                <div class="text-center">
                    <h3>{{ __('Restaurar clave') }}</h3>
                </div>
                <form method="POST" action="{{ route('password.update') }}" class="row login_form">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __("Correo") }}</div>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __("info@ejemplo.com") }}">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __("Clave") }}</div>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="{{ __("5+ caracteres requeridos") }}" required autocomplete="off">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __('Confirmar clave') }}</div>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{ __("5+ caracteres requeridos") }}" required autocomplete="off">
                    </div>
                    
                    <div class="col-lg-12 text-center">
                        <button type="submit" class="btn action_btn thm_btn btn-blue">{{ __('Restaurar clave') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
