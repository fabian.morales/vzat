@extends('frontoffice.layouts.app')

@section('content')
<section class="register signup_area signup_area_height">
    <div class="row ml-0 mr-0">
        <div class="sign_right signup_right">
            <div class="sign_inner signup_inner">
                <div class="text-center">
                    <h3>{{ __('Restaurar clave') }}</h3>
                </div>
                <form method="POST" action="{{ route('password.email') }}" class="row login_form">
                    @csrf
                    <div class="col-lg-12 form-group">
                        <div class="small_text">{{ __("Correo") }}</div>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __("info@ejemplo.com") }}">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <div class="col-lg-12 text-center">
                        <button type="submit" class="btn action_btn thm_btn btn-blue">{{ __('Enviar enlace para restaurar clave') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
