@extends('emails.layouts.user_master')

@section('content')
    @component('emails.components.title')
        @slot('text')
            {{ __("Hola :Name", ['Name' => $user->name]) }}.
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            {{ __("Hola :Name, su cuenta ha sido aprobada.", ['Name' => $user->name]) }}.
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            {{ __("Para continuar con el proceso de registro, active su cuenta haciendo clic en el siguiente enlace") }}:
        @endslot
    @endcomponent

    @component('emails.components.action')
        @slot('text')
            {{ __("Verificar") }}
        @endslot

        @slot('url')
            {{ route('user_verification', ['token' => $token]) }}
        @endslot
    @endcomponent

    <br />
    <hr />
    <br />

    @component('emails.components.regular_text')
        @slot('text')
            {{ __("Hello :Name, your account has been approved.", ['Name' => $user->name]) }}.
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            {{ __("To continue with the registration process, please activate your account by clicking on the link below") }}:
        @endslot
    @endcomponent

    @component('emails.components.action')
        @slot('text')
            {{ __("Verify") }}
        @endslot

        @slot('url')
            {{ route('user_verification', ['token' => $token]) }}
        @endslot
    @endcomponent

    @component('emails.components.subcopy')
        @slot('content')
            {{ __("Si tiene problemas para hacer clic en el botón de arriba, copie y pegue la siguiente URL en su navegador web") }}:

            @component('emails.components.flat_action')
                @slot('text')
                    {{ route('user_verification', ['token' => $token]) }}
                @endslot

                @slot('url')
                    {{ route('user_verification', ['token' => $token]) }}
                @endslot
            @endcomponent
        @endslot
    @endcomponent
@stop