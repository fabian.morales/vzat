@extends('emails.layouts.user_master')

@section('content')
    @component('emails.components.title')
        @slot('text')
            {{ __("Cuenta no aprobada") }}
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            Hola {{ $user->name }}, su cuenta no ha sido aprobada por el equipo de VZAT. Si tiene alguna duda por favor cont&aacute;ctenos <a href="{{ route('contact') }}">aqu&iacute;</a>.
        @endslot
    @endcomponent

    <br />
    <hr />
    <br />

    @component('emails.components.regular_text')
        @slot('text')
            Hello {{ $user->name }}, your account has not been approved by the VZAT team. If you have any questions please contact us <a href="{{ route('contact') }}">here</a>.
        @endslot
    @endcomponent
@stop
