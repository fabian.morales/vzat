@extends('emails.layouts.admin_master')

@section('content')
    @component('emails.components.title')
        @slot('text')
            {{ __("Se ha registrado un usuario nuevo") }}
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            {{ __("Estos son los datos del usuario nuevo") }}:
        @endslot
    @endcomponent

    <ul>
        <li><b>{{ __("Nombre") }}</b>: {{ $user->name }}</li>
        <li><b>{{ __("Correo") }}</b>: {{ $user->email }}</li>
        <li><b>{{ __("Entidad") }}</b>: {{ $user->entity }}</li>
        <li><b>{{ __("Cargo") }}</b>: {{ $user->position }}</li>
        @php
            $country = $user->country()->first()
        @endphp
        @if (!empty($country))
        <li><b>{{ __("País") }}</b>: {{ $country->name() }}</li>
        @endif
    </ul>

    @component('emails.components.action')
        @slot('text')
            {{ __("Revisar") }}
        @endslot

        @slot('url')
            {{ route('admin::users_approval::list') }}
        @endslot
    @endcomponent
@stop
