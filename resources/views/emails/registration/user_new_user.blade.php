@extends('emails.layouts.user_master')

@section('content')
    @component('emails.components.title')
        @slot('text')
            {{ __("Gracias por registrarte") }}
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            <p>Hemos recibido su registro. Una vez su solicitud sea aprobada por parte del equipo de VZAT, recibirá un correo electrónico para activar su cuenta.</p>
        @endslot
    @endcomponent

    <br />
    <hr />
    <br />
    
    @component('emails.components.regular_text')
        @slot('text')
            <p>We have received your registration. Once your application is approved by the VZAT team, you will receive an email to activate your account.</p>
        @endslot
    @endcomponent
@stop