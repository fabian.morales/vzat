@extends('emails.layouts.user_master')

@section('content')
    @component('emails.components.title')
        @slot('text')
            {{ __("Hemos recibido su solicitud") }}
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            {{ __("Hemos recibido su registro y nos pondremos en contacto con usted en el menor tiempo posible") }}.
        @endslot
    @endcomponent
@stop