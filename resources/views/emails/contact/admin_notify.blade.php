@extends('emails.layouts.admin_master')

@section('content')
    @component('emails.components.title')
        @slot('text')
            {{ __("Se ha recibido una nueva solicitud de contacto") }}
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            {{ __("Estos son los datos del contacto:") }}:
        @endslot
    @endcomponent

    <ul>
        <li><b>{{ __("Nombre") }}</b>: {{ $contact->name }}</li>
        <li><b>{{ __("Correo") }}</b>: {{ $contact->email }}</li>
        <li><b>{{ __("Entidad") }}</b>: {{ $contact->entity }}</li>
        @php
            $country = $contact->country()->first()
        @endphp
        @if (!empty($country))
        <li><b>{{ __("País") }}</b>: {{ $country->name() }}</li>
        @endif

        <li><b>{{ __("Mensaje") }}</b>: {{ $contact->message }}</li>
    </ul>

    @component('emails.components.action')
        @slot('text')
            {{ __("Revisar") }}
        @endslot

        @slot('url')
            {{ route('admin::contact::list') }}
        @endslot
    @endcomponent
@stop
