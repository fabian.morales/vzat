@extends('errors::illustrated-layout')

@section('title', __('Error. No encontramos lo que está buscando.'))
@section('code', '404')
@section('message', __('Error. No encontramos lo que está buscando.'))
