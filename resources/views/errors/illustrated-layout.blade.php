<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    {!! SEOMeta::generate() !!}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset("/front/img/favicon.ico") }}" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset("/front/assets/bootstrap/css/bootstrap.min.css") }}">
    <!-- icon css-->
    <link rel="stylesheet" href="{{ asset("/front/assets/font-awesome/css/all.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/assets/elagent-icon/style.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/assets/animation/animate.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/assets/niceselectpicker/nice-select.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/css/style.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/css/responsive.css") }}">
    <link rel="stylesheet" href="{{ asset("/front/css/app.css") }}">

    @section('css')
    @show
</head>
<body data-scroll-animation="true" class="body_error">
    <div class="body_wrapper error">
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-lg-8 col-md-9">
                    <div class="mb-40"></div>

                    <div class="error-code">
                        @yield('code', __('Oh no'))
                    </div>

                    <div class="mb-60"></div>

                    <p class="error-message">
                        @yield('message')
                    </p>

                    <p class="error-subcopy">{{ __("Nos disculpamos por el inconveniente. Visite nuestra página web de inicio para más contenido o realice una búsqueda") }}</p>
                    <div class="mb-40"></div>

                    <form action="{{ route('search::index') }}" method="POST">
                        @csrf
                        <div class="input-group">
                            <input type="search" name="text_search" id="text_search" class="form-control" placeholder="{{ __("Buscar") }}">
                            <button type="submit" class="d-none"><i class="icon_search"></i></button>
                        </div>
                    </form>
                    <div class="mb-60"></div>
                    <a href="{{ route("home") }}" class="btn btn-yellow wide">
                        {{ __("Regresar a la Página de Inicio") }} &nbsp;&nbsp;<i class="fas fa-arrow-right"></i>
                    </a>
                    <div class="mb-60"></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
