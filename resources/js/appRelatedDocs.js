import Vue from 'vue';
import VueI18n from 'vue-i18n'
import moment from 'moment';
import en from '../lang/en';
import es from '../lang/es';

const documentsUrl = document.getElementById('documentsUrl').value;
const locale = document.getElementById('locale').value;

const messages = { en, es };
Vue.use(VueI18n);
const i18n = new VueI18n({
    locale,
    messages,
});

Vue.filter('formatDate', (value) => {
    if (value) {   
        return moment(String(value), "YYYY-MM-DD")
            .locale(locale)
            .fromNow();
    }
});

import DocumentCardList from './components/DocumentCardList.vue';
const app = new Vue({
    el: '#app-doc-list',
    i18n,
    components: {
        DocumentCardList
    },
    render (h) {
        return h(DocumentCardList, { props: { documentsUrl } })
    }
});