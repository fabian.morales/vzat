import Vue from 'vue';
import VueI18n from 'vue-i18n'
import moment from 'moment';
import en from '../lang/en';
import es from '../lang/es';

const latestUrl = document.getElementById('latestUrl').value;
const searchUrl = document.getElementById('searchUrl').value;
const allDocumentsUrl = document.getElementById('allDocumentsUrl').value;
const locale = document.getElementById('locale').value;
const isPost = document.getElementById('isPost').value;
const defaultTextSearch = document.getElementById('textSearch').value;
const defaultCategoryId = document.getElementById('categoryId').value;
const defaultStateId = document.getElementById('stateId').value;
const aboutUsUrl = document.getElementById('aboutUsUrl').value;
const updateSearchUrl = document.getElementById('updateSearchUrl').value;

const messages = { en, es };
Vue.use(VueI18n);
const i18n = new VueI18n({
    locale,
    messages,
});

Vue.filter('formatDate', (value) => {
    if (value) {   
        return moment(String(value), "YYYY-MM-DD")
            .locale(locale)
            .fromNow();
    }
});

import SearchContainer from './components/SearchContainer.vue';
const app = new Vue({
    el: '#app-search',
    i18n,
    components: {
        SearchContainer
    },
    render (h) {
        return h(SearchContainer, { props: { latestUrl, searchUrl, allDocumentsUrl, locale, isPost, defaultTextSearch, defaultCategoryId, defaultStateId, aboutUsUrl, updateSearchUrl } })
    }
});
