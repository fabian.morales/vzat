<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'La clave ha sido cambiada exitosamente.',
    'sent' => 'Hemos enviado a tu correo el enlace para restablecer tu clave',
    'throttled' => 'Espera un momentos antes de intentar nuevamente.',
    'token' => 'Este enlace de restablecimiento de clave no es válido.',
    'user' => "No hay usuarios registrados con ese correo.",

];
