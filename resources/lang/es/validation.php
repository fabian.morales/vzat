<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'El :attribute debe ser aceptado.',
    'active_url' => 'El :attribute no es una URL válida.',
    'after' => 'El :attribute debe ser posterior a :date.',
    'after_or_equal' => 'El :attribute debe ser igual o posterior a :date.',
    'alpha' => 'El :attribute debe contener solo letras.',
    'alpha_dash' => 'El :attribute debe contener solo letras, números, guiones and guiones bajos.',
    'alpha_num' => 'El :attribute debe contener solo letras y números.',
    'array' => 'El :attribute debe ser una colección de valores.',
    'before' => 'El :attribute debe ser anterior a :date.',
    'before_or_equal' => 'El :attribute debe ser anterior o igual a :date.',
    'between' => [
        'numeric' => 'El :attribute debe estar entre :min and :max.',
        'file' => 'El :attribute debe estar entre :min and :max KB.',
        'string' => 'El :attribute debe estar entre :min and :max caracteres.',
        'array' => 'El :attribute debe estar entre :min and :max items.',
    ],
    'boolean' => 'El campo :attribute debe ser verdadero o falso.',
    'confirmed' => 'La confirmación de :attribute no coincide.',
    'date' => 'El :attribute no es una fecha válida.',
    'date_equals' => 'El :attribute debe ser una fecha igual a :date.',
    'date_format' => 'El :attribute no corresponde con el formato :format.',
    'different' => 'El :attribute y :other deben ser diferentes.',
    'digits' => 'El :attribute debe ser de :digits dígitos.',
    'digits_between' => 'El :attribute debe estar entre :min y :max dígitos.',
    'dimensions' => 'El :attribute tiene unas dimensiones inválidas.',
    'distinct' => 'El campo :attribute tiene un valor duplicado.',
    'email' => 'El :attribute debe ser una dirección de correo válida.',
    'ends_with' => 'El :attribute debe terminar con uno de estos valores: :values.',
    'exists' => 'El valor seleccionado de :attribute es inválido.',
    'file' => 'El :attribute debe ser un archivo.',
    'filled' => 'El campo :attribute debe tener un valor.',
    'gt' => [
        'numeric' => 'El :attribute debe ser mayor que :value.',
        'file' => 'El :attribute debe ser mayor que :value KB.',
        'string' => 'El :attribute debe ser mayor que :value caracteres.',
        'array' => 'El :attribute debe tener más de :value items.',
    ],
    'gte' => [
        'numeric' => 'El :attribute debe ser mayor o igual que :value.',
        'file' => 'El :attribute debe ser mayor o igual que :value kilobytes.',
        'string' => 'El :attribute debe ser mayor o igual que :value characters.',
        'array' => 'El :attribute debe tener :value items o más.',
    ],
    'image' => 'El :attribute debe ser una imagen.',
    'in' => 'El valor seleccionado para :attribute es inválido.',
    'in_array' => 'El campo :attribute no se encuentra en :other.',
    'integer' => 'El :attribute debe ser un número entero.',
    'ip' => 'El :attribute deb ser una dirección IP válida.',
    'ipv4' => 'El :attribute deb ser una dirección IPv4 válida.',
    'ipv6' => 'El :attribute deb ser una dirección IPv6 válida.',
    'json' => 'El :attribute debe ser una cadena JSON válida.',
    'lt' => [
        'numeric' => 'El :attribute debe ser menor que :value.',
        'file' => 'El :attribute debe ser menor que :value KB.',
        'string' => 'El :attribute debe ser menor que :value caracteres.',
        'array' => 'El :attribute debe tener menos de :value items.',
    ],
    'lte' => [
        'numeric' => 'El :attribute debe ser menor o igual que :value.',
        'file' => 'El :attribute debe ser menor o igual que :value KB.',
        'string' => 'El :attribute debe ser menor o igual que :value caracteres.',
        'array' => 'El :attribute no debe tener más de :value items.',
    ],
    'max' => [
        'numeric' => 'El :attribute no debe ser mayor que :max.',
        'file' => 'El :attribute no debe ser mayor que :max KB.',
        'string' => 'El :attribute no debe ser mayor que :max caracteres.',
        'array' => 'El :attribute no debe tener más de :max items.',
    ],
    'mimes' => 'El :attribute debe ser un tipo de archivo: :values.',
    'mimetypes' => 'El :attribute debe ser un tipo de archivo: :values.',
    'min' => [
        'numeric' => 'El :attribute debe ser al menos de :min.',
        'file' => 'El :attribute debe ser al menos de :min kilobytes.',
        'string' => 'El :attribute debe ser al menos de :min characters.',
        'array' => 'El :attribute debe tener al menos :min items.',
    ],
    'not_in' => 'El valor seleccionado de :attribute es inválido.',
    'not_regex' => 'El formato de :attribute is inválido.',
    'numeric' => 'El :attribute debe ser un número.',
    'password' => 'La clave es incorrecta.',
    'present' => 'El campo :attribute debe estar presente.',
    'regex' => 'El formato de :attribute es inválido.',
    'required' => 'El campo :attribute es requerido.',
    'required_if' => 'El campo :attribute es requerido cuando :other es :value.',
    'required_unless' => 'El campo :attribute a menos que :other esté en :values.',
    'required_with' => 'El campo :attribute es requerido cuando :values está presente.',
    'required_with_all' => 'El campo :attribute es requerido cuando :values están presentes.',
    'required_without' => 'El campo :attribute es requerido cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es requerido cuando ninguno de :values están presentes.',
    'same' => 'El :attribute y :other deben ser iguales.',
    'size' => [
        'numeric' => 'El :attribute debe ser de :size.',
        'file' => 'El :attribute debe ser de :size KB.',
        'string' => 'El :attribute debe ser de :size caracteres.',
        'array' => 'El :attribute debe contener :size items.',
    ],
    'starts_with' => 'El :attribute debe empezar con uno de estos valores: :values.',
    'string' => 'El :attribute debe ser una cadena de caracteres.',
    'timezone' => 'El :attribute debe ser una zona horaria válida.',
    'unique' => 'El :attribute no está disponible.',
    'uploaded' => 'No se pudo cargar el :attribute.',
    'url' => 'El formato de :attribute es inválido.',
    'uuid' => 'El :attribute debe ser un UUID válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'g-recaptcha-response' => [
            'required' => 'No se recibió la validación de captcha.',
            'captcha' => 'Error de Captcha. Intenta nuevamente.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'email_reason' => 'razón del correo',
        'email' => 'correo',
    ],

];
