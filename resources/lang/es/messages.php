<?php

return [
    "Reset Password Notification" => "Notificación de restablecimiento de clave",
    "You are receiving this email because we received a password reset request for your account." => "Recibió este correo electrónico porque recibimos una solicitud de restablecimiento de clave para su cuenta.",
    "Reset Password" => "Restablecimiento de clave",
    "If you did not request a password reset, no further action is required." => "Si no solicitó un restablecimiento de clave, no se requiere ninguna otra acción.",
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\ninto your web browser:" => "Si tiene problemas para hacer clic en el botón de \":actionText\", copie y pegue la siguiente URL en su navegador web:",
];