export default {
    titles: {
        'search_results': 'Search Results',
        'latest_documents': 'Documentos recientes',
        'all_documents': 'Todos los documentos',
        'all_states': 'Todos los estados',
        'submain_search': 'Nos complace compartir con ustedes los reportes desarrollados por nuestro equipo',
        'learn_more_vzat': 'Conoce más sobre Venezuela Analysis Team',
    },
    actions: {
        'see_all_documents': 'Ver todos los documentos',
        'search_reports': 'Buscar reportes',
        'filter_by_language': 'Filtrar por idioma',
        'filter_by_type': 'Filtrar por tipo de reporte',
        'filter_by_sector': 'Filtrar por sector',
        'filter_by_state': 'Filtrar por estado',
        'order_by': 'Organizar por',
        'reset_filters': 'Limpiar filtros',
        'apply_filters': 'Aplicar filtros',
    },
    messages: {
        'no_results': 'No hay resultados',
        'status_paginator': 'Mostrando {itemsPerPage} resultados (de {total} en total)',
    },
    options: {
        'all_documents': 'Todos los reportes',
        'all_states': 'Todos los estados',
        'latest': 'Más recientes',
        'oldest': 'Más antiguos',
    },
    labels: {
        'reports': 'Reportes',
        'language': 'Idioma',
        'type': 'Tipo',
        'sector': 'Sector',
        'state': 'Estado',
        'order': 'Orden',
        'updated': 'Actualizado',
        'about_us': 'Quiénes somos',
    },
    tooltips: {
        'sector': 'Seleccione los sectores',
        'state': 'Seleccione los estados',
        'language': 'Seleccione los lenguajes',
        'category': 'Seleccione los tipos de reporte',
        'order_by': 'Ordene por fecha o nombre',
    },
};