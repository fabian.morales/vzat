export default {
    titles: {
        'search_results': 'Search Results',
        'latest_documents': 'Latest documents',
        'all_documents': 'All documents',
        'all_states': 'All states',
        'submain_search': 'VZAT is pleased to share our range of products on the Venezuelan humanitarian crisis',
        'learn_more_vzat': 'Learn more about the Venezuela Analysis Team',
    },
    actions: {
        'see_all_documents': 'View all documents',
        'search_reports': 'Search reports',
        'filter_by_language': 'Filter by language',
        'filter_by_type': 'Filter by report type',
        'filter_by_sector': 'Filter by sector',
        'filter_by_state': 'Filter by state',
        'order_by': 'Order by',
        'reset_filters': 'Reset filters',
        'apply_filters': 'Appky filtros',
    },
    messages: {
        'no_results': 'There are no results',
        'status_paginator': 'Showing {itemsPerPage} results (of {total} total)',
    },
    options: {
        'all_documents': 'All reports',
        'all_states': 'All states',
        'latest': 'Latest',
        'oldest': 'Oldest',
    },
    labels: {
        'reports': 'Reports',
        'language': 'Language',
        'type': 'Type',
        'sector': 'Sector',
        'state': 'State',
        'order': 'Order',
        'updated': 'Updated',
        'about_us': 'About Us',
    },
    tooltips: {
        'sector': 'Select the sectors',
        'state': 'Select the states',
        'language': 'Select the languages',
        'category': 'Select the types',
        'order_by': 'Order by date or name',
    },
};