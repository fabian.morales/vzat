<?php

return [
    'google' => [
        'sitekey' => env('GOOGLE_RC_SITE'),
        'secret' => env('GOOGLE_RC_SECRET'),
    ],
    'mail' => [
        'admin' => env('MAIL_ADMIN_TO', 'soportemyc@gmail.com'),
        'support' => env('MAIL_SUPPORT', 'soportemyc@gmail.com'),
    ],
    'images' => [
        'base_path' => env('IMG_BASE_PATH'),
        'global_default' => env('GLOBAL_IMG_DEFAULT'),
        'post_default' => env('POST_IMG_DEFAULT'),
        'document_default' => env('DOCUMENT_IMG_DEFAULT'),
    ],
];